function [time, temperatures] = load(temp_filename, image_filename)
% Converts txt files from Labview temperature controller (1P room) into
% double. The header should be 22 lines.
% Get the timestamp from PCO image to scale time axis.

% load data
data = readtable(temp_filename, 'HeaderLines', 22);
% time
time = data(:, 1);
time = table2array(time);
% temperatures
temperatures = data(:, 2);
temperatures = table2array(temperatures);

% load the time it starts (line 11)
fileID = fopen(temp_filename);
begin_of_templog = textscan(fileID, '%s','delimiter', '\n');
begin_of_templog = begin_of_templog{1}{11};
begin_of_templog = begin_of_templog(6:end);
fclose(fileID);

begin_of_templog = str2double(begin_of_templog(1:2))*60*60 + ...
    str2double(begin_of_templog(4:5))*60 + ...
    str2double(begin_of_templog(7:end));

% get the timestamps
ts = PCO.timestamp(image_filename);
% time in the day in seconds
begin_of_experiment = ts.sec + ts.min*60 + ts.hour*60*60;

% origin of time
origin_of_time = begin_of_experiment - begin_of_templog;

% find closest value in time axis
[~, index] = min(abs(time - origin_of_time));

temperatures = temperatures(index:end);
time = time(index:end) - origin_of_time;

% figure;
% plot(time, temperatures);
% title(name);
end