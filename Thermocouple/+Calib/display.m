% 2017-11-15 Read previously saved calibration data from new calibration
% procedure.

clear;
close all
%% Load data
base = '/home/ljp/Science/Projects/Neurofish/Data/';     % base folder
study = 'Thermotaxis';

dat = '2017-11-16';     % date
Tsets = [27 30 33 0];              % temperature setpoint
protocol = 'calib_rand20_1s';
sbp = [2 2];        % subplot size
checkdate = datevec(dat);

% Format path
if checkdate(1) < 2018 && checkdate(2) < 9
    path_to_day = [base dat ' ' filesep];
else
    path_to_day = [base study filesep dat filesep];
end

% Define dir
input_dir = [path_to_day 'Calibration' filesep];

%data = cell(1, 4);
data = cell(1, numel(Tsets));

for idx_Tset = 1:numel(Tsets)
    Tset = Tsets(idx_Tset);
    if isstring(Tset)
        data_name = [input_dir 'calib_Tset=' char(Tset) '.mat'];
    else
        data_name = [input_dir 'calib_Tset=' num2str(Tset) '.mat'];
    end
    data{idx_Tset} = load(data_name); 
end

i = sbp(1); % first index for subplot
j = sbp(2); % second index for subplot
hotmap = [ones(numel(Tsets), 1), linspace(0, 1, numel(Tsets))', zeros(numel(Tsets), 1)];
coldmap = [zeros(numel(Tsets), 1), linspace(0, 1, numel(Tsets))', ones(numel(Tsets), 1)];

%% Plot full trace
figure;
hold on;
ccold = 0;
for idx = 1:numel(Tsets)
    set(gca, 'FontSize', 30);
    if Tsets(idx) > 22
        plot(data{idx}.trace.time, data{idx}.trace.temperature, 'Color', hotmap(idx, :), 'LineWidth', 3);
    elseif Tsets(idx) < 22
        ccold = ccold + 1;
        plot(data{idx}.trace.time, data{idx}.trace.temperature, 'Color', coldmap(ccold, :), 'LineWidth', 3);
    end
end
xlabel('Time [s]');
ylabel('Temperature [°C]');
%% Plot mean temperature
figure;
hold on
ccold = 0;
for idx = 1:1:numel(Tsets)
    %subplot(i, j, idx);
    set(gca,'FontSize', 30)
    if Tsets(idx) > 22
        plot(data{idx}.mean_time, data{idx}.mean_temp, 'Color', hotmap(idx, :), 'LineWidth', 3);
    elseif Tsets(idx) < 22
        ccold = ccold + 1;
        plot(data{idx}.mean_time, data{idx}.mean_temp, 'Color', coldmap(ccold, :), 'LineWidth', 3);
    end
    grid on
    title(['T_{set} = ' num2str(Tsets(idx)) '°C']);
    xlabel('Time [s]');
    ylabel('Temperature [°C]');
end

%% Plot peaks values
ccold = 0;
figure;
set(gca,'FontSize', 30)
for idx = 1:numel(Tsets)
    
    s = std(data{idx}.peak.values);
    m = mean(data{idx}.peak.values);
    mean_val = m*ones(1, numel(data{idx}.peak.values));  % to plot

    hold on
    grid on         % show grid
    curve_name = ['T_{set} = ' num2str(Tsets(idx))];    % make the curve name
    mcurve_name = ['T_{fish} = ' num2str(m, '%2.1f') '\pm ' num2str(s, '%2.1f') '°C'];
    if Tsets(idx) > 22
        plot(data{idx}.peak.values, 'o--', 'Color', hotmap(idx, :), 'DisplayName', curve_name, 'LineWidth', 2.5);
        h = plot(mean_val, 'Color', hotmap(idx, :), 'LineWidth', 3, 'DisplayName', mcurve_name); 
    elseif Tsets(idx) < 22
        ccold = ccold + 1;
        plot(data{idx}.peak.values, 'o--', 'Color', coldmap(ccold, :), 'DisplayName', curve_name, 'LineWidth', 2.5);
        h = plot(mean_val, 'Color', coldmap(ccold, :), 'LineWidth', 3, 'DisplayName', mcurve_name); 
    end
    h.Color(4) = 0.6;   % set transparency
    hold off
    
    legend;
    xlabel('# Pulse');
    ylabel('T_{fish} [°C]');
end

%% Plot peaks gradients
figure;
set(gca,'FontSize', 30)
for idx = 1:numel(Tsets)
    
    s = std(data{idx}.peak.deltaT);
    m = mean(data{idx}.peak.deltaT);
    mean_grad = m*ones(1, numel(data{idx}.peak.deltaT));
    
    hold on         % show grid
    grid on
    curve_name = ['T_{set} = ' num2str(Tsets(idx))];    % make the curve name
    mcurve_name = ['\Delta{T} = ' num2str(m, '%2.1f') '\pm ' num2str(s, '%2.1f') '°C'];
    if Tsets(idx) > 22
        plot(data{idx}.peak.deltaT, 'o--', 'Color', hotmap(idx, :), 'DisplayName', curve_name, 'LineWidth', 2.5);
        h = plot(mean_grad, 'Color', hotmap(idx, :), 'LineWidth', 3, 'DisplayName', mcurve_name);
    elseif Tsets(idx) < 22
        plot(data{idx}.peak.deltaT, 'o--', 'Color', coldmap(idx, :), 'DisplayName', curve_name, 'LineWidth', 2.5);
        h = plot(mean_grad, 'Color', coldmap(idx, :), 'LineWidth', 3, 'DisplayName', mcurve_name);
    end
    h.Color(4) = 0.6;   % set transparency for mean
    
    legend;
    xlabel('# Pulse');
    ylabel('\Delta{T} [°C]');
end