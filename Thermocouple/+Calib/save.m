% 2017-11-15 New calibration procedure for whole-brain at different
% temperature. Now the protocol is basically 20 random pulses, duration 1s,
% min dt = 10, max dt = 30. Parameters file should be named ParametersN.txt
% where N is the run number.

clear;

base = '/home/ljp/Science/Projects/Neurofish/Data/';     % base folder
study = 'Thermotaxis';
dat = '2018-06-29';             % date
runs = [1];                     % run number
Tsets = [17];              % temperature setpoint
protocol = 'calib_rand21_1s';

t_before = 2;
t_after = 10;

checkdate = datevec(dat);

% Format path
if checkdate(1) < 2018 && checkdate(2) < 9
    path_to_day = [base study '_Old' dat ' ' study filesep];
else
    path_to_day = [base study filesep dat filesep];
end

% Define output dir
output_dir = [path_to_day 'Calibration' filesep];
if ~exist(output_dir, 'dir')
    mkdir(output_dir);
end

c = 0;
for run = runs
    
    c = c + 1;
    Tset = Tsets(c);
    % Define path to run
    path_to_run = [path_to_day 'Run ' sprintf('%02i', run) filesep];
    
    % Define calibration file names
    pco_filename = [path_to_run 'Images.pcoraw'];
    if isstring(Tset)
        clb_filename = [path_to_run protocol '_Tset=' char(Tset)];
    else
        clb_filename = [path_to_run protocol '_Tset=' num2str(Tset)];
    end
    
    % Read calibration data
    [time, temperature] = Calib.load(clb_filename, pco_filename);
    
    % Get pulses onsets and duration
    [stim_onset, stim_tau] = getStimTimes(path_to_run);
    
    % find average sample rate of thermocouple
    sample_rate = 1./mean(diff(time));
    
    % Find peaks
    
    if Tset < 22
        temperature = - temperature;
    end
    
    [pks, loc, widths, dT] = findpeaks(temperature, sample_rate, ...
        'MinPeakProminence', .25);
    
    if Tset < 22
        temperature = - temperature;
        pks = - pks;
        dT = - dT;
    end
    
    % Check if all peaks are detected
    if numel(pks) ~= numel(stim_onset)
        warning('Not enough peaks detected.');
    end
    
%     % Average each peaks, interpolating
%     mean_temp = 0;      % init mean temperature
%     for idx_pulse = 1:numel(stim_onset)
%         
%         before_onset = stim_onset(idx_pulse) - t_before; % time before
%         after_onset = stim_onset(idx_pulse) + t_after; % time after
%         query_time = before_onset:1/sample_rate:after_onset; % missing points
%         missing_points = interp1(time, temperature, query_time, 'spline'); % interpolation
%         
%         mean_temp = mean_temp + missing_points; % add values
%     end
    
%     mean_temp = mean_temp./numel(stim_onset);
     mean_time = -t_before:1/sample_rate:t_after; % time vector, 0 is real onset
    
    tmp_temperatures = NaN(numel(stim_onset), numel(mean_time));
    
    for idx_pulse = 1:numel(stim_onset)
        before_onset = stim_onset(idx_pulse) - t_before; % time before
        after_onset = stim_onset(idx_pulse) + t_after; % time after
        query_time = before_onset:1/sample_rate:after_onset; % missing points
        missing_points = interp1(time, temperature, query_time, 'spline'); % interpolation
        
        tmp_temperatures(idx_pulse, :) = missing_points; % add values
    end
    
    mean_temp = mean(tmp_temperatures, 1);
    std_temp = std(tmp_temperatures, 1);
    
    % Verbose
    fprintf('Calibration data for Tset = %i°C processed,\nMean T_fish = %2.2f°C, mean delta_T = %2.2f°C.\n',...
        Tset, mean(pks), mean(dT));
    
    % Save data
    output_name = [output_dir 'calib_Tset=' num2str(Tset) '.mat'];
    param.onset = stim_onset;
    param.duration = stim_tau;
    peak.values = pks;
    peak.location = loc;
    peak.widths = widths;
    peak.deltaT = dT;
    trace.time = time;
    trace.temperature = temperature;
    save(output_name, 'trace', 'param', 'peak', 'mean_time', 'mean_temp', 'std_temp');
end