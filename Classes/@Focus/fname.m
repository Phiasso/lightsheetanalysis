function out = fname(this, tag, varargin)
%Focus.fname returns the filename corresponding to a tag.
%   OUT = FOCUS.FNAME(TAG) returns the filename corresponding to a tag. 
%   TAG should be a string, optionnaly comprising fileseparators to define 
%   subfolders. If TAGcontains an '@', the path is automatically 
%   adaptated to the current set.
%
%   See also: Focus, Focus.save, Focus.load.

% === Input variables =====================================================

in = inputParser;
in.addRequired('tag', @ischar);
in.addOptional('ext', 'mat', @ischar);
in.parse(tag, varargin{:});
in = in.Results;

% =========================================================================

% --- Run shortcut
if isa(this.set, 'struct')
    in.tag = strrep(in.tag, '@', [num2str(this.set.id) filesep]);
end

% --- Output
out = [this.Files in.tag '.' in.ext];

