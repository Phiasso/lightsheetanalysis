classdef Focus<handle
%FOCUS is the 'Neurofish' Focus.
    
    % --- PROPERTIES ------------------------------------------------------
    properties
        
        date = '';
        run = NaN;
        name = '';
        
        Data = '';
        Images = '';
        Files = NaN;
        Figures = NaN;
        Movies = NaN;
        IP = struct();
        
        dx = NaN;
        dy = NaN;
        exposure = NaN;
        dt = NaN;
        
        units = struct();
        sets = NaN;
        set = NaN;
           
    end
    
    % --- METHODS ---------------------------------------------------------
    methods
        
        % _________________________________________________________________
        function this = Focus(date, run)
        %Focus::constructor
        
            % --- Definitions
            proj = ML.Projects.select;
            this.date = date;
            this.run = run;  
            this.name = [this.date ' (' num2str(this.run, '%02i') ')'];
            this.Data = [proj.path 'Data' filesep this.date filesep 'Run ' num2str(this.run, '%02i') filesep];
            
            % Check existence
            if ~exist(this.Data, 'dir')
                warning('Focus:Data', 'No data found for date=%s and run=%i.', this.date, this.run);
            end
            
            % --- Set the Folders
            this.Images = [this.Data 'Images' filesep];
            this.Files = [this.Data 'Files' filesep];
            this.Figures = [proj.path 'Figures' filesep];
            this.Movies = [proj.path 'Movies' filesep];
            
            % --- Load the configuration file
            Conf = this.matfile('Config');
            
            if Conf.exist
                
                config = Conf.load();
                this.dx = config.dx;
                this.dy = config.dy;
                this.dt = config.dt;
                this.exposure = config.exposure;
                this.units = config.units;
                this.IP = config.IP;
                this.sets = config.sets;
                
                % --- Default selection
                this.select(1);
                
            else
                fprintf('\n');
                ML.cprintf([1 0.5 0], '<strong>WARNING</strong>: No configuration file detected for %s.', this.name);
                fprintf('\n');
            end
            
        end
    end
end
