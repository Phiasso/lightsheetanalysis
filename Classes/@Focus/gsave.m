function gsave(this, tag, varargin)
%FOCUS.GSAVE save Graph (figure) in the Figures directory.
%*  FOCUS.GSAVE(TAG) saves the curent figure in the TAG file in the
%   Focus.Figures directory.
%
%[  Note: A TAG composed of tokens separated by the filesystem separator
%   will save the file in subfolders. TAG supports both '@' and '@@'
%   shortcuts, See FOCUS.GNAME. ]
%
%*  See also: Focus, Focus.gname, Focus.gload.

% === Input variables =====================================================

in = inputParser;
in.addRequired('tag', @ischar);
in.addOptional('fig', gcf, @isnumeric);
in.addParamValue('type', 'fig', @(x) ischar(x) | iscell(x));
in.addParamValue('renderer', '', @ischar);
in.addParamValue('crop', true, @islogical);
in.addParamValue('quiet', false, @islogical);

in.parse(tag, varargin{:});
in = in.Results;

% =========================================================================

% --- Renderer
% 'zbuffer' : Fixes the Warning RGB bug (rasterize vetorial formats)
% 'OpenGL' : Enable transparency
if ~isempty(in.renderer)
    try
        set(in.fig, 'renderer', in.renderer);
    catch
        warning('FOCUS.GSAVE:RENDERER', 'Error while trying to set the renderer.');
    end
end

% --- Get the file name
fname = this.gname(in.tag);

% --- Create the directory (if needed)
d = fileparts(fname);
if ~exist(d, 'dir')
    fprintf('→ Creating folder : %s\n', d);
    mkdir(d);
end

% --- Save the figure

% Types
if ischar(in.type), in.type = {in.type}; end

% .fig
if ismember('fig', in.type)
    Visu.Plotbar.rmplotbar(in.fig);
    saveas(in.fig, [fname '.fig'], 'fig');
    Visu.Plotbar.plotbar(in.fig);
end

% .pdf
if ismember('pdf', in.type)
    if in.crop
        ps = get(gcf, 'PaperSize');
        pp = get(gcf, 'PaperPosition');
        set(gcf, 'PaperSize', pp(3:4));
        set(gcf, 'PaperPosition', [0 0 pp(3:4)]);
    end
    print(in.fig, [fname '.pdf'], '-dpdf');
    if in.crop
        set(gcf, 'PaperSize', ps);
        set(gcf, 'PaperPosition', pp);
    end
end

% .eps
if ismember('eps', in.type)
    print(in.fig, [fname '.eps'], '-depsc2', '-r500');
end

% .png
if ismember('png', in.type)
    print(in.fig, [fname '.png'], '-dpng', '-r300');
end

% --- Confirmation
if ~in.quiet
    Text.msg(fname, 'title', 'Figure saved', 'icon', 'save');
end

