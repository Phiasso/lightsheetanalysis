% 2018-05-30 - Convert data from selected run to Fishualizer-compatible data.
% CMTK was performed on transformed stack : rotated -90° + mirrored.
% Assumes raw x-y coordinates of neurons are toPosterior-toRight

clear
close all
clc

% Path parameters
% ---------------
root = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/';
atlas_xform = '/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/Warp_Atlas_On_LJP';
experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Fish_list_interleaved.txt';

% Will mirror y coordinates to take into account the mirror in the 1P
% setup.
mirror = 'y';	% set to 'n' to not mirror

skip_existing = 'y';    % if 'n', overwrite existing file

% What to save
save_coordinates = 'y'; % raw, registered on LJP reference brain and registered on ZBrain Atlas
save_labels = 'y';      % regions names from Zbrain Atlas
save_stim = 'y';        % stimulus trace
save_dff = 'y';         % DF/F of all neurons
save_spikes = 'y';      % infered spikes with Tubiana's BSD

% Specify Run
% -----------
study = 'Thermotaxis';

% Load list of experiments
list = readtable(experiment_list, 'DatetimeType', 'text');
n_exp = size(list, 1);

for idx_dates = 1:n_exp
    
    % Read date and runs list
    dat = list.Date{idx_dates};
    
    if isa(list.RunNumber, 'double')
        % Handles when only one day in a day and runs list is read as an
        % array directly
        runs = list.RunNumber(idx_dates);
        Tsets = list.Tset(idx_dates);
    else
        runs = eval(['[' list.RunNumber{idx_dates} ']']);
        Tsets = eval(['[' list.Tset{idx_dates} ']']);
    end
    
    % Specify manually dates and run
    % dat = '2018-01-10';
    % run = 4;
    % Tset = 13;
    
    for idx_run = 1:numel(runs)
        
        run = runs(idx_run);
        Tset = Tsets(idx_run);  % temperature setpoint in the experiment
        
        % Build path name where is the registration files
        reg_dir = [root 'Registration' filesep dat '_' sprintf('%02i', run) ...
            filesep];
        xform_dir = [reg_dir 'Transformations' filesep];
        xform_name_set1 = 'affine_grey_stack_1_LJP_mean_refbrain_01_ras';
        xform_name_set2 = 'affine_grey_stack_2_LJP_mean_refbrain_01_ras';
        
        % Build path name where is the calibration file
        stim_file = [root dat filesep 'Calibration' filesep 'calib_Tset=' num2str(Tset) '.mat'];
        
        % Build output HDF5 file
        d = dat;
        d(strfind(d, '-')) = [];    % remove hyphen
        output_dir = [root 'Datasets' filesep];
        output_file = [output_dir d '_Run' sprintf('%02i', run) 'Tset=' num2str(Tset) '.h5'];
        
        % Create directory and delete existing file if it exists
        if ~exist(output_dir, 'dir')
            mkdir(output_dir);
        end
        if exist(output_file, 'file')
            switch skip_existing
                case 'y'
                    continue
                case 'n'
                    delete(output_file);
                otherwise
                    fprintf('File existing, choose to overwrite or not (skip_existing)\n');
                    break
            end
        end
        
        % Get focus to access files
        F = getFocus([study filesep dat], run);
        n_layers = length(F.sets);      % number of layers
        
        fprintf('%s : creating HDF5 file\n', F.name);
                
        % Get neurons' coordinates
        % ------------------------
        switch save_coordinates
            case 'y'
                fprintf('Getting neurons'' coordinates...');
                tic
                
                % Split each set (outbound and return) into two arrays
                [c_xyz_set1, c_xyz_set2] = Store.getNeuronsCoordinates(F, true);
                
                switch mirror
                    case 'n'
                        % Mirror the coordinates in the y direction
                        c_xyz_set1(:, 2) = F.IP.height - c_xyz_set1(:, 2);
                        c_xyz_set2(:, 2) = F.IP.height - c_xyz_set2(:, 2);
                end
                
                % Rotate coordinates, warning, also rotation is also a mirror in
                % counterclockwise direction
                c_xyz_set1 = [c_xyz_set1(:, 2), c_xyz_set1(:, 1), c_xyz_set1(:, 3)];
                c_xyz_set2 = [c_xyz_set2(:, 2), c_xyz_set2(:, 1), c_xyz_set2(:, 3)];
                
                % Convert in microns
                c_xyz_set1(:, 1:2) = c_xyz_set1(:, 1:2).*F.dx;  % pix to µm
                c_xyz_set2(:, 1:2) = c_xyz_set2(:, 1:2).*F.dx;  % pix to µm
                
                % Concatenate all coordinates
                coordinates = vertcat(c_xyz_set1, c_xyz_set2);
                coordinates = coordinates*1e-3;                % µm to mm
                n_neurons = size(coordinates, 1);
                fprintf(' Done (%2.2fs).\n', toc);
                
                % Transform neuron's coordinates given CMTK registration
                % ------------------------------------------------------
                fprintf('Transforming neurons'' coordinates in LJP reference brain... ');
                tic
                %                 target = "/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/LJP_mean_refbrain_01_ras.nrrd";
                
                xformlist_set1 = [xform_dir xform_name_set1];
                %                 source_set1 = string(join([reg_dir 'Floatings' filesep 'grey_stack_1_ras.nrrd'], ''));
                reg_coord_set1 = Store.convertCoordinates(c_xyz_set1, xformlist_set1, 'inverse');
                
                xformlist_set2 = [xform_dir xform_name_set2];
                %                 source_set2 = string(join([reg_dir 'Floatings' filesep 'grey_stack_2_ras.nrrd'], ''));
                reg_coord_set2 = Store.convertCoordinates(c_xyz_set2, xformlist_set2, 'inverse');
                
                reg_coordinates = vertcat(reg_coord_set1, reg_coord_set2);
                reg_coordinates = reg_coordinates*1e-3;        % µm to mm
                fprintf(' Done (%2.2fs).\n', toc);
                
                % Transform neuron's registered coordinates on the Atlas Zbrain
                % -------------------------------------------------------------
                fprintf('Transforming neurons'' coordinates in ZBrain Atlas... ');
                tic
                %                 source = "/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/LJP_mean_refbrain_01_ras.nrrd";
                %                 target = "/home/ljp/Science/Projects/Neurofish/Data/Reference_Brains/ZBrain_Atlas.nrrd";
                atlas_coordinates = Store.convertCoordinates(reg_coordinates*1e3, atlas_xform);
                atlas_coordinates = atlas_coordinates.*1e-3;    % µm to mm
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Find in which brain regions are the neurons
        % -------------------------------------------
        switch save_labels
            case 'y'
                fprintf('Finding neurons'' labels...');
                tic
                labels = Store.findLabels(atlas_coordinates.*1e3);
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Get DF/F
        % --------
        switch save_dff
            case 'y'
                fprintf('Getting neurons'' DF/F...');
                tic
                [dff, time] = Store.getNeuronsDFF(F);
                n_times = size(dff, 2);
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Get stimulation trace
        % ---------------------
        switch save_stim
            case 'y'
                
                fprintf('Getting stimulation calibration...');
                tic;
                S = load(stim_file, 'param', 'trace', 'peak');
                % Interpolate to match time vector of DF/F
                temperature = interp1(S.trace.time, S.trace.temperature, time(1:n_times));
                temperature = temperature';
                
                temperature_bl = temperature;
                temperature_bl(isnan(temperature)) = [];
                % Fill values outside the stimulation period with a
                % baseline
                temperature(isnan(temperature)) = mean(temperature_bl(end-10:end));
                
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Get spikes
        % ----------
        switch save_spikes
            case 'y'
                fprintf('Getting neurons'' infered spikes...');
                tic
                [spikes, spikes_bin] = Store.getNeuronsSpikes(F);
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % Write data in HDF5 file
        % -----------------------------------------------------------------
        fprintf('Writing HDF5 file...');
        tic
        
        switch save_coordinates
            case 'y'
                h5create(output_file, '/Data/coordinates', [n_neurons 3]);
                h5write(output_file, '/Data/coordinates', coordinates);
                h5writeatt(output_file, '/Data/coordinates', 'Unit', 'mm');
                
                h5create(output_file, '/Data/coordinates_registered', [n_neurons 3]);
                h5write(output_file, '/Data/coordinates_registered', reg_coordinates);
                h5writeatt(output_file, '/Data/coordinates_registered', 'Unit', 'mm');
                h5writeatt(output_file, '/Data/coordinates_registered', 'ReferenceBrain', 'LJP_mean_refbrain_01_ras');
                
                h5create(output_file, '/Data/coordinates_atlas', [size(atlas_coordinates, 1) 3]);
                h5write(output_file, '/Data/coordinates_atlas', atlas_coordinates);
                h5writeatt(output_file, '/Data/coordinates_atlas', 'Unit', 'mm');
        end
        
        switch save_labels
            case 'y'
                h5create(output_file, '/Data/labels', size(labels));
                h5write(output_file, '/Data/labels', labels);
        end
        
        switch save_dff
            case 'y'
                h5create(output_file, '/Data/DFF', [n_neurons n_times]);
                h5write(output_file, '/Data/DFF', dff);
                
                h5create(output_file, '/Data/times', [1 n_times]);
                h5write(output_file, '/Data/times', time(1:n_times));
                h5writeatt(output_file, '/Data/times', 'Unit', 's');
                
                h5create(output_file, '/Data/times_real', [1 n_times*n_layers]);
                h5write(output_file, '/Data/times_real', time);
                h5writeatt(output_file, '/Data/times_real', 'Unit', 's');
        end
        
        switch save_spikes
            case 'y'
                h5create(output_file, '/Data/spikes', [n_neurons n_times]);
                h5write(output_file, '/Data/spikes', spikes);
                
                h5create(output_file, '/Data/spikes_bin', [n_neurons n_times]);
                h5write(output_file, '/Data/spikes_bin', spikes_bin);
        end
        
        switch save_stim
            case 'y'
                h5create(output_file, '/Data/stimulus', [size(temperature, 1) 1]);
                h5write(output_file, '/Data/stimulus', temperature);
                h5writeatt(output_file, '/Data/stimulus', 'Units', 'degCelcius');
                
                h5create(output_file, '/Data/stimulus_parameters', [size(S.param.onset, 1) 2]);
                h5write(output_file, '/Data/stimulus_parameters', [S.param.onset , S.param.duration]);
                h5writeatt(output_file, '/Data/stimulus_parameters', 'Units', 'Onsets(s)/Durations(s)');
                h5writeatt(output_file, '/Data/stimulus_parameters', 'Tset', num2str(Tset));
        end
        fprintf(' Done (%2.2fs).\n', toc);
        ML.CW.line;
    end
end