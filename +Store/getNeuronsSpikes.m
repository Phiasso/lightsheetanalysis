function [spikes, spikes_bin] = getNeuronsSpikes(F)
% Store.getNeuronsSpikes. Gather spikes from all neurons 
% from experiment F into a single array.
%
% INPUTS :
% ------
% F : focus object, focused on an experiment
%
% OUTPUTS :
% -------
% spikes : n_neurons x n_times array with inferred spikes,
% spikes_bin : n_neurons x n_times array with binarized inferred spikes.
%
% 2018-05-31 GLG

n_layers = length(F.sets);      % number of layers

spikes = [];
spikes_bin = [];

for layer = 1:n_layers
    
    F.select(layer);
    
    % Load neurons' DF/F
    signal = load(F.fname('@Spikes'));
    
    spikes = vertcat(spikes, signal.spikes);
    spikes_bin = vertcat(spikes_bin, signal.spikes_bin);
   
end