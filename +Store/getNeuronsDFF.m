function [dff, time] = getNeuronsDFF(F)
% Store.getNeuronsDFF. Gather DF/F from all neurons in experiment
%  F in a single matrix, along with time vector.
%
% INPUTS :
% ------
% F : Focus object, focused on an experiment.
%
% OUTPUTS :
% -------
% dff : (n_layers*n_neurons) x n_times array.
% time : 1 x (n_layers*n_times) array.
%
% 2018-05-31 GLG

n_layers = length(F.sets);      % number of layers
n_times = length(F.set.t);      % number of time steps

time = NaN(1, n_layers*n_times);
dff = [];

for layer = 1:n_layers
    
    F.select(layer);
    
    % Load neurons' DF/F
    signal = load(F.fname('@DFF'), 'neurons');
    dff = vertcat(dff, signal.neurons);
    
    % Load time vector
    t = load(F.fname('@Times'), 't');
    
    if mean(t.t) == 0
        time(1, ((layer-1)*n_times)+1:layer*n_times) = F.set.t*1e-3;
    else
        time(1, ((layer-1)*n_times)+1:layer*n_times) = t.t*1e-3;
    end
end

