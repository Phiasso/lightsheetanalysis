LightSheetAnalysis
------------------
Set of tools created to analyse & use zebrafish calcium imaging data from lightsheet microscope.
Documentation usually available at the beginning of each file.
All the MLab folder comes from Raphael Candelier (see http://candelier.fr/MLab/ ).