% 2018-06-11 - Create NRRD and perform registration for specified sets in
% specified folder. Made for interleaved stacks, where the outbound and the
%  return has been split (see cmtk_interleaved_preparation).

close all;
clear;
clc;

root = '/home/ljp/Science/Projects/Neurofish/Data/';
cmtk_dir = '/usr/local/lib/cmtk/bin/';      % where are CMTK binaries

% Directories
% -----------
study = 'Thermotaxis';
reg_dir = 'Registration';   % relative to study
dates_list_path = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Fish_list.txt';

% Name of folders containing tif images to register.
% Following names should be under root/study/reg_dir/date_run/ :
% stacks_list = {'grey_stack', 'Stimulus_P', 'Stimulus_N', 'Positive_Derivative_P', 'Positive_Derivative_N', ...
%     'Negative_Derivative_P', 'Negative_Derivative_N'};
stacks_list = {'response'};
n_stacks = numel(stacks_list);

% Stack to use to compute transformation
registered_stack = 'grey_stack';

% Reference brain
ref_brain = 'LJP_mean_refbrain_01_ras';
ref_brain_path = [root 'Reference_Brains' filesep ref_brain '.nrrd'];

% CMTK parameters
% ---------------
% (see command for CMTK settings)
warp = 'n';
create_nrrd = 'y';    % create NRRD from tiff stack
register = 'y';       % register grey stack on the reference brain
apply = 'y';          % apply CMTK transformation

% Read dates
dates_list = readtable(dates_list_path);
n_exp = size(dates_list, 1);

for idx_dates = 1:n_exp
    
    % Read date and runs list
    dat = char(dates_list.Date(idx_dates));
    runs = eval(['[' dates_list.RunNumber{idx_dates} ']']);
    
    % dat = '2018-01-10';
    % run = 4;
    % Tset = 13;
    
    for idx_run = 1:numel(runs)
        
        run = runs(idx_run);
        
        cwd = [root study filesep reg_dir filesep dat '_' sprintf('%02i', run) filesep];
        
        floatings_dir = ['Floatings' filesep];
        
        % -----------------------------------------------------------------
        % Create NRRD
        % -----------------------------------------------------------------
        switch create_nrrd
            case 'y'
                tic
                fprintf('% s - Run %i, building NRRD...', dat, run)
                % Get NRRD spec
                F = getFocus([study filesep dat], run);
                n_layers = length(F.sets);
                dx = F.dx;
                dy = F.dy;
                dz = 2.*F.sets(end).z/(n_layers - 1);
                param.exp_type = '';
                param.binning = [1 1];
                param.pix_size = [dx dy dz];        % cmtk space is right-anterior-superior
                param.rotate = -90;                 % clockwise rotation : '-', anticlockwise : '+'
                param.space_type = 'RAS';
                
                for stack = 1:n_stacks
                    
                    stack_name = stacks_list{stack};
                    
                    % Make two distinct nrrd file
                    for c = 1:2
                        in_folder = [cwd stack_name '_' num2str(c) filesep];
                        out_folder = [cwd floatings_dir];
                        out_name = [stack_name '_' num2str(c) '_ras'];
                        makeNrrdStack(in_folder, out_folder, out_name, param);
                    end
                    
                end
                
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % -----------------------------------------------------------------
        % Compute CMTK transformations
        % -----------------------------------------------------------------
        switch register
            case 'y'
                
                fprintf('Registering stack on reference brain...');
                tic
                
                % Directory where to save transformations
                transfo_path = [cwd 'Transformations' filesep];
                if ~exist(transfo_path, 'dir')
                    mkdir(transfo_path);
                end
                
                for c = 1:2
                    
                    % Build path names
                    affine_path = [transfo_path 'affine_'  registered_stack '_' num2str(c) '_' ref_brain filesep];
                    warp_path = [transfo_path 'warp_'  registered_stack '_' num2str(c) '_' ref_brain filesep];
                    % Create directories if necessary
                    if ~exist(affine_path, 'dir')
                        mkdir(affine_path);
                    end
                    switch warp
                        case 'y'
                            if ~exist(warp_path, 'dir')
                                mkdir(warp_path);
                            end
                    end
                    
                    % Define CMTK inputs
                    float_name = [cwd floatings_dir registered_stack '_' num2str(c) '_ras.nrrd'];

                    % === Rigid registration ===
                    % Define CMTK command
                    stacks = ['-o ' '"' affine_path '"'  ' ' '"' ref_brain_path '"' ' ' '"' float_name '"' ];
                    % Options from Bertoni (leave a space at the end of the string)
                    options = 'registration -i -v --coarsest 25.6 --sampling 3.2 --omit-original-data --exploration 25.6 --dofs 6 --dofs 9 --accuracy 3.2 ';
                    command = [cmtk_dir options stacks];
                    
                    % Launch CMTK
                    unix(command, '-echo');
                    
                    % === Non-rigid registration ===
                    switch warp
                        case 'y'
                            % Define CMTK command
                            stacks = ['-o ' '"' warp_save '"' ' ./' '"' affine_save '"'];
                            % Options from Burgess lab (leave a space at the end of the string)
                            options = 'warpx --fast --grid-spacing 100 --smoothness-constraint-weight 1e-1 --grid-refine 2 --min-stepsize 0.25 --adaptive-fix-thresh 0.25 ';
                            command = [cmtk_dir options stacks];
                            
                            % Launch CMTK
                            unix(command, '-echo');
                    end
                end
                
                fprintf(' Done (%2.2fs).\n', toc);
        end
        
        % -----------------------------------------------------------------
        % Apply transformations
        % -----------------------------------------------------------------
        switch apply
            case 'y'
                
                fprintf('Applying transformations...');
                tic
                % Following folders should already exists and contain
                % transformations
                transfo_path = [cwd 'Transformations' filesep];
                
                for stack = 1:n_stacks
                    
                    stack_name = stacks_list(stack);
                    
                    for c = 1:2
                        
                        affine_save = [transfo_path 'affine_' registered_stack '_' num2str(c) '_' ref_brain];
                        warp_save = [transfo_path 'warp_' registered_stack '_' ref_brain];
                        
                        % Define CMTK inputs
                        float_name = [cwd floatings_dir stack_name '_' num2str(c) '_ras.nrrd'];
                        
                        % Define CMTK outputs
                        reformated_stack_path = [cwd stack_name '_' num2str(c) '_' ref_brain '.nrrd'];
                        
                        % Define CMTK command
                        switch warp
                            case 'y'
                                command = ['"' cmtk_dir 'reformatx" --pad-out 0 -o ' '"' reformated_stack_path '"' ' --floating ' '"' float_name '"' ' ' '"' ref_brain_path '"' ' ' '"' warp_save '"'];
                            otherwise
                                command = ['"' cmtk_dir 'reformatx" --pad-out 0 -o ' '"' reformated_stack_path '"' ' --floating ' '"' float_name '"' ' ' '"' ref_brain_path '"' ' ' '"' affine_save '"'];
                        end
                        
                        % Launch CMTK
                        command = char(join(command, ''));
                        unix(command, '-echo');
                    end
                end
                fprintf(' Done (%2.2fs).\n', toc);
        end
    end
end