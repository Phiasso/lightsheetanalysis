function [] = register()

% Fill default values
% -------------------
cmtk_dir = '/usr/local/lib/cmtk/bin/';      % where is CMTK binaries

% Ask user to use non-rigid registration or not
% ---------------------------------------------
warp = inputdlg({'Use non-rigid registration [y/n]:'}, 'CMTK');
warp = warp{1};

% Get folders
% -----------
% Reference brain
[ref_brain_name, ref_brain_path] = uigetfile([pwd filesep '*.nrrd'], 'Choose the reference brain');
ref_full_path = [ref_brain_path filesep ref_brain_name];

% Floating brain
[flo_brain_name, flo_brain_path] = uigetfile([pwd filesep '*.nrrd'], 'Choose the floating brain');
flo_full_path = [flo_brain_path flo_brain_name];

% Where to save transformations
transfo_dir = uigetdir(flo_brain_path, 'Choose where to save transformations');
transfo_dir_affine = [transfo_dir filesep 'Affine'];
transfo_dir_warp = [transfo_dir filesep 'Warp'];

% Rigid transformations calculation
% ---------------------------------
stacks = ['-o ' '"' transfo_dir_affine '"'  ' ' '"' ref_full_path '"' ' ' '"' flo_full_path '"'];
% Options from Bertoni (leave a space at the end of the string)
options = 'registration -i -v --coarsest 25.6 --sampling 3.2 --omit-original-data --exploration 25.6 --dofs 6 --dofs 9 --accuracy 3.2 ';
command = [cmtk_dir options stacks];
unix(command, '-echo');

% Non-rigid transformations calculation
% -------------------------------------
switch warp
    case 'y'
        stacks = ['-o ' '"' transfo_dir_warp '"' ' ' '"' transfo_dir_affine '"'];
        % Options from Burgess lab (leave a space at the end of the string)
        options = 'warpx --fast --grid-spacing 100 --smoothness-constraint-weight 1e-1 --grid-refine 2 --min-stepsize 0.25 --adaptive-fix-thresh 0.25 ';
        command = [cmtk_dir options stacks];
        unix(command, '-echo');
end
end