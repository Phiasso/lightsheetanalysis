function [] = createNrrd(param)

% Check input and fill with default values
% ----------------------------------------
if ~isfield(param, 'pix_size')
    param.pix_size = [.4 .4 .4];        % [dx dy dz] in µm
end
if ~isfield(param, 'exp_type')
    param.exp_type = '';
end
if ~isfield(param, 'binning')
    param.binning = [1 1];              % x and y binning factor
end
if ~isfield(param, 'rotate')
    param.rotate = 0;                   % rotation in degree
end
if ~isfield(param, 'space_type')
    param.space_type = 'RAS';           % space orientation (rostro-anterior...)
end

% Get folders from user
% ---------------------
in_stack_dir = uigetdir(pwd, 'Select TIFF images folder');
[out_filename, out_pathname] = uiputfile(in_stack_dir, 'Choose output NRRD name');

% Create the NRRD stack from TIFF
% -------------------------------
makeNrrdStack(in_stack_dir, out_pathname, out_filename, param);
end