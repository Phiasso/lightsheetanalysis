function stop(this)
%ML.CW.CLI/stop Stop CLI engine
%   ML.CW.CLI/stop() stops the Command-Line Interface.
%
%   See also ML.CW.CLI, ML.CW.CLI/start
%
%   More on <a href="matlab:ML.doc('ML.CW.CLI.stop');">ML.doc</a>

this.quit = true;

%! ------------------------------------------------------------------------
%! Author: Raphaël Candelier
%! Version: 1.0
%
%! Revisions
%   1.0     (2015/04/02): Initial version.
%
%! ------------------------------------------------------------------------
%! Doc
%   <title>To do</title>
