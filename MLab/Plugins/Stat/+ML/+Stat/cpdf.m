function out = cpdf(val, varargin)
%STAT.CPDF Cumulative probability distribution function
%*  P = STAT.CPDF(VALUES) computes the cpdf of the vector VALUES. It 
%   returns a structure P containing the following fields: 'bin', 'cpdf'.
%
%*  See also: Stat.pdf.

% === Input variables =====================================================

in = inputParser;
in.addRequired('val', @isnumeric);

in.parse(val, varargin{:});
in = in.Results;

% =========================================================================

% --- Process
[f, x] = ecdf(in.val);

% --- Output
x = x(2:end);
f = 1-f(2:end);
f = f/trapz(x, f);

out = struct('bin', x, ...
             'cpdf', f);



