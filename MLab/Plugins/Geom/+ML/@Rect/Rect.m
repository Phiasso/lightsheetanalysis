classdef Rect<handle
%ML.Rect Rectangle class (ML.Geom plugin).
    
    % --- PROPERTIES ------------------------------------------------------
    properties
        
        x1 = NaN;
        x2 = NaN;
        y1 = NaN;
        y2 = NaN;

    end
    
    % --- METHODS ---------------------------------------------------------
    methods
        
        % _________________________________________________________________
        function this = Rect(x1, x2, y1, y2)
        %ML.Geom.Rect Constructor
        
            % --- Inputs
            if nargin==1 && numel(x1)==4
                x2 = x1(2);
                y1 = x1(3);
                y2 = x1(4);
                x1 = x1(1);
            end
            
            % --- Assignment
            this.x1 = min(x1,x2);
            this.x2 = max(x1,x2);
            this.y1 = min(y1,y2);
            this.y2 = max(y1,y2);
            
        end
        
    end
end