classdef XML<handle
%ML.XML [XML plugin] The MLab XML class.
    
    % --- PROPERTIES ------------------------------------------------------
    properties (Access = private)
        
        tags = struct('parent', {}, ...
            'name', {}, ...
            'type', {}, ...
            'content', {}, ...
            'options', {});
        
        left = '<';
        right = '>';
        ender = '/';
        pi = '?';
        increment = '    ';
        
    end
        
    % --- METHODS ---------------------------------------------------------
    methods
        
        % _________________________________________________________________
        function this = XML(varargin)
        %ML.XML Constructor
            
            % === Inputs ==================================================
            
            in = ML.Input(varargin{:});
            in.addParamValue('xml_tag', true, @islogical);
            in = +in;
            
            % =============================================================
        
            % --- Xml tag
            if in.xml_tag
                this.tags(1).parent = 0;
                this.tags(1).type = 'pi';
                this.tags(1).name = 'xml';
                this.tags(1).content = '';
                this.tags(1).options = struct('version', '1.0', 'encoding', 'UTF-8');
            end
            
        end

    end
    
end