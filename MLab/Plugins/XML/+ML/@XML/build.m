function out = build(this)
%ML.XML.build [XML plugin] Build the XML tree
%   ML.XML.BUILD() builds the XML object's tree and displays it.
%
%   OUT = ML.XML.BUILD() returns a XML string.
%
%   See also ML.XML, ML.XML.show.

str = '';
inc = 0;

% --- Find root children
rc = find([this.tags(:).parent]==0);

% --- Recursive build over root children
for i = 1:numel(rc)
    rec_build(rc(i));    
end

% --- Output

% Remove final EOLs
while strcmp(str(end), char(10))
    str = str(1:end-1);
end

if nargout
    out = str;
else
    disp(str);
end

% === Nested functions ====================================================

    function rec_build(id)

        t = this.tags(id);
        
        % --- Convert options struct to cell
        if ~isempty(t.options) && isstruct(t.options)
            fopt = fieldnames(t.options);
            options = {};
            for j = 1:numel(fopt)
                options{end+1} = fopt{j};
                options{end+1} = t.options.(fopt{j});
            end
            t.options = options;
        end
        
        switch t.type
            
            case 'cont'
                
                % Opening sequence
                str = [str repmat(this.increment, [1 inc]) this.left t.name];
                if ~isempty(t.options)
                    for j = 1:2:numel(t.options)
                        str = [str ' ' t.options{j} '="' t.options{j+1} '"'];
                    end
                end
                str = [str this.right char(10)];
                
                % Content
                if numel(t.content)
                    inc = inc + 1;
                    for j = 1:numel(t.content)
                        
                        if ismember(this.tags(t.content(j)).type, {'cont'})
                            
                            rec_build(t.content(j));
                            
                        else
                            
                            str = [str repmat(this.increment, [1 inc])];
                            rec_build(t.content(j));                            
                            str = [str char(10)];
                            
                        end
                        
                    end
                    inc = inc - 1;
                end
                
                % Closing sequence
                str = [str repmat(this.increment, [1 inc]) this.left this.ender t.name this.right char(10)];
                
            case 'tag'
                
                % Opening sequence
                str = [str this.left t.name];
                if ~isempty(t.options)
                    for j = 1:2:numel(t.options)
                        str = [str ' ' t.options{j} '="' t.options{j+1} '"'];
                    end
                end
                str = [str this.right];
                
                % Content
                if numel(t.content)
                    for j = 1:numel(t.content)
                       rec_build(t.content(j)); 
                    end
                end
                
                % Closing sequence
                str = [str this.left this.ender t.name this.right];
                
            case 'utag'
                
                str = [str this.left t.name];
                if ~isempty(t.options)
                    for j = 1:2:numel(t.options)
                        str = [str ' ' t.options{j} '="' t.options{j+1} '"'];
                    end
                end
                str = [str ' ' this.ender this.right];
                
            case 'text'
                
                str = [str t.content];
                
            case 'comment'
                
                str = [str '<!-- ' t.content ' -->'];
                
            case 'cdata'
                
                str = [str '<![CDATA[' t.content ']]>'];
                
            case 'pi'
                
                % Opening sequence
                str = [str this.left this.pi t.name];
                
                % Content
                if ~isempty(t.content)
                    str = [str ' ' t.content];
                end
                
                % Closing sequence
                str = [str ' ' this.pi this.right];
                
                % Root child special case
                if ~t.parent
                    str = [str char(10)];
                end
                
        end
                
    end

end