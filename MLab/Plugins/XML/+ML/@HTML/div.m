function out = div(this, varargin)
%ML.HTML.div Add a <div> element to the HTML object.
%   ML.HTML.DIV() add a <div> element to the HTML object.
%
%   ML.HTML.DIV(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.DIV(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.DIV(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'div', 'options', notin);

% --- Output
if nargout
    out = id;
end