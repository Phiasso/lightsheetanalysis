function out = footer(this, varargin)
%ML.HTML.footer Add a <footer> element to the HTML object.
%   ML.HTML.FOOTER() add a <footer> element to the HTML object.
%
%   ML.HTML.FOOTER(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.FOOTER(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.FOOTER(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'footer', 'options', notin);

% --- Output
if nargout
    out = id;
end