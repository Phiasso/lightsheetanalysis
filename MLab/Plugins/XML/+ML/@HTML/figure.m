function out = figure(this, varargin)
%ML.HTML.figure Add a <figure> element to the HTML object.
%   ML.HTML.FIGURE() add a <figure> element to the HTML object.
%
%   ML.HTML.FIGURE(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.FIGURE(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.FIGURE(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'figure', 'options', notin);

% --- Output
if nargout
    out = id;
end