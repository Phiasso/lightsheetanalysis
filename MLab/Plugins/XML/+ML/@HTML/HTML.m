classdef HTML<ML.XML
%ML.HTML [XML plugin] The MLab HTML class.
    
    % --- PROPERTIES ------------------------------------------------------
    
    properties (Access = private)
        
        filename = tempname;
    
    end
    
    properties
        
        html = NaN;
        head = NaN;
        body = NaN;
        
        parent = NaN;
        
    end
    
    % --- METHODS ---------------------------------------------------------
    methods
        
        % _________________________________________________________________
        function this = HTML()
        %ML.HTML Constructor
            
            % --- Call the parent's constructor
            this = this@ML.XML('xml_tag', false);
            
            % --- Basic HTML structure
            this.html = this.add(0, 'cont', 'html');
            
            this.head = this.add(this.html, 'cont', 'head');
            this.body = this.add(this.html, 'cont', 'body');
            
            this.parent = this.body;
            
        end

    end
    
end