function id = base(this, varargin)
%ML.HTML.base Add a <base> element to the HTML object.
%   ML.HTML.BASE(HREF) add a <base> element to the HTML object containing
%   the HREF string. The parent of the base element is the HTML header.
%
%   ID = ML.HTML.BASE(...) returns the identifier of the base element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addRequired('href', @ischar);
[in, notin] = +in;

% =========================================================================

notin = ['href' ; in.href ; notin];
id = this.add(this.head, 'utag', 'base', 'options', notin);

this.add(id, 'text', in.title);