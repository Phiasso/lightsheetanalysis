function out = wbr(this, varargin)
%ML.HTML.wbr Add a <wbr> element to the HTML object.
%   ML.HTML.WBR() add a <wbr> element to the HTML object.
%
%   ML.HTML.WBR(POSITION) specifies the position of the element. The default
%   position is given by the HTML object property 'parent'.
%
%   ID = ML.HTML.WBR(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
in = +in;

% =========================================================================

id = this.add(in.position, 'utag', 'wbr');

% --- Output
if nargout
    out = id;
end