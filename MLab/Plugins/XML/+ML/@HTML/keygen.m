function out = keygen(this, varargin)
%ML.HTML.keygen Add an <keygen> element to the HTML object.
%   ML.HTML.KEYGEN() add an <keygen> element to the HTML object.
%
%   ML.HTML.KEYGEN(NAME) specifies the name of the element.
%
%   ML.HTML.KEYGEN(POSITION, NAME) also specifies the position of the 
%   element.
%
%   ML.HTML.KEYGEN(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.KEYGEN(...) returns the identifiers of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('name', @ischar);
[in, notin] = +in;

% =========================================================================

notin = ['name' ; in.name; notin];
id = this.add(in.position, 'utag', 'keygen', 'options', notin);

% --- Outputs
if nargout
    out = id;
end