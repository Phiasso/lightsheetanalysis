function out = br(this, varargin)
%ML.HTML.br Add a <br> element to the HTML object.
%   ML.HTML.BR() add a <br> element to the HTML object.
%
%   ML.HTML.BR(POSITION) specifies the position of the element. The default
%   position is given by the HTML object property 'parent'.
%
%   ID = ML.HTML.BR(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
in = +in;

% =========================================================================

id = this.add(in.position, 'utag', 'br');

% --- Output
if nargout
    out = id;
end