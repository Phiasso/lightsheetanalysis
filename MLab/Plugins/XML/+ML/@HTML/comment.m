function id = comment(this, varargin)
%ML.HTML.comment Add a comment to the HTML object.
%   ML.HTML.COMMENT(TEXT) add the comment in the string TEXT to the HTML 
%   object.
%
%   ML.HTML.COMMENT(POSITION, TEXT) specifies the xml position of the new 
%   element.  The default position is given by the object property 
%   'parent'.
%
%   ID = ML.HTML.COMMENT(...) returns the identifier of the comment.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
in = +in;

% =========================================================================

id = this.add(in.position, 'comment', in.text);