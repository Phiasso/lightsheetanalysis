function out = ol(this, varargin)
%ML.HTML.ol Add a <ol> element to the HTML object.
%   ML.HTML.OL() add a <ol> element to the HTML object.
%
%   ML.HTML.OL(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.OL(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.OL(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'ol', 'options', notin);

% --- Output
if nargout
    out = id;
end