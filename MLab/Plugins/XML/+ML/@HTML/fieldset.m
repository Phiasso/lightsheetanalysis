function out = fieldset(this, varargin)
%ML.HTML.fieldset Add a <fieldset> element to the HTML object.
%   ML.HTML.FIELDSET() add a <fieldset> element to the HTML object.
%
%   ML.HTML.FIELDSET(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.FIELDSET(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.FIELDSET(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'fieldset', 'options', notin);

% --- Output
if nargout
    out = id;
end