function out = tbody(this, varargin)
%ML.HTML.tbody Add a <tbody> element to the HTML object.
%   ML.HTML.TBODY() add a <tbody> element to the HTML object.
%
%   ML.HTML.TBODY(POSITION) specifies the position of the element. 
%   The default position is given by the object property 'parent'.
%
%   ML.HTML.TBODY(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.TBODY(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'tbody', 'options', notin);

% --- Output
if nargout
    out = id;
end