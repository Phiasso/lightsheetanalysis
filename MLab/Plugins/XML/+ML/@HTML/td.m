function [out1, out2] = td(this, varargin)
%ML.HTML.td Add a <td> element to the HTML object.
%   ML.HTML.TD() add a <td> element to the HTML object.
%
%   ML.HTML.TD(TEXT) specifies the text content of the element.
%
%   ML.HTML.TD(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.TD(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.TD(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addOptional('text', '', @ischar);
[in, notin] = +in;

% =========================================================================

if isempty(in.text)

    id = this.add(in.position, 'tag', 'td', 'options', notin);

    % --- Outputs
    if nargout
        out1 = id;
    end
    
else
    
    id = this.add(in.position, 'tag', 'td', 'options', notin);
    id_txt = this.add(id, 'text', in.text);

    % --- Outputs
    if nargout
        out1 = id;
        out2 = id_txt;
    end
end