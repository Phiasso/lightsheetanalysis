function out = iframe(this, varargin)
%ML.HTML.iframe Add an <iframe> element to the HTML object.
%   ML.HTML.IFRAME() add an <iframe> element to the HTML object.
%
%   ML.HTML.IFRAME(SOURCE) specifies the source of the element.
%
%   ML.HTML.IFRAME(POSITION, SOURCE) also specifies the position of the 
%   element.
%
%   ML.HTML.IFRAME(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.IFRAME(...) returns the identifiers of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('src', @ischar);
[in, notin] = +in;

% =========================================================================

notin = ['src' ; in.src; notin];
id = this.add(in.position, 'utag', 'iframe', 'options', notin);

% --- Outputs
if nargout
    out = id;
end