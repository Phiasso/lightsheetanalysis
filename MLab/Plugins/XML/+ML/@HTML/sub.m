function [out1, out2] = sub(this, varargin)
%ML.HTML.sub Add a <sub> element to the HTML object.
%   ML.HTML.SUB() add a <sub> element to the HTML object.
%
%   ML.HTML.SUB(TEXT) specifies the text content of the element.
%
%   ML.HTML.SUB(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.SUB(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.SUB(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'sub', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end