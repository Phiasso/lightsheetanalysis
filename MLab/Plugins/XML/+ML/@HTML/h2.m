function out = h2(this, varargin)
%ML.HTML.h2 Add a <h2> element to the HTML object.
%   ML.HTML.H2() add a <h2> element to the HTML object.
%
%   ML.HTML.H2(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.H2(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.H2(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'h2', 'options', notin);

% --- Output
if nargout
    out = id;
end