function out = th(this, varargin)
%ML.HTML.th Add a <th> element to the HTML object.
%   ML.HTML.TH() add a <th> element to the HTML object.
%
%   ML.HTML.TH(POSITION) specifies the position of the element. 
%   The default position is given by the object property 'parent'.
%
%   ML.HTML.TH(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.TH(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'th', 'options', notin);

% --- Output
if nargout
    out = id;
end