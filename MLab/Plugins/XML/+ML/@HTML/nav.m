function out = nav(this, varargin)
%ML.HTML.nav Add a <nav> element to the HTML object.
%   ML.HTML.NAV() add a <nav> element to the HTML object.
%
%   ML.HTML.NAV(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.NAV(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.NAV(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'nav', 'options', notin);

% --- Output
if nargout
    out = id;
end