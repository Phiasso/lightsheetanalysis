function out = aside(this, varargin)
%ML.HTML.aside Add a <aside> element to the HTML object.
%   ML.HTML.ASIDE() add a <aside> element to the HTML object.
%
%   ML.HTML.ASIDE(POSITION) specifies the position of the element. The 
%   default position is given by the object property 'parent'.
%
%   ML.HTML.ASIDE(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.ASIDE(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'aside', 'options', notin);

% --- Output
if nargout
    out = id;
end