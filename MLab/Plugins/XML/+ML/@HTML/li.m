function [out1, out2] = li(this, varargin)
%ML.HTML.li Add a <li> element to the HTML object.
%   ML.HTML.LI() add a <li> element to the HTML object.
%
%   ML.HTML.LI(TEXT) specifies the text content of the element.
%
%   ML.HTML.LI(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.LI(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.LI(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'li', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end