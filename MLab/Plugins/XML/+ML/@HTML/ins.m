function [out1, out2] = ins(this, varargin)
%ML.HTML.ins Add a <ins> element to the HTML object.
%   ML.HTML.INS() add a <ins> element to the HTML object.
%
%   ML.HTML.INS(TEXT) specifies the text content of the element.
%
%   ML.HTML.INS(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.INS(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.INS(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML, ML.HTML.del.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'ins', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end