function show(this, varargin)
%ML.HTML.show [HTML plugin] Show the HTML document
%   ML.HTML.SHOW() builds the HTML tree and displays it in the browser.
%
%   ML.HTML.BUILD(HTML) shows the HTML code in the browwser.
%
%   See also ML.HTML, ML.XML.build.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('html', this.build, @ischar);
in = +in;

% =========================================================================

% Write to temporary file
fid = fopen(this.filename, 'w');
fprintf(fid, in.html);
fclose(fid);

% Display content
web(this.filename);