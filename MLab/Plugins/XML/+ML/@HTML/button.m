function [out1, out2] = button(this, varargin)
%ML.HTML.button Add a <button> element to the HTML object.
%   ML.HTML.BUTTON() add a <button> element to the HTML object.
%
%   ML.HTML.BUTTON(TEXT) specifies the text content of the element.
%
%   ML.HTML.BUTTON(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.BUTTON(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.BUTTON(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'button', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end