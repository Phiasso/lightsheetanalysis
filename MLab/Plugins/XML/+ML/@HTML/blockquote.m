function out = blockquote(this, varargin)
%ML.HTML.blockquote Add a <blockquote> element to the HTML object.
%   ML.HTML.BLOCKQUOTE() add a <blockquote> element to the HTML object.
%
%   ML.HTML.BLOCKQUOTE(POSITION) specifies the position of the element. The
%   default position is given by the object property 'parent'.
%
%   ML.HTML.BLOCKQUOTE(..., 'opt', OPT, ...) specifies the element's 
%   options.
%
%   ID = ML.HTML.BLOCKQUOTE(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'blockquote', 'options', notin);

% --- Output
if nargout
    out = id;
end