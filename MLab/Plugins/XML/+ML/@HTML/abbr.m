function [out1, out2] = abbr(this, varargin)
%ML.HTML.abbr Add an <abbr> element to the HTML object.
%   ML.HTML.ABBR() add an <abbr> element to the HTML object.
%
%   ML.HTML.ABBR(TITLE, TEXT) specifies the title property and the text
%   content of the element.
%
%   ML.HTML.ABBR(POSITION, TITLE, TEXT) also specifies the position of the 
%   element.
%
%   ML.HTML.ABBR(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.ABBR(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('href', @ischar);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

notin = ['href' ; in.href ; notin];
id = this.add(in.position, 'tag', 'abbr', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end