function set_status(this, i, j, status)
%ML.Steps.set_status Steps mutator for the 'status' property.
%   ML.STEPS.SET_STATUS(ELM, STEP, STATUS) sets the 'status' property at
%   STATUS for element ELM and step STEP. ELM and STEP can be either
%   numerical indexes of strings.
%
%   See also ML.Steps, ML.Steps.set_elms, ML.Steps.set_steps.

% --- Element
if ischar(i)
    I = ismember(this.elms, i);
    switch nnz(I)
        case 0
            warning('ML.Steps.status',  ['No element found with name ''' i '''. Aborting.']);
            return
        case 1
            i = find(I);
        otherwise
            warning('ML.Steps.status',  'This should NOT occur');
            return
    end
end

% --- Step
if ischar(j)
    I = ismember(this.steps, j);
    switch nnz(I)
        case 0
            warning('ML.Steps.status',  ['No step found with name ''' j '''. Aborting.']);
            return
        case 1
            j = find(I);
        otherwise
            warning('ML.Steps.status',  'This should NOT occur');
            return
    end
end

this.status{i,j} = status;