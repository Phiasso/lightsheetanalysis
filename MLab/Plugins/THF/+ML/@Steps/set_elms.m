function set_elms(this, elms)
%ML.Steps.set_elms Steps mutator for the 'elms' property.
%   ML.STEPS.SET_ELMS(ELMS) replaces the 'elms' property by the value of
%   ELMS.
%
%   See also ML.Steps, ML.Steps.set_steps.

this.elms = elms;

if size(this.status,1)<numel(this.elms)
    this.status{numel(this.elms),1} = [];
elseif size(this.status,1)>numel(this.elms)
    this.status = this.status(1:numel(this.elms),:);
end