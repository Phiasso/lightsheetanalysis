function set_steps(this, steps)
%ML.Steps.set_steps Steps mutator for the 'steps' property.
%   ML.STEPS.SET_STEPS(STEPS) replaces the 'steps' property by the value of
%   STEPS.
%
%   See also ML.Steps, ML.Steps.set_elms.

this.steps = steps;

if size(this.status,2)<numel(this.steps)
    this.status{1,numel(this.steps)} = [];
elseif size(this.status,2)>numel(this.steps)
    this.status = this.status(:,1:numel(this.steps));
end