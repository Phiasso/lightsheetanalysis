function [C, R] = circumcircle(X, Y, varargin)
%GEOM.CIRCUMCIRCLE compute the circumcircle of triangles
%*  [C, R] = GEOM.CIRCUMCIRCLE(X, Y) compute the circumcenters C and the 
%   circumradius R. X and Y are N-by-3 arrays ([x1, x2, x2], [y1, y2, y3]).
%
%*  CIRCUMCIRCLE(X, Y, W) uses the power distances with the weights W 
%   instead of the classical distances (corresponding to W=0). W is a
%   N-by-3 array.
%
%*  See also: wdelaunay.

% === Input variables =====================================================

in = inputParser;
in.addRequired('X', @isnumeric);
in.addRequired('Y', @isnumeric);
in.addOptional('W', zeros(size(X)), @isnumeric);
in.parse(X, Y, varargin{:});
in = in.Results;

X = in.X;
Y = in.Y;
W = in.W;

% =========================================================================


D = 2*(X(:,1).*(Y(:,2)-Y(:,3)) + ...
       X(:,2).*(Y(:,3)-Y(:,1)) + ...
       X(:,3).*(Y(:,1)-Y(:,2)));
 
C = [((X(:,1).^2+Y(:,1).^2-W(:,1).^2).*(Y(:,2)-Y(:,3)) + ...
      (X(:,2).^2+Y(:,2).^2-W(:,2).^2).*(Y(:,3)-Y(:,1)) + ...
      (X(:,3).^2+Y(:,3).^2-W(:,3).^2).*(Y(:,1)-Y(:,2)))./D ...
     ((X(:,1).^2+Y(:,1).^2-W(:,1).^2).*(X(:,3)-X(:,2)) + ...
      (X(:,2).^2+Y(:,2).^2-W(:,2).^2).*(X(:,1)-X(:,3)) + ...
      (X(:,3).^2+Y(:,3).^2-W(:,3).^2).*(X(:,2)-X(:,1)))./D];

R = sqrt((C(:,1)-X(:,1)).^2 + (C(:,2)-Y(:,1)).^2 - W(:,1).^2);