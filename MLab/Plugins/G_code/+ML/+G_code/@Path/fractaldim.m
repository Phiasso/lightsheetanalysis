function [df,FD] = shape2df(sh)
%SHAPE2DF extracts the structural fractal dimension of a shape
%*  DF = SHAPE2DF(SH) returns the structural fractal dimension.
%
%*  [DF,FD] = SHAPE2DF(SH) also returns the Feret Diameter of the shape.
%
%[  Note: See 'A random walk through fractal dimensions' (Brian Kaye - 1989) chap.2 
%   for a definition of the textural and structural fractal dimensions.  ]
%
%*  Exemples:
%|  >> shape2df({von_koch(5)})      % Should give log(4)/log(3) ~ 1.262
%|  ans =
%|    1.2946
%
%*  See also: cluster2shape, von_koch.

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%~ Author:	Raphael Candelier
%~ Place:	GIT/SPEC/IRAMIS/DSM/CEA
%~ Copyright:	Groupe Instabilites & Turbulence
%~ Version:	Gbox v3.2
%~ Last modified : Thursday 9-apr-2009 (14h 15m 44s) ~~~
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -- Get the Feret diameter
FD = max(pdist(sh{1}));
sh{1} = sh{1}/FD;

% -- Get the fractal dimension
lambda = logspace(-2,-1,50);
P = NaN(numel(lambda),1);

for i = 1:numel(lambda)
    edges = {min(sh{1}(:,1)):lambda(i):max(sh{1}(:,1))+lambda(i) ; ...
             min(sh{1}(:,2)):lambda(i):max(sh{1}(:,2))+lambda(i)};     
    P(i) = sum(sum(hist3(sh{1},'Edges',edges)>0));
end

% loglog(lambda,P,'b.-')
f = fit(vert(log(lambda)),vert(log(P)),'poly1');
df = -f.p1;
