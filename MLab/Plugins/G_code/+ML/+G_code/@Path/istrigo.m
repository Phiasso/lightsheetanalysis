function b = istrigo(P)
%ISTRIGO Determines if a polygon is trigonometrically orientated.
%
%*  ISTRIGO(P) returns 1 if the polygon P is trigonometrically orientated,
%   0 if the points are aligned and -1 otherwise. P is a N-by-2
%   polygon (N points in dimension 2).
%
%[  Note: Only works for triangles, i.e. N=3  ]
%
%*  See also: .

if size(P,1)~=3
    warning('is_trigo only works for triangles.');
    b = NaN;
    return
end

b = det([P(1,1)-P(2,1) P(3,1)-P(1,1) ; P(1,2)-P(2,2) P(3,2)-P(1,2)]);

if b~=0, b = -sign(b); end
    
