function out = iscrossing(P)
%ISCROSSING Checks if 2D polygon is crossing itself
%*  OUT = ISCROSSING(P) checks if polygon P is crossing itself. P should be
%   a n-by-2 array, representing a n point polygon with vertices [x y].
%
%*  See also: isconvex.

N = size(P,1);
P_ = [P(2:end,:) ; P(1,:)];

[I, J] = meshgrid(1:N, 1:N);
[I, J] = find(I>J+1 & (I<N | J>1));
Ax = P(I,1);                
Ay = P(I,2);
Bx = P_(I,1);   
By = P_(I,2);
Cx = P(J,1);                
Cy = P(J,2);
Dx = P_(J,1);   
Dy = P_(J,2);   

r = ((Ay-Cy).*(Dx-Cx)-(Ax-Cx).*(Dy-Cy))./((Bx-Ax).*(Dy-Cy) - (By-Ay).*(Dx-Cx));
s = ((Ay-Cy).*(Bx-Ax)-(Ax-Cx).*(By-Ay))./((Bx-Ax).*(Dy-Cy) - (By-Ay).*(Dx-Cx));

out = any((r>=0 & r<=1) & (s>=0 & s<=1));