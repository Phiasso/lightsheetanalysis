function [T, I] = trigonomize(T)
%TRIGONOMIZE set a triangle's orientation to trigonometric.
%
%*  TRO = TRIGONOMIZE(TRI) where TRI is a triangle (3-by-2 array) returns the
%   trigonometrized triangle TRO.
%
%*  [T, I] = TRIGONOMIZE(T) returns also the indices I such that 
%   TRO = TRI(I,:).
%
%*  See also: is_trigo.

if ~is_trigo(T)
    I = [1 3 2];
    T = T(I,:);
else
    I = [1 2 3];
end