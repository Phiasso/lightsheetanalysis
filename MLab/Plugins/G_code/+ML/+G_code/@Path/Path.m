classdef Path<ML.G_code.Points
%Path is the path class.
    
    % --- PROPERTIES ------------------------------------------------------
    properties
        closed = NaN;
        convex = NaN;
    end
    
    % --- METHODS ---------------------------------------------------------
    methods
        
        % _________________________________________________________________
        function this = Path(pos, closed)
        %Poly::constructor
        
            % Call the parent's constructor
            this = this@ML.G_code.Points(pos);
        
            % Default values
            if ~exist('closed', 'var'), closed = true; end
            
            % Assignment
            this.closed = closed;
            
        end
    end
end
