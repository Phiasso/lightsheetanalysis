function ptraj()
%PTRAJ Trajectory plot

clc
clf

% --- Definitions
r = 0.1;

N = 3;
% t = [5*rand(N,2) -ones(N,1) Inf(N,1)];
l = 0.15;
t = [0 0 -1 Inf ; ...
     l/2 1 -1 Inf ; ...
     l 0.5 -1 Inf];

% --- Preparation
fac = cell(6,1);
vtx = NaN((size(t,1)-1)*6,2);

% --- Set segments
for i = 1:size(t,1)-1
    nv = gnv(t,i);
    vtx(1+(i-1)*6:i*6,:) = [ones(3,1)*t(i,1:2) + r*[1 0 -1]'*np(nv) ; ... 
                          ones(3,1)*t(i+1,1:2) + r*[-1 0 1]'*np(nv)];   
end
fac{6} = reshape(1:(size(t,1)-1)*6, [6 size(t,1)-1])';

% --- Correct joints
for i = 2:size(t,1)-1
    
    % Preparation
    pv = gpv(t,i);
    nv = gnv(t,i);
    v = n(n(nv)+n(pv));
    
    % Correction point
    j0 = (i-1)*6-1;
    fc = r/sin(acos(dot(n(pv),v)));
    
    % Re-assign
    j1 = (i-1)*6-2;
    j2 = (i-1)*6;
    j3 = (i-1)*6+1;
    j4 = (i-1)*6+3;
    
    fc
    norm(nv)/dot(n(nv),v)
    pfc = min(fc, norm(pv)/dot(n(pv),v));
    if dot(v, vtx(j1,:)-vtx(j0,:))>0, vtx(j1,:) = vtx(j0,:) + pfc*v;
    else, vtx(j2,:) = vtx(j0,:) + pfc*v; end
    
    nfc = min(fc, norm(nv)/dot(n(nv),v));
    if dot(v, vtx(j3,:)-vtx(j0,:))>0, vtx(j3,:) = vtx(j0,:) + nfc*v;
    else, vtx(j4,:) = vtx(j0,:) + nfc*v; end
    
    
end

% % % 
% % % for i = 1:size(t,1)-1
% % %     
% % %     v0 = size(vtx,1);
% % %     pv = gpv(t,i);
% % %     cv = gcv(t,i);
% % %     nv = gnv(t,i);
% % %     
% % %     if any(isnan(gpv(t, i)))
% % %         vtx1 = ones(3,1)*t(i,1:2) + r*[1 0 -1]'*np(cv);
% % %     else
% % %         
% % %         da = diffang(vec2ang(-pv), vec2ang(cv));
% % %         
% % %         if da==0 && da==pi
% % %             vtx1 = ones(3,1)*t(i,1:2) + r*[1 0 -1]'*np(cv);
% % %             
% % %         elseif da<0
% % %             vtx1 = [t(i,1:2)+r*np(cv) ; t(i,1:2) ; t(i,1:2)+r*[0 1]];
% % %             
% % %         else
% % %             vtx1 = [t(i,1:2)+r*[0 -1] ; t(i,1:2) ; t(i,1:2)-r*np(cv)];
% % %             
% % %         end
% % %             
% % %     end
% % %     
% % %     if any(isnan(gnv(t, i)))
% % %         vtx2 = ones(3,1)*t(i+1,1:2) + r*[-1 0 1]'*np(cv);
% % %     else
% % %         
% % %         da = diffang(vec2ang(-cv), vec2ang(nv));
% % %         
% % %         if da==0 && da==pi
% % %             vtx2 = ones(3,1)*t(i+1,1:2) + r*[-1 0 1]'*np(cv);
% % %             
% % %         elseif da<0
% % %             vtx2 = [t(i+1,1:2)+r*[0 1] ; t(i+1,1:2) ; t(i+1,1:2)+r*np(cv)];
% % %             
% % %         else
% % %             vtx2 = [t(i+1,1:2)-r*np(cv) ; t(i+1,1:2) ; t(i+1,1:2)+r*[0 -1]];
% % %             
% % %         end
% % %     end
% % %     
% % %     vtx = [vtx ; vtx1 ; vtx2];
% % %     fac{6} = [fac{6} ; v0 + (1:6)];
% % %     
% % % end

hold on

for i = 1:numel(fac)
    
    if isempty(fac{i}), continue; end
    
    cdata = rand(size(fac{i},1),3);
    
    p = patch('Faces', fac{i}, 'Vertices', vtx, ...
              'FaceColor','flat', 'FaceVertexCData', cdata);
end

scatter(t(1,1), t(1,2), 50, 'FaceColor','r', 'MarkerEdgeColor', 'k')

axis equal

end

% _________________________________________________________________________
function v = gpv(t, i)
%GPV get previous vector

    % - Check
    if i-1<1 || i>size(t,1)
        v = [NaN NaN];
    else
        if isinf(t(i,4))
            v = [t(i-1,1)-t(i,1) t(i-1,2)-t(i,2)];
        else
            v = [NaN NaN];
        end
    end

end

% _________________________________________________________________________
function v = gnv(t, i)
%GNV get next vector

      % - Check
    if i<1 || i+1>size(t,1)
        v = [NaN NaN];
    else
        if isinf(t(i+1,4))
            v = [t(i+1,1)-t(i,1) t(i+1,2)-t(i,2)];
        else
            v = [NaN NaN];
        end
    end

end


% _________________________________________________________________________
function v = up(v)
%Flips a vector to the up direction

if v(2)<0, v = -v; end
end

% _________________________________________________________________________
function v = n(v)
%Normalizes a vector

v = v./sqrt(v(1)^2+v(2)^2);
end

% _________________________________________________________________________
function w = np(v)
%Normalized upper perpendicular vector

w = n([v(2) -v(1)]);
end
