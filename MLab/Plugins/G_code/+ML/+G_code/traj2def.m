function traj2def(traj, varargin)
%TRAJ2DEF Trajectory to definiton

% === Input variables =====================================================

in = inputParser;
in.addRequired('traj', @isnumeric);
in.addParamValue('mode', 'xy', @ischar);

in.parse(traj, varargin{:});
in = in.Results;

% =========================================================================

% --- Display trajectory definition
fprintf('\ntraj = [');
for i = 1:size(in.traj,1)
    
    switch in.mode
       
        case 'xy'
            fprintf('%.05f %.05f', in.traj(i,1), in.traj(i,2));
            
        case 'xyz'
            fprintf('%.05f %.05f %.05f', in.traj(i,1), in.traj(i,2), in.traj(i,3));
            
    end
    if i<size(in.traj,1), fprintf(' ; '); end
end
fprintf('];\n\n');
