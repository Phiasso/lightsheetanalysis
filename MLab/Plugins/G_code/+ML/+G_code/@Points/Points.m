classdef Points
%Points is the Point class.
    
    % --- PROPERTIES ------------------------------------------------------
    properties
        dim = NaN;
        N = 0;
        pos = [];
    end
    
    % --- METHODS ---------------------------------------------------------
    methods
        
        % _________________________________________________________________
        function this = Points(pos)
        %Points::constructor
            
            this.N = size(pos,1);
            this.dim = size(pos,2);
            this.pos = pos;
            
        end
    end
end
