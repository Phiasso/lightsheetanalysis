function L = lines(this, varargin)
%GEOM.POINTS.LINES get lines coefficients
%*  L = GEOM.POINTS.LINES(I, FORM) get the coefficients of lines.
%   The doublets of points are specified by I, a M-by-2 array of index.
%   The output L is a struct with whose field are:
%   * 'form': the value of FORM, giving the representation type
%   * 'equation': the equation relative to the form   
%   * coefficient fields: their number and meaning depend on the specified
%   form.
%
%   The coefficient fields depend on the FORM representation:
%
%   * 'Cartesian':       Ax + By + C = 0 (and A.^2 + B.^2 = 1)
%   * 'slope-intercept': y = Ax + B
%       In case the line is vertical, A = NaN and the eqaution is x = B.
%
%*  GEOM.POINTS.LINES(I) assumes FORM = 'slope-intercept'.
%
%*  GEOM.POINTS.LINES() assumes I = [1 2].
%
%*  See also: Geom.Points, Geom.Points.planes

% === Input variables =====================================================

in = inputParser;
in.addOptional('I', [1 2], @isnumeric);
in.addOptional('form', 'slope-intercept', @ischar);

in.parse(varargin{:});
in = in.Results;

% =========================================================================

L = struct('form', in.form);

switch in.form
    
    case 'Cartesian'
        
        L.equation = 'Ax + By + C = 0';
        
        dx = this.pos(I(:,2),1) - this.pos(I(:,1),1);
        dy = this.pos(I(:,2),2) - this.pos(I(:,1),2);
        
        L.A = 1./sqrt(1+(dx./dy).^2);
        L.B = 1./sqrt(1+(dy./dx).^2);
        L.C = -L.A.*this.pox(I(:,1),1) - L.B.*this.pox(I(:,1),2);        
    
    case 'slope_intercept'
        
        L.equation = 'y = Ax + B (vertical: A is NaN and x = B)';
        
        dx = this.pos(I(:,2),1) - this.pos(I(:,1),1);
        dy = this.pos(I(:,2),2) - this.pos(I(:,1),2);
        L.A = dy./dx;
        L.B = this.pos(I(:,1),2) - L.A.*this.pos(I(:,1),1);
        
        % Vertical lines
        J = ~isfinite(L.A);
        L.A(J) = NaN;
        L.B(J) = this.pos(I(J,1),1);
        
    otherwise
            
        error('GEOM.POINTS.LINES:Form', 'The specified form is not recognized.');
end