function [P, dist] = proj(this, C)
%GEOM.POINTS.PROJ get projections
%*  POINTS = GEOM.POINTS.PROJ(C) get the projection on the object defined 
%   by the coefficients in C. In dimension 3, C is [a b c] for lines and 
%   [a b c d] for planes.
%
%*  [P, DIST] = GEOM.POINTS.PROJ(...) also returns the distances from the
%   points to the projection.
% 
%*  See also: Geom.Points.

switch this.dim
    
    case 2
        
        warning('Not implemented yet');
    
    case 3
        
        switch numel(C)
            
            % --- LINE
            case 3
                
                warning('Not implemented yet');
                
            % --- PLANE
            case 4
        
                % Definitions
                a = C(1);
                b = C(2);
                c = C(3);
                d = C(4);
                p1 = a*this.pos(:,1) + b*this.pos(:,2) + c*this.pos(:,3) + d;
                p2 = a^2+ b^2 + c^2;
                
                % Outputs
                x = this.pos(:,1) - a*p1/p2;
                y = this.pos(:,2) - b*p1/p2;
                z = this.pos(:,3) - c*p1/p2;
                P = Geom.Points([x y z]);
                dist = abs(p1)/sqrt(p2);
        end
end