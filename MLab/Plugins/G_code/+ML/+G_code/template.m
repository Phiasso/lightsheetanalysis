% G_CODE TEMPLATE
%
% All length are in mm, times are in s.
% _________________________________________________________________________

clc
clf, set(gcf, 'WindowStyle', 'docked');

% === Parameters ==========================================================


% =========================================================================

% --- Create G_code instance
G = ML.G_code;
G.title = 'template';
G.settings.start_mode = 'Safe';

% --- Define substrate
G.substrate = struct('material', 'PMMA', ...
                     'R_curv', Inf, ...
                     'dims', [50 50 5], ...
                     'z_clear', 0.1, ...
                     'z_up', 2, ...
                     'd_up', 2, ...
                     'z_stop', 20);

% --- Define tools
t1 = G.add_tool('mill', 1);
t2 = G.add_tool('drill', 1);

% === Define elements =====================================================

G.rect([10 10], [11 15], 0, 'tool', t1, 'pref_axis', 'x');
 
G.hole([1 1], -1, 'z_step', 0.5, 'tool', t2);

T = [1 1 -1 NaN NaN NaN ; ...
     10 10 -1 NaN NaN NaN];
G.traj(T, 'tool', t1);

% === Display informations ================================================

% Generate trajectories
G.generate();  

% --- Display result
G.times();

G.visu_traj();
G.visu_substrate();

return

% --- Generate G-code
G.code();

% --- Save code
LS = get(0, 'UserData');
cp = ML.Project.select;     % Get current project (if defined)
G.save([cp.path 'Patterns/GCO/']);
