function code(this, tools)
%GEOM.PATTERNS.G_CODE.CODE Generates the G_code
%*  CODES = G_CODE.CODE() Generates the codes for the different tools.
%
%*  See also: Geom.Patterns.G_code.

% --- Parameters ----------------------------------------------------------

nl = char(10);
lw = 77;

% -------------------------------------------------------------------------

% --- Default values
if ~exist('tools', 'var')
    tools = 1:numel(this.tools); 
end

% --- Generate trajectories (optional)
for i = tools
    if numel(this.trajs)<i || isempty(this.trajs(i).traj)
        this.generate(i);
    end
end

% --- Generate code
for i = tools

    code = '';
    
    % -- INFORMATIONS -----------------------------------------------------
    
    code = [code '( === ' this.title ' ' repmat('=', [1 lw-numel(this.title)-9]) ' )' nl nl];
    
    code = [code '( Tool [' num2str(i) '/' num2str(numel(this.tools)) ']: ' ...
             this.tools(i).type  ' [diameter = ' num2str(this.tools(i).diam*1000) char(181) 'm | -' ...
             num2str(round(this.tools(i).diam*1000/2.54), '%04i') '-]'];
    if ~isempty(this.tools(i).desc), code = [ code ', ' this.tools(i).desc ]; end
    code = [code ' )' nl];
    
    code = [code '( Approximated time: ' ML.Time.s2h(this.trajs(i).time).str ' )' nl nl];
    
	code = [code '( Date of creation: ' datestr(now) ' )' nl];
    code = [code '( Machine: ' this.machine ' )' nl nl];
    
    code = [code '( Generated with the G_CODER Matlab toolkit' char(169) ' v' this.version ', by R. Candelier )' nl];
    code = [code '( ' repmat('=', [1 lw-4]) ' )' nl];
    
    % -- HEADER -----------------------------------------------------------

    code = [code nl '( --- HEADER START ' repmat('-', [1 lw-21]) ' )' nl nl];
    
    code = append(code, 'G21', 'Set length unit to mm');
    code = append(code, 'G94', 'Feed per minute mode');
    code = append(code, 'G54', 'Use fixture offset 1');
    % code = append(code, 'G43', 'Apply no tool length offset');
    code = append(code, 'M48', 'Enable speed and feed override');
    code = append(code, 'M7', 'Mist coolant on');
        
    code = [code nl];
    code = append(code, '', ['- - - - - - - - - - - - - - - -  ' this.settings.start_mode ' approach']);
    code = append(code, 'M3', 'Rotate spindle clockwise');
    code = append(code, ['S' num2str(this.tools(i).spindle)], 'Set spindle rotation speed [rpm]');
    
    switch lower(this.settings.start_mode)
        
        case 'fast'
            code = append(code, ['G1 Z' num2str(this.substrate.z_up) ' F' num2str(this.tools(i).feedrates.far)], ' V ');
            
        case 'soft'
            code = append(code, ['G1 Z' num2str(this.substrate.z_up*5) ' F' num2str(this.tools(i).feedrates.far)], '\ /');
            code = append(code, ['G1 Z' num2str(this.substrate.z_up) ' F' num2str(this.tools(i).feedrates.close)], ' v ');
            
        case 'safe'
            code = append(code, ['G1 Z' num2str(this.substrate.z_up*5) ' F' num2str(this.tools(i).feedrates.far)], '\ /');
            code = append(code, ['G1 Z' num2str(this.substrate.z_up) ' F' num2str(this.tools(i).feedrates.close)], ' v ');
            code = append(code, 'T1 M6', '*** Check list ***');
            code = append(code, '', 'Everything''s clear | Z0 is done | Spindle on');
            code = append(code, '', 'Press Start if everything is OK');
    end
    
    code = append(code, 'M3', 'Rotate spindle clockwise');
    code = append(code, ['S' num2str(this.tools(i).spindle)], 'Set spindle rotation speed [rpm]');
    code = append(code, 'G4 P3', 'Let spindle rotation establish');
    
    code = [code nl '( --- HEADER STOP ' repmat('-', [1 lw-20]) ' )' nl nl];
    
    % --- TRAJECTORY ------------------------------------------------------
    
    T = this.trajs(i).traj;
    for j = 2:size(T, 1);
        
        if ~isfinite(T(j,5))
            
            block = '';
            if ~isnan(T(j,1)) & T(j,1)~=T(j-1,1)
                block = [block ' X' num2str(T(j,1))];
            end
            
            if ~isnan(T(j,2)) & T(j,2)~=T(j-1,2)
                block = [block ' Y' num2str(T(j,2))];
            end
            
            if ~isnan(T(j,3)) & T(j,3)~=T(j-1,3)
                block = [block ' Z' num2str(T(j,3))];
            end
        
            if ~isempty(block)
                block = ['G1' block ' F' num2str(T(j,4))];
            end 
            
        else
            
            if T(j,7)>0
                inst = 'G3';
            else
                inst = 'G2';
            end
            
            block = '';
            if ~isnan(T(j,1))
                block = [block ' X' num2str(T(j,1))];
            end
            
            if ~isnan(T(j,2))
                block = [block ' Y' num2str(T(j,2))];
            end
            
            block = [inst block ' I' num2str(T(j,5)-T(j-1,1)) ' J' num2str(T(j,6)-T(j-1,2)) ' F' num2str(T(j,4))];
        end
    
        code = append(code, block, this.trajs(i).coms{j});
    end
    
    % -- FOOTER -----------------------------------------------------------

    code = [code nl '( --- FOOTER START ' repmat('-', [1 lw-21]) ' )' nl nl];
    
    code = append(code, ['G1 Z' num2str(this.substrate.z_stop) ' F' num2str(this.tools(i).feedrates.far)], 'Clear vertically');
    code = append(code, 'M9', 'Coolants off');
    code = append(code, 'G80', 'Cancel motion mode');
    code = append(code, 'M5', 'Stop spindle rotation');
    code = append(code, 'M30', 'Program end and rewind');
    
    code = [code nl '( --- FOOTER STOP ' repmat('-', [1 lw-20]) ' )' nl];

    % --- Send to the output
%     if nargout
%         codes(i) = this.code;
%     end

    this.trajs(i).code = code;
end

% -------------------------------------------------------------------------
function out = fcom(com)

if isempty(com)
    out = '';
else
    out = ['( ' com ' )'];
end

% -------------------------------------------------------------------------
function out = append(c, a, com)

nl = char(10);
cw = 50;

if isempty(a) && isempty(com)
    out = c;
else
    out = [c a repmat(' ', [1 cw-numel(fcom(com))-numel(a)]) fcom(com) nl];
end
