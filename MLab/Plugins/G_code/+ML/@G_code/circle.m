function hole(this, pos, R, z, varargin)
%GEOM.PATTERNS.G_CODE.CIRCLE Circle element
%*  G_CODE.CIRCLE(POS, R, Z) where POS is a 2 elements array [x,y], R is 
%   the radius and Z is the depth to drill.
%
%*  G_CODE.CIRCLE(..., 'key', value) specifies options. Keys can be:
%   * 'tool' (this.tool):   The tool to use.
%   * 'step_size' (tool.diam): The step size.
%
%*  See also: Geom.Patterns.G_code, Geom.Patterns.G_code.traj, Geom.Patterns.G_code.rect.

% === Input variables =====================================================

in = inputParser;
in.addRequired('pos', @isnumeric);
in.addRequired('R', @isnumeric);
in.addRequired('z', @isnumeric);
in.addParamValue('z_step', this.tools(this.tool).diam, @isnumeric);
in.addParamValue('z_start', 0, @isnumeric);
in.addParamValue('tool', this.tool, @isnumeric);

in.parse(pos, R, z, varargin{:});
in = in.Results;

% =========================================================================

% --- Checks

% Check tool
this.check();

% Out of bounds check
r = this.tools(this.tool).diam/2;
bounds = [0 this.substrate.dims(1) 0 this.substrate.dims(2)];

if any(in.pos(1)-r-R<bounds(1) || in.pos(1)+r+R>bounds(2) || ...
       in.pos(2)-r-R<bounds(3) || in.pos(2)+r+R>bounds(4))
    warning('G_CODE::CIRCLE out of bounds', 'The circle arrives of bounds.');
end

% Add the trajectory

T = [in.pos(1) in.pos(2)-R+r in.z NaN NaN NaN ; ...
     in.pos(1) in.pos(2)-R+r in.z in.pos 1];

this.traj(T, 'tool', in.tool, 'z_step', in.z_step, 'z_start', in.z_start);