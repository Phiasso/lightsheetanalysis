classdef G_code<handle
%G_coder class.
    
    % --- PROPERTIES ------------------------------------------------------
    properties
        
        % -- General
        version = '2.0.2';
        machine = 'MINITECH 12159';             % Machine name
        title = '';                             % Title of the project
        
        % -- Tool list
        tools = struct('type', {}, ...
                       'diam', {}, ...
                       'desc', {}, ...
                       'optimize', {}, ...
                       'feedrates', {}, ...
                       'spindle', {});
        tool = NaN;
        
        % -- Substrate
        substrate = struct('material', 'PMMA', ...
                           'R_curv', Inf, ...
                           'dims', [50 50 5], ...
                           'z_clear', 0.1, ...
                           'z_up', 2, ...
                           'd_up', 2, ...
                           'z_stop', 50);
                       
        % --- Settings
        settings = struct('start_mode', 'Safe');     % 'Fast', 'Soft' or 'Safe'
                       
        % -- Elements
        elms = cell(0,1);
        
        % -- Processing
        trajs = struct('tool', {}, ...
                       'traj', {}, ...
                       'coms', {}, ...
                       'time', {}, ...
                       'code', {});
        date = date;

    end
    
    % --- METHODS ---------------------------------------------------------
    methods
        
        % _________________________________________________________________
        function this = G_code()
        %G_CODE::constructor
            
        end
        
        % _________________________________________________________________
        function bool = check(this)
        %G_CODE::CHECK Checks
        
            if isnan(this.tool)
                error('G_CODE::CHECK tool', 'No tool defined !');
            end
            bool = true;
        end
        
        % _________________________________________________________________
        % Useful shortcuts
        function gx(this, x, com)
            if ~exist('com', 'var'), com = ''; end
            this.go(x, NaN, NaN, 'com', com);
        end
        
        function gy(this, y, com)
            if ~exist('com', 'var'), com = ''; end
            this.go(NaN, y, NaN, 'com', com);
        end
        
        function gz(this, z, com)
            if ~exist('com', 'var'), com = ''; end
            this.go(NaN, NaN, z, 'com', com);
        end
        
        function gv(this, v, com)
            if ~exist('com', 'var'), com = ''; end
            this.go(NaN, NaN, NaN, 'v', v, 'com',com);
        end
        
        function com(this, com)
            this.go(NaN, NaN, NaN, 'com', com);
        end
        
        function p = pos(this, tool)
            if numel(this.trajs(tool).traj)
                p = this.trajs(tool).traj(end,1:3);
            else
                p = [NaN NaN NaN];
            end
        end
    end
end