function id = add_tool(this, type, diam, varargin)
%GEOM.PATTERNS.G_CODE.ADD_TOOL add a tool to the tool list
%*  ID = G_CODE.ADD_TOOL(TYPE, DIAM) add a tool to the tool list. DIAM should be
%   expressed in mm.
%
%*  ID = G_CODE.ADD_TOOL(..., 'feedrates', FEEDS) specifies the feedrates.
%
%*  ID = G_CODE.ADD_TOOL(..., 'desc', DESC) specifies a tool description.
%
%*  See also: Geom.Patterns.G_code.

% === Input variables =====================================================

in = inputParser;
in.addRequired('type', @ischar);
in.addRequired('diam', @isnumeric);
in.addParamValue('desc', '', @ischar);
in.addParamValue('feedrates', NaN, @isstruct);
in.addParamValue('spindle', NaN, @isnumeric);
in.addParamValue('optimize', true, @islogical);

in.parse(type, diam, varargin{:});
in = in.Results;

% =========================================================================

tool = struct('type', in.type, ...
              'diam', in.diam, ...
              'desc', in.desc, ...
              'optimize', in.optimize);

% --- Set feedrates (in mm/min)

if isstruct(in.feedrates)
    
    tool.feedrates = in.feedrates;
    
else
    
    tool.feedrates = struct();
    switch this.substrate.material
        
        case 'PMMA'
            
            if tool.diam<=0.1
                
                tool.feedrates.far =   300;
                tool.feedrates.close = 120;
                tool.feedrates.in =    20;
                
            elseif tool.diam<=1
                
                tool.feedrates.far =   300;
                tool.feedrates.close = 120;
                tool.feedrates.in =    30;
                
            else
                
                tool.feedrates.far =   300;
                tool.feedrates.close = 120;
                tool.feedrates.in =    40;
                
            end
            
        case 'Brass'
            
            if tool.diam<=0.1
                
                tool.feedrates.far =   300;
                tool.feedrates.close = 120;
                tool.feedrates.in =    20;
                
            elseif tool.diam<=1
                
                tool.feedrates.far =   300;
                tool.feedrates.close = 120;
                tool.feedrates.in =    30;
                
            else
                
                tool.feedrates.far =   300;
                tool.feedrates.close = 120;
                tool.feedrates.in =    40;
                
            end
            
    end
end

% --- Set spindle (in krpm)

if ~isnan(in.spindle)
    
    tool.spindle = in.spindle;
    
else
    
    switch this.substrate.material
        
        case 'PMMA'
            
            a = -8.7661;
            b = 19.7425;
            
            tool.spindle = a*log(tool.diam) + b;
            
        case 'Brass'
            
            a = -8.7661;
            b = 19.7425;
            
            tool.spindle = a*log(tool.diam) + b;
            
    end
end

% Machine correction
tool.spindle = (tool.spindle+1.643)/0.988;

% Round off and conversion to rpm
tool.spindle = round(tool.spindle*10)*100;

% --- Add tool
this.tools(end+1) = tool;
id = numel(this.tools);
this.tool = id;