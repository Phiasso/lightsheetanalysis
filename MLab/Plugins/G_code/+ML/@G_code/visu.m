function visu(this, varargin)
%GEOM.PATTERNS.G_CODE.VISU Display the edges from the trajectories
%*  G_CODE.VISU() display the trajectories for the different tools.
%
%*  See also: G_CODE.


% === Input variables =====================================================

in = inputParser;
in.addOptional('tools', 1:numel(this.tools), @isnumeric);
in.addParamValue('edges', true, @islogical);
in.addParamValue('ang_bin', pi/60, @isnumeric);

in.parse(varargin{:});
in = in.Results;

% =========================================================================

% --- Generate trajectories (optional)
for i = in.tools
    if numel(this.trajs)<i | isempty(this.trajs(i).traj)
        this.generate(i);
    end
end

% --- Preparation
hold on
cm = lines(numel(in.tools));

% --- Tool loop
for i = 1:numel(in.tools)

    % Find tool
    tool = in.tools(i);
    r = this.tools(tool).diam/2;
    
    % Get trajectory
    T = this.trajs(tool).traj;
    
    % --- Get arc-ed trajectories
    x = [];
    y = [];
    z = [];
    for j = 1:size(this.trajs(tool).traj,1)
        
        if any(isnan(this.trajs(tool).traj(j,1:3)))
            continue;
        elseif isnan(this.trajs(tool).traj(j,5))
            x(end+1,1) = this.trajs(tool).traj(j,1);
            y(end+1,1) = this.trajs(tool).traj(j,2);
            z(end+1,1) = this.trajs(tool).traj(j,3);
        else
            A = this.trajs(tool).traj(j-1,1:2);
            B = this.trajs(tool).traj(j,1:2);
            C = this.trajs(tool).traj(j,5:6);
            R = sqrt(sum((A-C).^2));
            
            if all(A==B)
                t1 = Geom.vec2ang(A-C);
                th = t1:in.ang_bin:t1+2*pi;
            else
                
                if this.trajs(tool).traj(j,7)>0
                    t1 = Geom.vec2ang(A-C);
                    t2 = Geom.vec2ang(B-C);
                    if t2<t1, t2 = t2+2*pi; end
                    th = linspace(t1, t2, round(abs(t2-t1)/in.ang_bin));
                else
                    t1 = Geom.vec2ang(B-C);
                    t2 = Geom.vec2ang(A-C);
                    if t2<t1, t2 = t2+2*pi; end
                    th = fliplr(linspace(t1, t2, round(abs(t2-t1)/in.ang_bin)));
                end
                   
            end
            th = vect(th);
            x = [x ; C(1)+R*cos(th)];
            y = [y ; C(2)+R*sin(th)];
            z = [z ; this.trajs(tool).traj(j,3)*ones(numel(th),1)];
        end
    end
    
    % Reduction
    I = isfinite(x);
    x = x(I);
    y = y(I);
    z = z(I);
    
    % --- Plots
    
    % Lines
    plot3(x, y, z, '-', 'color', cm(i,:));
    
    % Markers
    plot3(T(:,1), T(:,2), T(:,3), '.', 'color', cm(i,:));
    
    % --- Edges
    
    if in.edges
    
        % --- Reduction
        
        x
        y
        
        
        % Remove consecutive doublets
        I = [true ; diff(x) | diff(y)];
        x = x(I);
        y = y(I);
        
        % Remove multiple occurences of the same triplet
% % %         xy = [x y];
% % %         J = [];
% % %         for j = 2:numel(x)-1
% % %             [tf, I] = ismember(xy(j,:), xy(j+1:end-1,:), 'rows');
% % %             I = I+j;
% % %             if tf && ((all(xy(I-1,:)==xy(j-1,:)) && all(xy(I+1,:)==xy(j+1,:))) || ...
% % %                      (all(xy(I-1,:)==xy(j+1,:)) && all(xy(I+1,:)==xy(j-1,:))))
% % %                  J = [J I-1:I+1];
% % %             end
% % %         end
% % %         size(J)
% % %         J = setdiff(1:numel(x), J);
% % %         
% % %         x = x(J);
% % %         y = y(J);
% % %         
% % %         clf
% % %         plot3(x, y, 1:numel(x), '.-')
% % %         return
        
        % --- Tiny trajectory
        if numel(x)<=2
            
           continue 
        end
        
        u = [diff(x) diff(y)];
        u = u./(sqrt(sum(u.^2,2))*ones(1,2));
        v = [-u(:,2) u(:,1)]*r;
        
        t1 = [];
        t2 = [];
        
        for j = 1:numel(x)
        
            if j==1
                
                t1(end+1,:) = [x(1) y(1)]  + v(1,:);
                t2(end+1,:) = [x(1) y(1)]  - v(1,:);
                
            elseif j==numel(x)
                
                Pp = [x(end-1) y(end-1)];
                P  = [x(end) y(end)];
                tmp = [P+v(end,:) ; P-v(end,:)];
                I = Geom.sameside(t1(end,:), tmp, Pp, P);
                t1(end+1,:) = tmp(I, :);
                t2(end+1,:) = tmp(~I, :);
                
            else
                
                % Points in the trajectoy
                Pp = [x(j-1) y(j-1)];
                P  = [x(j) y(j)];
                Pn = [x(j+1) y(j+1)];
                
                % Points on the edges
                A1 = t1(end,:);
                A2 = t2(end,:);
                tmp = [P+v(j-1,:) ; P-v(j-1,:)];
                I = Geom.sameside(A1, tmp, Pp, P);
                B1 = tmp(I, :);
                B2 = tmp(~I, :);
                
                % Turns back
                if all(Pp==Pn)
                    alpha = Geom.vec2ang(B1-P);
                    beta =  Geom.vec2ang(B2-P);
                    s = sign(det([B1(1)-Pp(1) B2(1)-B1(1) ; B1(2)-Pp(2) B2(2)-B1(2)]));
                    if s>0
                        theta = vect(alpha:sign(beta-alpha)*in.ang_bin:beta);
                    else
                        theta = vect(beta:sign(alpha-beta)*in.ang_bin:alpha);
                    end                    
                    
                    t1 = [t1 ; B1 ; ones(numel(theta),1)*P - r*[cos(theta) sin(theta)]];
                    t2 = [t2 ; B2 ; ones(numel(theta),1)*P - r*flipud([cos(theta) sin(theta)])];
                    continue;
                end
                
                tmp = [P+v(j,:) ; P-v(j,:) ; Pn+v(j,:) ; Pn-v(j,:)];
                I = Geom.sameside(B1, tmp, P, Pn);
                J = find(~I);
                I = find(I);
                C1 = tmp(I(1),:);
                D1 = tmp(I(2),:);
                C2 = tmp(J(1),:);
                D2 = tmp(J(2),:);
                
                % Intersections and arcs
                p1 = Geom.segcross(A1, B1, C1, D1);
                p2 = Geom.segcross(A2, B2, C2, D2);
                
                if any(isnan(p1))
                    alpha = Geom.vec2ang(B1-P);
                    beta =  Geom.vec2ang(C1-P);
                    if beta+pi<alpha, beta = beta + 2*pi; end
                    if beta-pi>alpha, beta = beta - 2*pi; end
                    theta = vect(alpha:sign(beta-alpha)*in.ang_bin:beta);
                    t1 = [t1 ; ones(numel(theta),1)*P + r*[cos(theta) sin(theta)]];
                else
                    t1(end+1,:) = p1;
                end
                
                if any(isnan(p2))
                    alpha = Geom.vec2ang(B2-P);
                    beta =  Geom.vec2ang(C2-P);
                    if beta+pi<alpha, beta = beta + 2*pi; end
                    if beta-pi>alpha, beta = beta - 2*pi; end
                    theta = vect(alpha:sign(beta-alpha)*in.ang_bin:beta);
                    t2 = [t2 ; ones(numel(theta),1)*P + r*[cos(theta) sin(theta)]];
                else
                    t2(end+1,:) = p2;
                end

            end
            
        end
                
        plot(t1(:,1), t1(:,2), 'k:');
%         plot(t2(:,1), t2(:,2), 'k:');
        
    end
end

% --- Adjustment
box on
axis square
daspect([1 1 1]);








