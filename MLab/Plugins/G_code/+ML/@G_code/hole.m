function hole(this, pos, z, varargin)
%GEOM.PATTERNS.G_CODE.HOLE Hole element
%*  G_CODE.HOLE(POS, Z) where POS is a n-by-2 array [x,y] and Z is a n-by-1
%   array of depths to drill. Z can also be a single value.
%
%*  G_CODE.HOLE(..., 'key', value) specifies options. Keys can be:
%   * 'tool' (this.tool):   The tool to use.
%   * 'step_size' (tool.diam): The step size.
%
%*  See also: Geom.Patterns.G_code, Geom.Patterns.G_code.traj, Geom.Patterns.G_code.rect.

% === Input variables =====================================================

in = inputParser;
in.addRequired('pos', @isnumeric);
in.addRequired('z', @isnumeric);
in.addParamValue('z_step', this.tools(this.tool).diam, @isnumeric);
in.addParamValue('z_start', 0, @isnumeric);
in.addParamValue('tool', this.tool, @isnumeric);

in.parse(pos, z, varargin{:});
in = in.Results;

% =========================================================================

% --- Checks

% Check tool
this.check();

% Out of bounds check
r = this.tools(this.tool).diam/2;
bounds = [0 this.substrate.dims(1) 0 this.substrate.dims(2)];

if any(in.pos(1)-r<bounds(1) || in.pos(1)+r>bounds(2) || ...
       in.pos(2)-r<bounds(3) || in.pos(2)+r>bounds(4))
    warning('G_CODE::HOLE out of bounds', 'Some holes are out of bounds.');
end

% --- Preparation
if numel(in.z)==1, in.z = in.z*ones(size(in.pos,1),1); end  % Depths

for i = 1:size(in.pos,1)

    this.elms{end+1} = struct('type', 'hole', ...
                              'tool', in.tool, ...
                              'pos', in.pos(i,:), ...
                              'z', in.z(i), ...
                              'z_step', in.z_step, ...
                              'z_start', in.z_start);
end
