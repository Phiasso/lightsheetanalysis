function visu_3D(this)
%G_CODE::VISU_3D 3D Visualization of the result
%*  VISU_3D() Visualization of the result, in 3D.
%
%*  See also: G_CODE.

% --- Checks
this.check();

% --- 3D Projection object
a = Geom.Proj_3D();

% --- Elements
for i = 1:numel(this.elms) 
    
   elm = this.elms{i};
   tool = this.tools(elm.tool);
   
   switch elm.type
       
       case 'hole'
           
           b = Geom.Proj_3D();
           
           n = 20;
           theta = linspace(0,2*pi,n);
           x = tool.diam/2*cos(theta);
           y = tool.diam/2*sin(theta);

           b.vtx = ones(n+1,1)*elm.pos + [0 0 ; x' y'];
           b.fac = [[ones(n-2,1) (2:n-1)' (3:n)' ; 1 n 2] elm.z*ones(n-1, 1)];
          
           a.merge(b);
           
       case 'traj'
           
   end
    
    
end

a.surf();
% a.mesh()