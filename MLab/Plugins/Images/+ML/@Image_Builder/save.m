function save(this, varargin)
%ML.Image_Builder.save Save the Image
%
%   See also: ML.Image_Builder.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addRequired('filename', @ischar);
in.addParamValue('format', 'png', @ischar);
in.addParamValue('BitDepth', 16, @isnumeric);
in = +in;

% =========================================================================

% --- Checks
if isempty(this.Img.pix)
    error('The image is not defined.');
end

options = {};
options{end+1} = 'BitDepth';
options{end+1} = in.BitDepth;

if ~isnan(this.transparency_color)
    options{end+1} = 'transparency';
    options{end+1} = this.transparency_color;
end

% --- Output
imwrite(this.Img.pix, in.filename, in.format, options{:});
