function build(this)
%ML.Image_Builder.build Build the Image
%
%   See also: .

% --- Checks
if isnan(this.width)
    error('The image width has not been specified.');
end

if isnan(this.height)
    error('The image height has not been specified.');
end


% --- Initialization
Img = repmat(permute(this.background_color, [1 3 2]), [this.height this.width 1]);

% --- Insert Blocks
for i = 1:numel(this.Blocks)
          
    s = size(this.Blocks(i).content);
    
    Img(this.Blocks(i).y+[0:s(1)-1], ...
        this.Blocks(i).x+[0:s(2)-1], :) = this.Blocks(i).content;
    
end

% --- Output
this.Img = ML.Image(Img);
