function waitline(this, varargin)
%[Display].waitline Manage waitline
%   [Display].waitline() Displays a waitline.
%
%     See also ML.Time.Display

% TO DO:
%   - Estimate remaining time ?
%   - Use onCleanup

% --- Checks --------------------------------------------------------------

% --- First call flush
if this.counter==-1
    this.step;
    if isnumeric(varargin{1}) && ischar(varargin{end})
        fprintf('%s\n', varargin{end});
        this.rmlast = true;
    end
end

% --- Check counter
this.counter = this.counter + 1;
if mod(this.counter, round(this.period)) && this.counter < this.itot-1
    return
end

% --- Input management ----------------------------------------------------
if this.rmlast
    varargin(end) = [];
end

for i = 1:numel(varargin)
    if isnumeric(varargin{i})
        varargin{i} = inputname(i+1);
    end
end

% --- File parsing --------------------------------------------------------

% Get caller line
[fname, line] = ML.Files.whocalled;
fd = ['l' num2str(line)];

% --- Get variables
V = struct();   % Variable structure

for i = 1:numel(varargin)
        
    % Get range
    if ~isfield(this.range, fd) || ~isfield(this.range.(fd), varargin{i})
        
        % --- Parse caller file
        tmp = textscan(fileread(fname), '%s', 'Delimiter', {'\n', '\r'});
        txt = tmp{1};
        for j = line:-1:1
            tok = regexp(strtrim(txt{j}), ['^for ' varargin{i} ' = (.*)$'], 'tokens');
            if numel(tok)
                this.range.(fd).(varargin{i}) = evalin('caller', tok{1}{1});
                break;
            end
        end
        
    end
    
    % Store variable
    V.(varargin{i}) = evalin('caller', varargin{i});
    
end

% Total number of iterations
if ~isfinite(this.itot)
    this.itot = 1;
    for i = 1:numel(varargin)
        this.itot = this.itot*numel(this.range.(fd).(varargin{i}));
    end
end

% Longest variable size
lvs = max(cellfun(@numel, varargin));

% --- Display

% Clear last commands
if this.counter
    fprintf('%s', repmat(char(8), [1 this.cwl*numel(varargin)]));
end

for i = 1:numel(varargin)
   
    v = varargin{i};
    p = (find(V.(v) == this.range.(fd).(v)))/numel(this.range.(fd).(v));
    
    s = [v repmat(' ', [1 lvs-numel(v)+1]) '['];
    l = this.cwl-numel(s)-2;
    ne = round(p*l);
    
    s = sprintf('%s%s%s]', s, repmat('=', [1 ne]), repmat(' ', [1 l-ne]));
    
    % Middle percentage
    if p==1
        tmp = ' Done ';
    else
        tmp = num2str(p*100,'%.2f');
        tmp = [repmat(' ', [1 5-numel(tmp)]) tmp '%'];
    end
    s(round(numel(s)/2)+(-2:3)) = tmp;
        
    fprintf('%s\n', s);
        
end

% --- Last call reset
if this.counter == this.itot-1
    this.counter = -1;
    this.itot = Inf;
    this.range = struct();
    
    this.stop
    
end