function out = s2h(t)
%ML.Time.s2h Convert seconds to 'human' time
%   ML.TIME.S2H(T) converts the numerical time T (in seconds) in a string 
%   days, hours, minutes and seconds. The date corresponding to the next T
%   seconds is also displayed.
%
%   S = ML.TIME.S2H(T) returns a string S, in a short h-m-s version.
%
%   Example:
%   When will it be in 12345 seconds ?
%
%   >>  Time.s2h(12345)
%   3 hours - 25 minutes - 45 seconds
%   → 02-May-2012 22:56:26
%
%   See also .

% --- Definition
tu = cumprod([1 60 60 24]);
S = struct();
S.d = floor(t/tu(4)); tmp = t-S.d*tu(4);
S.h = floor(tmp/tu(3)); tmp = tmp-S.h*tu(3);
S.m = floor(tmp/tu(2));
S.s = tmp-S.m*tu(2);

% --- Strings

S.str = '';
show = false;

% Days
if S.d>0
    S.str = [S.str sprintf('%d day', S.d)];
    if S.d>1, S.str = [S.str, sprintf('s')]; end
    S.str = [S.str ' - '];
    show = true;
end

% Hours
if S.h>0 || show
    S.str = [S.str, sprintf('%d hour', S.h)];
    if S.h>1, S.str = [S.str, sprintf('s')]; end
    S.str = [S.str ' - '];
    show = true;
end

% Minutes
if S.m>0 || show
    S.str = [S.str, sprintf('%d minute', S.m)];
    if S.m>1, S.str = [S.str, sprintf('s')]; end
    S.str = [S.str ' - '];
end

% Seconds
S.str = [S.str sprintf('%d second', round(S.s))];
if S.s>1, S.str = [S.str, sprintf('s')]; end

% After
S.f = datestr(now+t/tu(4));

% --- Output
if nargout
    out = S;
else
    fprintf('%s\n', S.str);
    fprintf('%s %s\n', char(8594), S.f);
end