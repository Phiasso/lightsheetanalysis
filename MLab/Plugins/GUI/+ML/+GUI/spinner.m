function spinner(varargin)
%ML.GUI.spinner Spinner uicontrol
%   ML.GIU.SPINNER() add a spinner uicontrol to the current figure.
%
%   See also: uicontrol.

% --- Input parameters ----------------------------------------------------

in = ML.Input(varargin{:});
in.addParamValue('value', 1, @isnumeric);
in.addParamValue('range', [-Inf Inf], @isnumeric);
in.addParamValue('inc', 1, @isnumeric);
in.addParamValue('position', [10 10 100 20], @isnumeric);
in.addParamValue('callback', @(h,e) h.Value, @(x) isa(x, 'function_handle'));
in.addParamValue('fig', gcf, @isnumeric);
in = +in;

% -------------------------------------------------------------------------

jModel = javax.swing.SpinnerNumberModel(in.value, in.range(1), in.range(2), in.inc);
jSpinner = javax.swing.JSpinner(jModel);
jhSpinner = javacomponent(jSpinner, in.position, in.fig);
jhSpinner.StateChangedCallback = in.callback;