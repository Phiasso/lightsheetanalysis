function close(this)
%ML.GUI.close
%
%   See also: ML.GUI.

close(this.fig);