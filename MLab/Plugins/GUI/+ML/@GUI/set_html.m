function set_html(this, varargin)
%ML.GUI.set_html Set content of an html widget
%
%   See also: ML.GUI.

% === Inputs ==============================================================

in = ML.Input;
in.tag = @ischar;
in.html = @ischar;
in = +in;

% =========================================================================

browser = get(findobj('Tag', in.tag), 'UserData');
browser.setHtmlText(in.html);