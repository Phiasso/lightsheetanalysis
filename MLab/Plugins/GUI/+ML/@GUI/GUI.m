classdef GUI<handle
%ML.HUI [HUI plugin] The MLab Html User Interface class.
% The 'name' property should be a value structure field 
    
    % --- PROPERTIES ------------------------------------------------------
    properties (Access = private)
        
        grid_layout = true;
        tabs = struct('name', {}, 'widgets', {}, 'layout', ...
            struct('Width', {}, 'Height', {}));
                
    end
    
    properties (GetAccess = public, SetAccess = private)
        
        fig = NaN;
        name = '';
        width = NaN;
        height = NaN;
        
    end
    
    properties (Access = public)
        
        style = 'classic';
        current_tab = 1;
        display_tab = 1;
        auto_tag = true;
        
    end
    
    % --- METHODS ---------------------------------------------------------
    methods
        
        % _________________________________________________________________
        function this = GUI(varargin)
        %ML.HUI Constructor
            
            % === Inputs ==================================================
            
            in = ML.Input;
            in.name{ML.uniqid} = @ischar;
            in.grid(true) = @islogical;
            in = +in;
            
            % =============================================================
        
            % !! CHECK that the name is not already assigned !!
            
            % Define name
            this.name = in.name;
            
            % --- First tab, empty
            this.new_tab('');
            
            % --- Grid layout ?
            this.grid_layout = in.grid;
            
        end
        
    end
    
end
