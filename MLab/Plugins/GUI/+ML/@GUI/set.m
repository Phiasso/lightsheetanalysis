function set(this, varargin)
%ML.GUI.setfig
%
%   See also: ML.GUI.

% === Inputs ==============================================================

in = ML.Input;

in.src = @(x) isstruct(x) || ischar(x);
in.key{'Value'} = @ischar;
in.value{NaN} = @(x) true;
in = +in;

% =========================================================================

% --- Default tab
if ischar(in.src)
    in.src = struct('tab', this.display_tab, 'tag', in.src);
end

% --- Get object widget property
set(findobj('Tag', in.src.tag), in.key, in.value);
