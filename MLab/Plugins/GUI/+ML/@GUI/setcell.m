function setcell(this, varargin)
%ML.GUI.setcell Set cell propoerties
%
%   Note: Does not manage other units than pixels.
%
%   See also: ML.HUI.

% === Input ===============================================================

in = ML.Input;
in.row = @isnumeric;
in.col = @isnumeric;
in.Width(NaN) = @isnumeric;
in.Height(NaN) = @isnumeric;
in.Unit('px') = @ischar;
in = +in;

% =========================================================================

% --- Set Width and Height

if ~isnan(in.Width)
    this.tabs(this.current_tab).layout(in.row,in.col).Width = in.Width;
end

if ~isnan(in.Height)
    this.tabs(this.current_tab).layout(in.row,in.col).Height = in.Height;
end
