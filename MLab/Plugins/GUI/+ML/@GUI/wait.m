function wait(this)
%ML.HUI.txt Text shortcut
%   ML.HUI.TEXT([ROW COL], TXT) adds a text widget in the cell [ROW COL]
%   of the current grid.
%
%   ML.HUI.TEXT(..., PARAM, VALUE) defines PARAM / VALUE couples. Valid
%   options are those that a <p> tag can handle. VALUE can be either a 
%   string or a numeric value.
%
%   See also: ML.HUI.

uiwait(this.fig);