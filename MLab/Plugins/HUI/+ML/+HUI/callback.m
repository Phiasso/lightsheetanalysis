function callback(varargin)
%ML.HUI.callback Callback manager
%
%   See also: ML.HUI.

% === Inputs ==============================================================

in = ML.Input;
in.id = @ischar;
in.tab = @isnumeric;
in.tag = @ischar;
in.callback = @ischar;
in = +in;

% =========================================================================

% --- Get HUI object
H = ML.HUI.manager('get', in.id);

% --- Trig callback
H.(in.callback)(struct('tab', in.tab, 'tag', in.tag));

