function tabs(id, varargin)
%ML.HUI.tabs Tabs manager
%
%   See also: ML.HUI.

% === Inputs ==============================================================

in = ML.Input(id, varargin{:});
in.addRequired('id', @ischar);
in.addParamValue('show_tab', [], @isnumeric);
in = +in;

% =========================================================================

% --- Get HUI object
H = ML.HUI.manager('get', in.id);

% ---Show tab
if ~isempty(in.show_tab)
    H.display_tab = in.show_tab;
    H.show;
end