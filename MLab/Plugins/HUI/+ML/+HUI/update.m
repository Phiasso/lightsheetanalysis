function update(varargin)
%ML.HUI.update
%
%   See also: ML.HUI.

% === Inputs ==============================================================

in = ML.Input;
in.id = @ischar;
in.tab = @isnumeric;
in.tag = @ischar;
in.key = @ischar;
in.val = @ischar;
in = +in;

% =========================================================================

% --- Get HUI object
H = ML.HUI.manager('get', in.id);

% --- Set update
H.set_widget(struct('tab', in.tab, 'tag', in.tag), in.key, in.val);