function tag = wait(this, varargin)
%ML.HUI.wait HUI wait for resume
%
%   See also: ML.HUI.

uiwait(this.fig)