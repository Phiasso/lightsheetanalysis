function tag = link(this, varargin)
%ML.HUI.link HUI link
%   ELM = ML.HUI.LINK(TEXT) Creates a HUI link containing TEXT. ELM
%   is a html string which can be used as an input in ML.HUI.cell.
%
%   ML.HUI.LINK(..., EVENT, CALLBACK) adds a callback related to an 
%   event. Possible values for EVENT are:
%   - 'click'
%   - 'mouseover'
%   - 'mouseout'
%
%   See also: ML.HUI, ML.HUI.cell.

% === Inputs ==============================================================

in = ML.Input;

if this.grid_layout
    in.row = @isnumeric;
    in.col = @isnumeric;
else
    in.pos = @isnumeric;
end
in.text = @ischar;
in.tab(this.current_tab) = @isnumeric;
in.tag('') = @ischar;
in.style('') = @ischar;

in.data(struct()) = @isstruct;
in.click('') = @ML.isfunction_handle;
in.mouseover('') = @ML.isfunction_handle;
in.mouseout('') = @ML.isfunction_handle;
in.visible(true) = @islogical;

in = +in;

% =========================================================================

% --- Define widget
in.type = 'link';
in.tag = this.def_widget(in);

% --- Output
if nargout
    tag = in.tag;
end