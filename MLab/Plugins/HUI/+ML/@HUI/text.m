function tag = text(this, varargin)
%ML.HUI.txt Text shortcut
%   ML.HUI.TEXT([ROW COL], TXT) adds a text widget in the cell [ROW COL]
%   of the current grid.
%
%   ML.HUI.TEXT(..., PARAM, VALUE) defines PARAM / VALUE couples. Valid
%   options are those that a <p> tag can handle. VALUE can be either a 
%   string or a numeric value.
%
%   See also: ML.HUI.

% === Inputs ==============================================================

in = ML.Input;

if this.grid_layout
    in.row = @isnumeric;
    in.col = @isnumeric;
else
    in.pos = @isnumeric;
end
in.text = @ischar;
in.tab(this.current_tab) = @isnumeric;
in.tag('') = @ischar;
in.style('') = @ischar;

in.visible(true) = @islogical;

in = +in;

% =========================================================================

% --- Define widget
in.type = 'text';
in.tag = this.def_widget(in);

% --- Output
if nargout
    tag = in.tag;
end