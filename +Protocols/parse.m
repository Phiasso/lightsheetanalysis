function [onsets, durations] = parse(chemin)
% This function reads a Lightsheet Engine parameters file and return
% digital stimuli onsets and durations.
%
% INPUTS :
% ------
% chemin : full path to the parameters file.
%
% OUTPUTS :
% -------
% onsets : n_stim x 1 vector, contains stiumuli onsets in seconds.
% durations : n_stim x 1 vector, contains stimuli durations in seconds.

fid = fopen(chemin, 'r');   % open files

c = 0;  % initialise line counter
read_string = '';

while ~strcmp(read_string, '# Digital Signals')
    read_string = fgetl(fid); % read line by line
    c = c + 1;
end

% c + 3 is the beginning of stim onsets

data = readtable(chemin, 'HeaderLines', c + 3, 'Delimiter', '\t', ...
    'MultipleDelimsAsOne', true, 'ReadVariableNames', false);

stim_onset = data(:, 2);
stim_duration = data(:, 3);
onsets = table2array(stim_onset);
durations = table2array(stim_duration);

fclose(fid);