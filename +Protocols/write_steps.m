% 2018-07-03 - Write steps protocols for Lightsheet Engine.

clear
close all
clc

% Options
% -------
output_dir = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Protocols/';
ds_out = 1;         % digital stimuli number : 1, 2
n_stim = 3;         % number of stimuli
duration = 30;      % duration of stimuli (seconds)
dt = 5*60;          % time between two stimuli (seconds)
dspl = 'y';         % show result

name = 'protocol_steps_30s_3.txt';

% ---
template_path = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Protocols/Template.txt';

makeline = @(ds, onset, duration) ['DS' num2str(ds) '\t' num2str(onset) '\t' num2str(duration) '\n'];

% Copy the template
copyfile(template_path, [output_dir name]);

% Open the template
fid = fopen([output_dir name], 'a');

t1 = randi(120);
fprintf(fid, ['\n' makeline(ds_out, t1, duration)]);

for idx_stim = 1:n_stim-1
    
    t1 = t1 + dt;
    
    fprintf(fid, makeline(ds_out, t1, duration));
end

fclose(fid);

switch dspl
	case 'y'
		figure;
		onsets = Protocols.parse([output_dir name]);
		t = linspace(0, max(onsets), 1000);
		dt = max(onsets)/1000;
		ofr = fix(onsets/dt);
		S = zeros(size(t));
		S(ofr) = 1;
		plot(t, S);
end