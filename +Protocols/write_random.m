% 2018-06-04 - Write random protocols for Lightsheet Engine.

clear
close all
clc

% Options
% -------
output_dir = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Protocols/';
ds_out = 1;     % digital stimuli number : 1, 2
n_stim = 20;    % number of stimuli
duration = 1;   % duration of stimuli (seconds)
min_dt = 10;    % minimal time between two stim
max_dt = 30;    % max time between two stim
dspl = 'y';		% show result

name = 'protocol_rand_20_1s_DS1.txt';

% ---
template_path = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/Protocols/Template.txt';

makeline = @(ds, onset, duration) ['DS' num2str(ds) '\t' num2str(onset) '\t' num2str(duration) '\n'];

% Copy the template
copyfile(template_path, [output_dir name]);

% Open the template
fid = fopen([output_dir name], 'a');

t1 = randi([min_dt max_dt]);
fprintf(fid, ['\n' makeline(ds_out, t1, duration)]);

for idx_stim = 1:n_stim
    
    t1 = t1 + randi([min_dt max_dt-1]) + rand;
    
    fprintf(fid, makeline(ds_out, t1, duration));
end

fclose(fid);

switch dspl
	case 'y'
		figure;
		onsets = Protocols.parse([output_dir name]);
		t = linspace(0, max(onsets), 1000);
		dt = max(onsets)/1000;
		ofr = fix(onsets/dt);
		S = zeros(size(t));
		S(ofr) = 1;
		plot(t, S);
end