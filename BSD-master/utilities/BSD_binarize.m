function [binN,Labels]=BSD_binarize(N,threshold,timeTolerance,dt)

Labels(:,1)=find(N>=threshold);
if timeTolerance>dt % cluster the 'part of spikes' into big spikes.
    if size(Labels,1)>1
        Z = linkage(Labels(:,1),'ward','euclidean');
        c = cluster(Z,'Cutoff',timeTolerance/dt,'Criterion','distance');
        
        Labels(:,2)=c;
    else
        Labels(:,2)=1;
        c=1;
        
    end
    
    binN=zeros(max(c),3);
    for k=1:max(c)
        Positions=Labels(:,2)==k;
        binN(k,1)=sum(Labels(Positions,1).*N(Labels(Positions,1)))/sum(N(Labels(Positions,1)));
        binN(k,2)=sum(N(Labels(Positions,1)));
        binN(k,3)=sqrt(sum(Labels(Positions,1).^2.*N(Labels(Positions,1)))/sum(N(Labels(Positions,1)))-binN(k,1)^2);
    end
    [~, I] = sort(binN(:, 1));
    binN= binN(I,:);
else % no clustering.
    if size(Labels,1)>1
        Labels(:,2) = 1:length(Labels);
        binN=zeros(size(Labels,1),3);
        binN(:,1) = Labels(:,1);
        binN(:,2) = N(Labels(:,1));
    else
        Labels(:,2) = Labels(:,1);
        binN = zeros(size(Labels,1),3);
    end
end

