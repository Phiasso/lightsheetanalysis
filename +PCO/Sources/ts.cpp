#include <iostream>
#include "tiffio.h"

// -------------------------------------------------------------------------
// Pre-requisites: 
//  - libtiff4-dev
//
// Compilation:
//  - g++ -o ts ts.cpp -ltiff
// -------------------------------------------------------------------------

using namespace std;

main(int argc, char* argv[]) {
    
    // Disable warnings
    TIFFSetWarningHandler (0);
        
    // Read Tiff file
    TIFF* tif = TIFFOpen(argv[1], "r");
    if (tif) {

        // Define buffer
        uint16* buf = NULL;
        uint32 w;
        TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w);
        buf = new uint16 [w];
        
        // Read first line
        TIFFReadScanline(tif, buf, 0);        
        for (int i=0; i<14; i++) {
            cout << buf[i] << ' ';
        }
        cout << endl;
        
        TIFFClose(tif);         
    }
    
    return 0;
}