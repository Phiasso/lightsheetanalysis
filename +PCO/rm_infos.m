function img = rm_infos(img, varargin)
%PCO.RM_INFOS Remove information in the upper-left part of a PCO image.
%
%*  See also: PCO.timestamp.

% === Input variables =====================================================

in = inputParser;
in.addRequired('img', @(x) isnumeric(x) | isa(x, 'Image'));
in.addParamValue('rep', 0, @isnumeric);

in.parse(img, varargin{:});
in = in.Results;

% === Parameters ==========================================================

x = 1:10;
y = 1:300;

% =========================================================================

% --- Manage Image objects
if isa(img, 'Image')
    in.img = in.img.pix;
end

% --- Processing
in.img(x,y) = in.rep;

% --- Output
if isa(img, 'Image')
    img.pix = in.img;
else
    img = in.img;
end