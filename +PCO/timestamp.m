function ts = timestamp(img, varargin)
%PCO.TIMESTAMP Get the timestamp from PCO images.
%*  TS = PCO.TIMESTAMP(IMG) get the timestamp from a PCO image. TS is a
%   struct with fields:
%   * 't':      The timestamp itself (µs)
%   * 'min':    The minute
%   * 'hour':   The hour
%   * 'day':    The day
%   * 'month':  The month
%   * 'year':   The year
%   * 'text':   The date, in text format
%
% See also: .

% === Input variables =====================================================

in = inputParser;
in.addRequired('img', @(x) ischar(x) | isnumeric(x) | isa(x, 'Image'));

in.parse(img, varargin{:});
in = in.Results;

% =========================================================================

% --- Prepare output
ts = struct('t', 0);

if ischar(in.img)
   
    try
        [~, stat] = unix(['./+PCO/Bin/ts ''' in.img '''']);
        raw = uint16(str2double(Text.split(stat, ' ')));
    catch
        in.img = Image(in.img);
    end
end

if ~exist('raw', 'var')
    % --- Handles Image objects
    if isa(in.img, 'Image')
        img = in.img.pix;
    else
        img = in.img;
    end
    
    % Get raw bits
    raw = uint16(img(1,1:14));
end

% --- Process

extract = @(n) double(mod(raw(n),16) + 10*idivide(raw(n),16));

% Get time in microseconds
for n = 11:14
    p = (14-n)*2;
    ts.t = ts.t + double(mod(raw(n),16))*10^p + double(idivide(raw(n),16))*10^(p+1);
end

% Get times
ts.sec = ts.t/10^6;
ts.min = extract(10);
ts.hour = extract(9);
ts.day = extract(8);
ts.month = extract(7);
ts.year = extract(6) + extract(5)*100;

% Get number
ts.num = extract(4) + 100*(extract(3) + 100*(extract(2) + 100*(extract(1))));
      
% Get final timestamp
ts.t = ts.t + 10^6*(60*(ts.min + 60*ts.hour));

% Text format
ts.text = sprintf('%i-%i-%i %i:%i:%.06f', ts.year, ts.month, ts.day, ts.hour, ts.min, ts.sec);

