function minimum = min2(A)
% Returns the miminum value of 2D array

minimum = min(min(A));

end