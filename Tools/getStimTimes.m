function [stim_onset, stim_duration] = getStimTimes(chemin)
% this function reads the parameters file to get digital stimuli (DS)
% onsets and duration. The Parameters.txt file should be named
% 'ParametersN.txt' where N is the run number.
% 2017-10-19 GLG
% Update 2017-11-14 - Now detects automatically the size of header by
% looking for line 'Digital Stimuli'.
% Update 2018-07-03 - Now uses Parameters class.

run = num2str(str2double(chemin(end-2:end-1))); % convert run number to %01i

% Try both names
fparam1 = [chemin 'Parameters.txt'];  % create filename
fparam2 = [chemin 'Parameters' run '.txt'];  % create filename

if ~exist(fparam1, 'file')
    fparam = fparam2;
else
    fparam = fparam1;
end

% Try to parse Parameters with Parameters object
P = NT.Parameters;
P.load(fparam);

if ~isempty(P.Signals.DS.tstart)
    
    stim_onset = P.Signals.DS.tstart;
    stim_duration = P.Signals.DS.tstop - P.Signals.DS.tstart;
    
else
    
    fid = fopen(fparam, 'r');   % open files
    
    c = 0;  % initialise line counter
    read_string = '';
    
    while ~strcmp(read_string, '# Digital Signals')
        read_string = fgetl(fid); % read line by line
        c = c + 1;
    end
    
    % c + 3 is the beginning of stim onsets
    
    data = readtable(fparam, 'HeaderLines', c + 3, 'Delimiter', '\t', ...
        'MultipleDelimsAsOne', true, 'ReadVariableNames', false);
    
    if size(data, 2) == 1
        data = readtable(fparam, 'HeaderLines', c + 3, 'Delimiter', '   ', ...
            'MultipleDelimsAsOne', true, 'ReadVariableNames', false);
    end
    
    stim_onset = data(:, 2);
    stim_duration = data(:, 3);
    stim_onset = table2array(stim_onset);
    stim_duration = table2array(stim_duration);
    
    fclose(fid);
    
end
end