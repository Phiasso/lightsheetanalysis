function [] = renameParameters(base, study, dat, runs)
% This function renames the 'Parameters.txt' file found in
% base/study/dat/runs to ParametersN.txt where N is the run number.
% 2017-11-17 GLG

% Format path
checkdate = datevec(dat);
if checkdate(1) < 2018 && checkdate(2) < 9
    path_to_day = [base dat ' ' filesep];
else
    path_to_day = [base study filesep dat filesep];
end

% Rename files if it exists
for run = runs
    
    path_to_run = [path_to_day 'Run ' sprintf('%02i', run) filesep];
    oldname = [path_to_run 'Parameters.txt'];
    newname = [path_to_run 'Parameters' sprintf('%01i', run) '.txt'];
    if exist(oldname, 'file')
        movefile(oldname, newname);
        fprintf('%s, %s, Run %02i, Parameters file renamed.\n', study, dat, run);
    else
        fprintf('%s, %s, Run %02i, Parameters file not found.\n', study, dat, run);
    end
    
end
end