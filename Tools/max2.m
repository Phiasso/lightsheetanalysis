function maximum = max2(A)
% Returns the maximum value of a 2D array

maximum = max(max(A));
end