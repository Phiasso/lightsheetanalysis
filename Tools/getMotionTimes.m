function motion_times = getMotionTimes(F, layer, motion_threshold, time_after_motion, sym)
% This function wraps extract_motion_times_glg function with a focus
% object and returns index of times steps where a motion occured

% Default value
if ~exist('sym', 'var')
    sym = 'sym';
end

F.select(layer);
Drifts = load(F.fname('IP/@Drifts')); % Load drifts
drift = [ Drifts.dx' Drifts.dy'];
dt_brain = length(F.sets).*F.dt.*1e-3;
after_motion_frame = round(time_after_motion./dt_brain);

motion_times = extract_motion_times_glg(drift, motion_threshold, after_motion_frame, sym);