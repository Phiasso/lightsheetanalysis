function Z = getTriangleAltitude(P)
% This function reads a Parameter object to compute mean z altitude of each
% layers, when the slave Horizontal Mirror (HM) and triangle Vertical 
% Mirror (VM) method is used.
%
% INPUTS :
% ------
% P : Parameter object or string giving the path to the paramters file.
% delay_before : double, specify the time spent before the HM starts
% scanning in milliseconds (ms)
% delay_after : double, specify the time spent after the HM finishes
% scanning in milliseconds (ms)
%
% OUTPUTS :
% -------
% Z : n_layers x 1 array, gives the real mean Z attitude of each layer.
%
% 2018-07-02 GLG

% Default values
% --------------
Rate = 1e5;
dt = 1/Rate;

% Check input
% -----------
if ~isa(P, 'NT.Parameters')
    pfile = P;
    P = NT.Parameters;
    P.load(pfile);
end

% Get experiment settings
% -----------------------
% - Vertical mirror
Exposure = P.Exposure/1000;         % exposure
Delay = P.Delay/1000;               % delay
Nlayers = P.NLayers;                % number of layers (multiple of 2)
Height = (Nlayers - 1)*P.Increment; % total height

% Compute vertical mirror waveform
Nup = Nlayers/2;
Ndown = Nlayers/2;
dataVert = circshift([linspace(0, Height, round((Exposure*Nup + Delay*(Nup))/dt)) ...
    linspace(Height, 0, round((Exposure*Ndown + Delay*(Ndown))/dt))], -round(Delay/dt));
nSamples = numel(dataVert);

% - Shutter
if ~exist('delay_before', 'var')
    DelayBefore = P.DelayBefore/1000;
else
    DelayBefore = delay_before/1000;
end
if ~exist('delay_after', 'var')
    DelayAfter = P.DelayAfter/1000;
else
    DelayAfter = delay_after/1000;
end

% Compute shutter waveform
Nb = round(DelayBefore/dt);
Na = round(DelayAfter/dt);
Nt = round(Exposure/dt)-Nb-Na;
Np = round(Delay/dt);

dataShut = [];
for j = 1:Nlayers
    dataShut = [dataShut zeros(1, Nb) ones(1, Nt) zeros(1, Na + Np)];
end

% Apply shutter mask on vertical trajectory
shm = dataShut(1:nSamples);
shm(shm==0) = NaN;
yv = dataVert(1:nSamples);
yvm = yv.*shm;

% Determine z altitude
% --------------------
% - Initialisation
subZ = 0;
c = 0;
l = 0;
Z = NaN(Nlayers, 1);

% Find blocks of numbers (non-NaN) and take the mean
for i = 2:numel(yvm)
    
    if ~isnan(yvm(i))
        
        subZ = subZ + yvm(i);
        c = c + 1;
        
    elseif isnan(yvm(i)) && ~isnan(yvm(i - 1))
        
        l = l + 1;
        
        subZ = subZ./c;
        
        Z(l) = subZ;
        
        subZ = 0;
        c = 0;
        
    end
end