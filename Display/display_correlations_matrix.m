% 2017-11-29 Rewrite Wolf's 'correction_cor_matrix_mvt' to compute
% correlation matrix.

clear
close all
clc

%% Parameters
study = 'Thermotaxis';
dat = '2018-06-07';
checkdate = datevec(dat);
run = 3;
layer = 4;
dff_to_use = 'cor';       % display DFFcor or DFFcorcor
motion_thresh = 5.5;            % discard images with motion speed > motion_thresh*std
time_after_im = 10;           % discard X frames after a motion
start_time = 470;        % first time to consider in the analysis (SECONDS)
end_time = 'end';

% Get Focus
if checkdate(2) < 9 && checkdate(1) < 2018
    F = getFocus([study '_Old' filesep dat ' ' study], run);
else
    F = getFocus([study filesep dat], run);
end

fprintf('%s', F.name);
ML.CW.line;

% Complete parameters
nlayers = length(F.sets);
dt_brain = nlayers*(F.dt*1e-3);

start_frame = round(start_time/dt_brain);

% Select layer
F.select(layer);

% Prepare filenames
DFF = F.matfile('@DFF');
DFFprime = F.matfile('@DFFprime');
Neurons = F.matfile('IP/@Neurons');

% Load files
fprintf('Loading DFF...\n');
if strcmp(dff_to_use, 'corcor')
    DFF = DFFprime.load('DFFcorcor');     % DFFcorcor
    DFF = DFF.DFFcorcor;
elseif strcmp(dff_to_use, 'cor')
    DFF = DFF.load('neurons');            % DFFcor
    DFF = DFF.neurons;
end
fprintf('Done.\n');

% Time
t = (F.set.t).*1e-3;        % in seconds
Ntimes = numel(F.set.t);   % number of frames per layer

% Remove stimulation with motion
fprintf('Discarding frames with motions...\n');
motion_times = getMotionTimes(F, layer, motion_thresh, time_after_im, 'sym');
DFF_wm = DFF;
DFF_wm(:, motion_times) = 0;    % DFF without motions
fprintf('Done, %i/%i frames discarded.\n', numel(motion_times), Ntimes);

% Start from specified point
if ischar(end_time)
    if strcmp(end_time, 'end')
        DFF_wm = DFF_wm(:, start_frame:end);
    end
else
    end_frame = round(end_time/dt_brain);
    DFF_wm = DFF_wm(:, start_frame:end_frame);
end
        
tic;
fprintf('Computing correlation matrix...');
correlation_matrix = computeCorrelationMatrix(DFF_wm');  % correlation matrix
fprintf('Correlation matrix calculated in %2.2fs\n', toc);

% Display
Display.correlations_matrix;