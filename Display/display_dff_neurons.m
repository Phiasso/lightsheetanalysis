% 2017-11-07 Display raw DFF of the neuron the user click on

clear
close all

%% Parameters
study = 'Thermotaxis';
dat = '2018-07-03';
checkdate = datevec(dat);
run = 2;
layer = 3;
what_to_display = 'cor';       % display DFFcor or DFFcorcor

% Get Focus
if checkdate(2) < 9 && checkdate(1) < 2018
    F = getFocus([dat ' ' study], run);
else
    F = getFocus([study filesep dat], run);
end

Display.dff_neurons;