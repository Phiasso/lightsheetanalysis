% 2018-03-13 Display selected neuron DF/F with mean DF/F around
% stimulation. Also plots the stimulations from calibration.

clear
close all

%% Parameters
study = 'Thermotaxis';
dat = '2018-01-31';
checkdate = datevec(dat);
run = 3;
layer = 4;
what_to_display = 'cor';       % display DFFcor or DFFcorcor
t_before = 2;       % time before stim for average
t_after = 10;       % time after stim for average
Tset = 29;
plot_real_stim = 'y';
calib_name = 'calib';

% Get Focus
if checkdate(2) < 9 && checkdate(1) < 2018
    F = getFocus([dat ' ' study], run);
else
    F = getFocus([study filesep dat], run);
end

Display.dff_neurons_stim;