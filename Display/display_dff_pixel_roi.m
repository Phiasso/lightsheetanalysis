% 2018-03-19 Display DF/F by pixels in the selected ROI.

clear;
close all
clc

% Experiment
% ----------
study = 'Thermotaxis';
dat = '2018-01-31';
checkdate = datevec(dat);
run = 6;
layer = 2;

% Get Focus
if checkdate(2) < 9 && checkdate(1) < 2018
    F = getFocus([dat ' ' study], run);
else
    F = getFocus([study filesep dat], run);
end

F.select(layer);

Display.dff_pixels_roi;