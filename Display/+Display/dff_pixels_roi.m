% Get stimulation onsets
stim = getStimTimes(F.Data);
stim_frames = fix(stim/((F.dt)*1e-3*numel(F.sets)));

% Prepare filenames
DFF_name = [F.Data 'signal_stacks_glg' filesep num2str(layer) filesep 'dff.mat'];
avg_name = [F.Data 'grey_stack_glg' filesep 'Image_' sprintf('%02i', layer) '.tif'];

% Load files
dff = load(DFF_name);
index = dff.dff.index;
dff = dff.dff.signal_stack;

% Brain contour
Brain = matfile(F.fname('IP/@Brain'));
bbox = Brain.bbox;

% Time
t = F.sets.t;
t = t.*1e-3;

mean_image_ori = imread(avg_name);
figure;
ax = subplot(2,1,1);
imshow(rescalegd(mean_image_ori));

while 1==1
    
    mean_image = mean_image_ori;
    
    h = imfreehand(ax);
    mask = h.createMask;
    h.delete;
    roi_pix = find(mask);
    idx_pix = ismember(index, roi_pix);
    dff_roi = mean(dff(idx_pix, :), 1);
    
    if roi_pix > 0
        
        mean_image(roi_pix) = 0;  % put the pixel in black
        subplot(2,1,1);
        imshow(rescalegd(mean_image));
        
        % Plot DFF trace below mean image
        subplot(2,1,2)
        hold off;
        plot(t, dff_roi);
        ylabel('\Delta{F}/F');
        xlabel('Time [s]');
        hold on;
        
        % Stimulation representation
        supe = max(dff_roi);
        infe = min(dff_roi);
        w = abs(supe) + abs(infe);
        
        for n = 1:length(stim)
            p = patch([stim(n) stim(n) stim(n)+5 stim(n)+5],[infe supe supe infe], [0 0 0]);
            set(p,'FaceAlpha',0.2);
            set(p,'EdgeColor','none')
        end
    elseif isempty(roi_pix)
        title('No pixel here...');
    end
    
end