% Select layer
F.select(layer);

% Get stimulation onsets
[stim, durations] = getStimTimes(F.Data);

% Prepare filenames
DFF = F.matfile('@DFF');
DFFprime = F.matfile('@DFFprime');
Neurons = F.matfile('IP/@Neurons');

% Load files
if strcmp(what_to_display, 'corcor')
    DFF = DFFprime.load('DFFcorcor');     % DFFcorcor
    DFF = DFF.DFFcorcor;
elseif strcmp(what_to_display, 'cor')
    DFF = DFF.load('neurons');            % DFFcor
    DFF = DFF.neurons;
end

% Time
t = F.sets.t;
t = t.*1e-3;

% Neurons index
neuron_index = Neurons.load('ind');
neuron_index = neuron_index.ind;
Nneurons = length(DFF(:, 1));            % number of neurons

% Brain contour
Brain = F.matfile('IP/@Brain');
bbox = Brain.load('bbox');
bbox = bbox.bbox;
L = zeros(bbox(2) - bbox(1) + 1, bbox(4) - bbox(3) + 1)';

for i = 1:Nneurons
    L(neuron_index{1,i}) = i;
end

%% Display mean image
figure;
subplot(2,1,1);
mean_image = imread(F.fname('IP/@Mean', 'png'));
imshow(rescalegd(mean_image))

L1=0*L;

% Begin UI crosshair
while 1==1
    for i = 1:Nneurons
        ind = cell2mat(neuron_index(i)) ;
        L1(ind) = i;
    end
    
        [y,x] = ginput(1);                  % select neuron
        w = find(L==L(ceil(x),ceil(y)));    % get neuron index
        [x,y] = ind2sub(size(L),w);         % convert to coordinates
        wneuron = L1(x(1),y(1));
        disp(wneuron);
        
        if wneuron > 0
            
            w = cell2mat(neuron_index(wneuron));
            mean_image(w) = 0;  % put the neuron in black
            subplot(2,1,1)
            imshow(rescalegd(mean_image));
            
            % Plot DFF trace below mean image
            subplot(2,1,2)
            hold off;
            plot(t, DFF(wneuron, :));
            ylabel('\Delta{F}/F');
            xlabel('Time [s]');
            title(['Neuron #' num2str(wneuron)]);
            hold on;
            
            % Stimulation representation
            supe = max(DFF(wneuron, :));
            infe = min(DFF(wneuron, :));
            
            for n = 1:length(stim)
                p = patch([stim(n) stim(n) stim(n)+durations(n) stim(n)+durations(n)], ...
                    [infe supe supe infe], [0 0 1]);
                set(p,'FaceAlpha',0.2);
                set(p,'EdgeColor','none')
            end
        elseif wneuron == 0
            title('No neuron here...');
        end
end