% Select layer
F.select(layer);

% Get stimulation onsets
stim = getStimTimes(F.Data);
stim_frames = fix(stim/((F.dt)*1e-3*numel(F.sets)));

% Prepare filenames
DFF_name = [F.Data 'signal_stacks_glg' filesep num2str(layer) filesep 'dff.mat'];
avg_name = [F.Data 'grey_stack_glg' filesep 'Image_' sprintf('%02i', layer) '.tif'];

% Load files
DFF = load(DFF_name);
dff = DFF.dff.signal_stack;
index = DFF.dff.index;

% Time
t = F.sets.t;
t = t.*1e-3;

%% Display mean image
mean_image = imread(avg_name);
img_size = size(mean_image);
figure;
subplot(2,1,1);
imshow(rescalegd(mean_image));

% Begin UI crosshair
while 1==1
    
    for i = 1:10
        [x, y] = ginput(1);                  % select pixel
        x = ceil(x);
        y = ceil(y);
        
        idx_px = sub2ind(img_size, y, x);
        pix = find(index == idx_px);
        
        disp(pix);
        
        if pix > 0
            
            mean_image(y, x) = 0;  % put the pixel in black
            subplot(2,1,1);
            imshow(rescalegd(mean_image));
            
            % Plot DFF trace below mean image
            subplot(2,1,2)
            hold off;
            plot(t, dff(pix, :));
            ylabel('\Delta{F}/F');
            xlabel('Time [s]');
            title(['Pixel #' num2str(pix)]);
            hold on;
            
            % Stimulation representation
            supe = max(dff(pix, :));
            infe = min(dff(pix, :));
            w = abs(supe) + abs(infe);
            
            for n = 1:length(stim)
                p = patch([stim(n) stim(n) stim(n)+5 stim(n)+5],[infe supe supe infe], [0 0 0]);
                set(p,'FaceAlpha',0.2);
                set(p,'EdgeColor','none')
            end
        elseif isempty(pix)
            title('No pixel here...');
        end
    end
end