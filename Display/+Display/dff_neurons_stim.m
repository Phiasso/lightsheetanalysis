% Select layer
F.select(layer);

% Get stimulation onsets
stim = getStimTimes(F.Data);
stim_frames = fix(stim/((F.dt)*1e-3*numel(F.sets)));

% Prepare filenames
DFF = F.matfile('@DFF');
DFFprime = F.matfile('@DFFprime');
Neurons = F.matfile('IP/@Neurons');

% Load files
if strcmp(what_to_display, 'corcor')
    DFF = DFFprime.load('DFFcorcor');     % DFFcorcor
    DFF = DFF.DFFcorcor;
elseif strcmp(what_to_display, 'cor')
    DFF = DFF.load('neurons');            % DFFcor
    DFF = DFF.neurons;
end

% Time
t = F.sets.t;
t = t.*1e-3;

% Neurons index
neuron_index = Neurons.load('ind');
neuron_index = neuron_index.ind;
Nneurons = length(DFF(:, 1));            % number of neurons

% Brain contour
Brain = F.matfile('IP/@Brain');
bbox = Brain.load('bbox');
bbox = bbox.bbox;
L = zeros(bbox(2) - bbox(1) + 1, bbox(4) - bbox(3) + 1)';
for i = 1:Nneurons
    L(neuron_index{1,i}) = i;
end

switch plot_real_stim
    case 'y'
        calib_filename = [F.Data(1:end-7) 'Calibration' filesep calib_name '_Tset=' num2str(Tset) '.mat'];
        stim_calib = load(calib_filename);
end

%% Display mean image
figure;
subplot(2,1,1);
mean_image = imread(F.fname('IP/@Mean', 'png'));
imshow(rescalegd(mean_image))

L1=0*L;

% Begin UI crosshair
while 1==1
    for i = 1:Nneurons
        ind = cell2mat(neuron_index(i)) ;
        L1(ind) = i;
    end
    
    for i = 1:10
        [y,x] = ginput(1);                  % select neuron
        w = find(L==L(ceil(x),ceil(y)));    % get neuron index
        [x,y] = ind2sub(size(L),w);         % convert to coordinates
        wneuron = L1(x(1),y(1));
        disp(wneuron);
        
        if wneuron > 0
            
            w = cell2mat(neuron_index(wneuron));
            mean_image(w) = 0;  % put the neuron in black
            subplot(2,2,[1 2])
            imshow(rescalegd(mean_image));
            
            DFF_oi = DFF(wneuron, :);
            
            [peristim_time, peristim_sig] = periStimMean(DFF_oi', stim, t_before, t_after, F);
            
            % Plot DFF trace below mean image
            subplot(2,2,3)
            yyaxis left
            plot(t, DFF_oi);
            ylabel('\Delta{F}/F');
            title(['Neuron #' num2str(wneuron)]);
            
            % Stimulation representation
            switch plot_real_stim
                case 'y'
                    yyaxis right
                    p = plot(stim_calib.trace.time, stim_calib.trace.temperature);
                    ylabel('Temperature [°C]');
                    p.Color(4) = 0.4;
                otherwise
                    supe = max(DFF(wneuron, :));
                    infe = min(DFF(wneuron, :));
                    
                    for n = 1:length(stim)
                        p = patch([stim(n) stim(n) stim(n)+2 stim(n)+2],[infe supe supe infe], [0 0 1]);
                        set(p,'FaceAlpha',0.2);
                        set(p,'EdgeColor','none')
                    end
            end
            xlabel('Time [s]');
            
            % Plot mean DFF trace around stim
            subplot(2, 2, 4)
            yyaxis left
            plot(peristim_time, peristim_sig, 'LineWidth', 1.5);
            ylabel('\Delta{F}/F');
            
            switch plot_real_stim
                case 'y'
                    yyaxis right
                    plot(stim_calib.mean_time, stim_calib.mean_temp);
                    ylabel('Temperature [°C]');
            end
            
            xlabel('Time around stimulation [s]');
            
        elseif wneuron == 0
            title('No neuron here...');
        end
    end
end