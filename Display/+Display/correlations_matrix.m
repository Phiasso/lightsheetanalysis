% 2017-11-29 Display correlations above specified threshold on the mean
% fish. Run 'compute_correlation_matrix' before.
%close all

% Neurons index
neuron_index = Neurons.load('ind');
neuron_index = neuron_index.ind;
Nneurons = length(DFF(:, 1));            % number of neurons

% Brain contour
Brain = F.matfile('IP/@Brain');
bbox = Brain.load('bbox');
bbox = bbox.bbox;
L = zeros(bbox(2) - bbox(1) + 1, bbox(4) - bbox(3) + 1)';

for i = 1:Nneurons
    L(neuron_index{1,i}) = i;
end

correl_neg = -0;   % show (anti)correlation > thresh
correl_pos = 0;

% Mean image
mean_img = double(imread(F.fname('IP/@Mean', 'png')));

hfig = figure;
imshow(rescalegd(mean_img))

L1=0*L;
L1p = zeros(size(mean_img));

% Begin UI crosshair
while 1==1
    for i = 1:Nneurons
        ind = cell2mat(neuron_index(i)) ;
        L1(ind) = i;
    end
    
    %for i = 1:10
    [y,x] = ginput(1);                  % select neuron
    w = find(L == L(ceil(x), ceil(y)));    % get neuron index
    [x,y] = ind2sub(size(L),w);         % convert to coordinates
    wneuron = L1(x(1),y(1));
    disp(wneuron);
    
    % Prepare RGB image
    L1p = 0*L1p;
    
    if wneuron > 0
        
        w = cell2mat(neuron_index(wneuron));
        mean_img(w) = 0;  % put the neuron in black
        imshow(rescalegd(mean_img));
        
        for h = 1:length(correlation_matrix(:, 1))
            
            ind2=cell2mat(neuron_index(h));
            
            if correlation_matrix(wneuron, h) < correl_neg
                
                L1p(ind2) = correlation_matrix(wneuron, h);
                
            elseif correlation_matrix(wneuron, h) > correl_pos
                
                L1p(ind2) = correlation_matrix(wneuron, h);
                
            end
            
            %L1p(ind2) = correlation_matrix(wneuron, h);
            
%             if h == wneuron
%                 L2p(ind2) = 1;
%             end
            
        end
        
        clf;
        corrClim = [min2(L1p) max2(L1p)];       % values limit
        
        if corrClim(1) == 0 && corrClim(2) == 0
            corrClim = [-1 1];
        end
        
        im_overlay(mean_img, L1p, corrClim, [], jet, [], hfig);
        colorbar;
        
    elseif wneuron == 0
        title('No neuron here...');
    end
    %end
end