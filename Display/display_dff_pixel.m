% 2017-12-18 Display raw DFF of the pixel the user click on

clear
close all

%% Parameters
study = 'Thermotaxis';
dat = '2018-01-31';
checkdate = datevec(dat);
run = 3;
layer = 1;

% Get Focus
if checkdate(2) < 9 && checkdate(1) < 2018
    F = getFocus([dat ' ' study], run);
else
    F = getFocus([study filesep dat], run);
end

Display.dff_pixels;