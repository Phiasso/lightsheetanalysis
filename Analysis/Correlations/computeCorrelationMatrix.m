function correlation_matrix = computeCorrelationMatrix(X)
% Computes the correlation matrix, removing first component of PCA.
% Warning : X must have time series on columns !

correlation_matrix = corrcoef(X);
correlation_matrix(isnan(correlation_matrix)) = 0;  % Remove NaN
[v, lambda] = eig(correlation_matrix);              % Compute eigenvalues/vectors
lambda = real(diag(lambda));                        % Get eigenvalues
[lambda_sorted, index_sorted] = sort(lambda);
correlation_matrix = correlation_matrix - v(:, index_sorted(end))*v(:, index_sorted(end))'*lambda_sorted(end);