% 2017-11-17 Prepare for Routines: rename parameter file and reeq. number
% of zeros in images names.

base = '/home/ljp/Science/Projects/Neurofish/Data/';
study = 'Thermotaxis';
dat = '2018-07-04';
runs = 7;

% Choose what to do
% rename_param = 1;
reeq_numzero = 1;

% if rename_param
%     renameParameters(base, study, dat, runs);
% end

if reeq_numzero
    reformatImagesName(base, study, dat, runs);
end