% 2017-10-25 Re-write worksheet_real_signals and add signal and dff
% extraction codes

% Parameters
% -------------------------------------------------------------------------
% study = 'Thermotaxis';
% dat = '2018-07-03';
% run = 2;

% Specify what to perform after Routines
perform_signal_stacks = 1;
perform_dff_extraction = 1;

% Options
regist = 0;             % perform registration 
%sigma = 5*[1 1.5];     % Gaussian filter sigma for signal_stacks
binsize = 1;            % binning
fast = 0;				% use parallel pool

checkdate = datevec(dat);

%Get focus
if checkdate(2) < 9 && checkdate(1) < 2018
    F = getFocus([dat ' ' study], run);
else
    F = getFocus([study filesep dat], run);
end

% Complete parameters;
n_layers = length(F.sets);

% -------------------------------------------------------------------------

% create_signal_stack
if perform_signal_stacks

    fprintf('%s : Extracting raw fluo\n', F.name);
    ML.CW.line;
    
    timer = 0;
    for layer = 1:n_layers
        
        tic
        fprintf('Layer #%i...', layer);
        
        % Select current layer
        F.select(layer);
        
        % Extrack fluo from pixels
        [signal_stack, index, greystack] = Extraction.raw_pixels(F, binsize, regist);
        
        % Prepare filenames
        sig_path = [F.Data 'signal_stacks' filesep sprintf('%i', layer) filesep];
        gre_path = [F.Data 'grey_stack' filesep];
        
        if ~exist(sig_path, 'dir')
            mkdir(sig_path);
        end
        if ~exist(gre_path, 'dir')
            mkdir(gre_path);
        end
        
        % Save signal matrix and mean image
        save([sig_path 'sig.mat'], 'signal_stack', 'index');
        imwrite(greystack, [gre_path 'Image_' sprintf('%02i', layer) '.tif']);
        
        timer = timer + toc;
        fprintf('Done (%2.2fs).\n', toc);
    end
    
    fprintf('All done in %2.2fs.\n', timer);
    
    
end

% -------------------------------------------------------------------------

% DFF extraction
if perform_dff_extraction
    
    % Define output folders
    make_output_name = @(n) [F.Data 'signal_stacks' filesep sprintf('%01i', n) filesep];
    
    fprintf('%s : Computing new baseline and DFF for pixels\n', F.name);
    ML.CW.line;
    
    timer = 0;
    for layer = 1:n_layers
        
        tic;
        fprintf('Layer %i...', layer);
        
        % Select current layer
        F.select(layer);
        
        % Compute baseline and DF/F of pixels
        [dff, baseline, noise, index] = Extraction.dff_pixels(F, fast);
        
        % Prepare filenames
        save_dff = [make_output_name(layer) 'dff.mat'];
        save_bsl = [make_output_name(layer) 'baseline.mat'];
        
        % Save baseline and dff
        save(save_dff, 'dff', 'noise', 'index');
        save(save_bsl, 'baseline');
        
        timer = timer + toc;
        fprintf('Done (%2.2fs).\n', toc);
    end
    
    fprintf('All done in %2.2fs.\n', timer);
end