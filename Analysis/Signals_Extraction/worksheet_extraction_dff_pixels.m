% 2017-12-15 Worksheet to extract DF/F from signal_stack (raw fluo time
% series per pixels). Replaces extraction_dff_glg

clear;
clc;

% Parameters
study = 'Thermotaxis';
dat = '2017-11-16';
runs = 6;
fast = 1;

checkdate = datevec(dat);

for run = runs
    % Get Focus
    if checkdate(2) < 9 && checkdate(1) < 2018
        F = getFocus([dat ' ' study], run);
    else
        F = getFocus([study filesep dat], run);
    end
    
    % Define output folders
    make_output_name = @(n) [F.Data 'signal_stacks' filesep sprintf('%01i', n) filesep];
    
    n_layers = numel(F.sets);
    
    fprintf('%s : Computing new baseline and DFF for pixels\n', F.name);
    ML.CW.line;
    
    timer = 0;
    for layer = 16
        
        tic;
        fprintf('Layer %i...', layer);
        
        % Select current layer
        F.select(layer);
        
        % Compute baseline and DF/F for pixels
        [dff, baseline, noise, index] = Extraction.dff_pixels(F, fast);
        
        % Prepare filenames
        save_dff = [make_output_name(layer) 'dff.mat'];
        save_bsl = [make_output_name(layer) 'baseline.mat'];
        
        % Save baseline and dff
        save(save_dff, 'dff', 'noise', 'index');
        save(save_bsl, 'baseline');
        
        timer = timer + toc;
        fprintf('Done (%2.2fs).\n', toc);
        
    end
    
    fprintf('All done in %2.2fs.\n', timer);
end