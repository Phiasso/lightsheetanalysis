function [dff, baseline, noise, index] = dff_pixels(F, fast)
% This functions extracts DF/F from raw signals in F.Data/signal_stacks and
% stores it in the same folder along with the baseline and the noise.
% DF/F is defined as (Fluo (- Background) - baseline)/baseline.
% Uses following functions : fast_baseline, progression_info, noise_dff.
%
% INPUT
% F : Focus object, focused on a layer (F.select(layerID))
% stepsize : the signals matrix is split into smaller matrix to speed up
% fast_baseline function.
% fast : boolean, use parfor loop or not.
%
% OUTPUT
% dff : DF/F, rows are time series of a single pixel
% baseline : baseline, rows are time series of a single pixel
% noise : computed noise
% index : pixels' index from reference image
%
% 2017-12-15 GLG

% Preparation
% -------------------------------------------------------------------------
% Fill default values
% -------------------
window = 80;        % moving window in seconds
percen = 8;         % nth percentile

% Complete parameters
% -------------------
dt = F.dt*1e-3;
Nlayer = length(F.sets);
dt_brain = dt*Nlayer;
layer = F.set.id;

% Load files
% ----------
Background = F.matfile('IP/@Background');       % Background
bg = Background.load('mean_first');
bg = bg.mean_first;         % background properly computed

fluo = load([F.Data 'signal_stacks' filesep num2str(layer)], 'signal_stack');
index = load([F.Data 'signal_stacks' filesep num2str(layer)], 'index');

% Handle old version where fluo and indices are in a structure
if ~isfield(fluo, 'signal_stack')
    data = load([F.Data 'signal_stacks' filesep num2str(layer)], 'DD');
    fluo = data.DD.signal_stack;
    index = data.DD.index;
end

% Compute baseline and DF/F
% -------------------------------------------------------------------------
signal_stack = single(fluo) - single(bg);   % convert to single and remove background

% Compute baseline
% ----------------
baseline = computeBaseline(signal_stack, percen, round(window/dt_brain), fast);
baseline = single(baseline);

baseline(baseline == 0) = 1;                % remove 0
dff = (signal_stack - baseline)./baseline;	% DF/F
noise = noise_dff(dff.signal_stack);        % compute estimated noise
end