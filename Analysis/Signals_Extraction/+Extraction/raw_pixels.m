function [signal_stack, index, greystack] = raw_pixels(F, binsize, regist)
% This functions stores raw fluo data from a set of images into a matrix
% where each row is a time serie associated to a pixel within the brain
% mask.
% INPUTS :
% ------
% F: Focus object, focused on a layer.
% binsize: Image will be resized with a factor 1/binsize.
% regist: Wether or not you want to register the image skeleton for further
% correction. Slower and induces weird effect on the mean image.

% OUTPUTs :
% -------
% signal_stack : Raw fluo signal matrix.
% index : Linear indices of pixels within the brain mask.
% greystack : Mean image, averaged over each image of the layer.

% Fill default values
% -------------------------------------------------------------------------
if regist
    sigma = 5*[1 1.5];              % Gaussian filter sigma
end

% Get the pixel list
% -------------------------------------------------------------------------
pixel_list = load(F.fname('IP/@Brain'));
pixel_list = pixel_list.ind;
pixel_list = convert2BinnedIndex(pixel_list, [F.IP.height F.IP.width], binsize);

% Start raw signal extraction
% -------------------------------------------------------------------------
% Correction preparation
% ----------------------
drift = load(F.fname('IP/@Drifts'));        % Load drift file
N_img_layer = numel(F.set.frames);          % Number of frames for this layer
Img1 = F.iload(1);                          % Load fist frame as a reference
Img1.rm_infos('rep', 100);                  % Remove timestamp
% Make a canvas from first image size
%[yim, xim] = meshgrid(1:Img1.width, 1:Img1.height);

% If eyes movement, prepare a skeleton of the picture for registration
if regist
    Gfx = imgaussfilt(Img1.pix, sigma);
    Gfy = imgaussfilt(Img1.pix, fliplr(sigma));
    Ref_1 = abs(Gfx - Gfy);
end

Img_bin = imresize(Img1.pix, 1/binsize);    % Bin the picture
sigstack = uint16(zeros(size(pixel_list, 1), N_img_layer));  % Initialise signal stacks
Img_mean = Img_bin;                         % Initialise mean image
sigstack(:, 1) = Img_bin(pixel_list);       % Fill signal stacks with first frame

% Process all images from the set
% -------------------------------
fprintf('[');
for t_image = 2:N_img_layer
    
    % Keep user informed
    if mod(t_image,fix(N_img_layer./100)) == 1
        fprintf('#');
    end
    
    Img2 = F.iload(t_image);            % Load image
    Img2.rm_infos('rep', 100);          % Remove timestamp
    
    % Correct drift
    Img2.translate(-drift.dy(t_image), -drift.dx(t_image));
    
    % If eyes movement, perform a registration for further correction
    if regist
        % Image skeleton
        Gfx = imgaussfilt(Img2.pix, sigma);
        Gfy = imgaussfilt(Img2.pix, fliplr(sigma));
        Ref_2 = abs(Gfx - Gfy);
        
        % Image registration
        regist_param = imregdemons(Ref_2, Ref_1, [50 5 1], 'PyramidLevels', 3, ...
            'AccumulatedFieldSmoothing', 2, 'DisplayWaitbar', false);
        Img2 = imwarp(Img2.pix, regist_param);
    else
        Img2 = Img2.pix;
    end
    
    Img_bin = imresize(Img2, 1/binsize);                % Bin the image
    Img_mean = Img_mean + Img_bin;                      % Add the frame to the mean
    sigstack(:, t_image) = Img_bin(pixel_list);         % Store raw fluo signal
end
fprintf(']\n');

% Output
% -------------------------------------------------------------------------
signal_stack = sigstack;                    % Matrix intensity x time
index = pixel_list;                         % Linear indices of pixels within the brain mask
greystack = uint16(Img_mean/N_img_layer);   % Mean image
end