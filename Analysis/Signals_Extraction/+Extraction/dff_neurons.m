function [dff, baseline] = dff_neurons(F)
% This function computes the baseline and the DF/F of neurons get by focus
% F.
% INPUT
% F : Focus object, focused on a layer.
%
% OUTPUT
% dff : DF/F of each neurons
% baseline : baseline of each neurons
%

% Fill default values
% -------------------------------------------------------------------------
window = 80;        % moving window in seconds
percen = 8;         % nth percentile

% Complete parameters
% -------------------------------------------------------------------------
dt = F.dt*1e-3;
Nlayer = length(F.sets);
dt_brain = dt*Nlayer;

% Prepare filenames
% -------------------------------------------------------------------------
Neurons = F.matfile('IP/@Neurons');           % raw fluo
Background = F.matfile('IP/@Background');     % Background

% Load files
% -------------------------------------------------------------------------
fluo = Neurons.load('fluo');
fluo = fluo.fluo;

bg = Background.load('mean_first');
bg = bg.mean_first;         % background properly computed
fluo = fluo - bg;           % remove background

% Compute baseline and DF/F
% -------------------------------------------------------------------------
% Compute baseline, computeBaseline(signal, percentile, window, use
% parallel loop)
baseline = computeBaseline(fluo, percen, round(window/dt_brain), false);
baseline(baseline == 0) = 1;        % remove 0s
dff = (fluo - baseline)./baseline;

end