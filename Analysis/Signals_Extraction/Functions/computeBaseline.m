function baseline = computeBaseline(signal, q, window, fast)
% This function computes the baseline of the input signal by computing the
% qth quantile of the signal in a moving window. Uses ordfilt2 to simulate
% the moving window. 20% faster than with quantile, with uint16 array.
%
% INPUTS :
% ------
% signal : signal matrix, time series on rows (size is n_pixel x n_times)
% q : quantile (typically 8 to get the 8th percentile).
% window : size of the moving time window in frames
% fast : choose to use a parallel loop or not.
%
% OUPUTS :
% ------
% baseline : matrix with the same size of signal, containing baseline value
% through time (columns) for each pixel (rows).
%
% 2018-02-28 Update - Now uses ordfilt2 function that is 10 to 20% faster

domain = ones(window, 1);
stepsize = round(size(signal, 1)/8) - 1;

if fast
    
    ranges = 0:stepsize:size(signal, 1);    % Split the signal in several arrays
    bl = cell(length(ranges), 1);
    
    parfor idx = 1:length(ranges)
        n = ranges(idx);
        range = n + 1:min((n + stepsize), size(signal, 1));       % define range
        bl_tmp = ordfilt2(signal(range, :)', q, domain, 'symmetric');
        bl{idx} = bl_tmp';
    end
    
    baseline = cat(1, bl{:});
    
else
    
    baseline = ordfilt2(signal', q, domain, 'symmetric');
    baseline = baseline';
    
end