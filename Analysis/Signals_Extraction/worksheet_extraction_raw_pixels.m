% 2018-01-04 - Worksheet to use the function extractRAW_pixels that
% extracts raw fluo data from set of images, with drift correction.

clear
close all
clc

% Parameters
study = 'Thermotaxis';      % Focus will look for the run in study/dat
dat = '2018-01-10';
runs = [9 10];

% Options
binsize = 1;        % Bin signals
regist = 0;         % Register image for further correction (if eyes free)

checkdate = datevec(dat);   % Gather year, month and day

for run = runs
    
    % Get Focus
    if checkdate(2) < 9 && checkdate(1) < 2018
        F = getFocus([dat ' ' study], run);
    else
        F = getFocus([study filesep dat], run);
    end
    
    n_layers = numel(F.sets);
    
    fprintf('%s : Extracting raw fluo\n', F.name);
    ML.CW.line;
    
    timer = 0;
    for layer = 1:n_layers
        
        tic
        fprintf('Layer #%i...', layer);
        
        % Select current layer
        F.select(layer);
        
        % Extrack fluo from pixels
        [signal_stack, index, greystack] = Extraction.raw_pixels(F, binsize, regist);
        
        % Prepare filenames
        sig_path = [F.Data 'signal_stacks' filesep sprintf('%i', layer) filesep];
        gre_path = [F.Data 'grey_stack' filesep];
        
        if ~exist(sig_path, 'dir')
            mkdir(sig_path);
        end
        if ~exist(gre_path, 'dir')
            mkdir(gre_path);
        end
        
        % Save signal matrix and mean image
        save([sig_path 'sig.mat'], 'signal_stack', 'index');
        imwrite(greystack, [gre_path 'Image_' sprintf('%02i', layer) '.tif']);
        
        timer = timer + toc;
        fprintf('Done (%2.2fs).\n', toc);
    end
    
    fprintf('All done in %2.2fs.\n', timer);
    
end