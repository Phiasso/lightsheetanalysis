% 2017-12-15 Worksheet to compute DFFcorcor on neurons from the Routines.
% Typically to run after a Routines ends to compute the new DFF for each
% neurons and save it as DFFprime. Replaces Wolf's 'real_signals' (~1000x
% slower).
% 2018-07-03 - New DFF is now the same (8th quantile) - use this function
% only to compute differently the DF/F

% Parameters
study = 'Thermotaxis';
dat = '2017-07-20';
runs = 5:10;

checkdate = datevec(dat);

for run = runs
    
    % Get Focus
    if checkdate(2) < 9 && checkdate(1) < 2018
        F = getFocus([dat ' ' study], run);
    else
        F = getFocus([study filesep dat], run);
    end
    
    n_layers = numel(F.sets);
    
    fprintf('%s : Computing new baseline and DFF\n', F.name);
    ML.CW.line;
    
    timer = 0;
    for layer = 1:n_layers
        
        tic
        fprintf('Layer #%i...', layer);
        
        % Select current layer
        F.select(layer);
        
        % Compute the new baseline and DFFcorcor
        [DFFcorcor, baseline2] = Extraction.dff_neurons(F);
        
        % Prepare filenames
        DFFprime = F.matfile('@DFFprime');
        Neurons = F.matfile('IP/@Neurons');
        
        % Save/add new DFF/baseline
        
        DFFprime.save(DFFcorcor, 'DFFcorcor');
        Neurons.save(baseline2, 'baseline2');
        
        
        timer = timer + toc;
        fprintf('Done (%2.2fs).\n', toc);
    end
    
    fprintf('All done in %2.2fs.\n', timer);
    
end