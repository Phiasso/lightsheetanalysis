function createBlackStack(F, ntimes, directory)
% This function creates a 'black stack', a series of stack made from mean
% image from an experiment. Useful to overlay difference on it.

% Input :
% -------
% F : Focus object.
% ntimes : number of series to create.
% directory : folder with images will be located in F.Data / direcory
%
% Output :
% --------
% Create a folder 'black_stack' under F.Data/directory with series of mean stack from
% 'grey_stack'.

input_new = [F.Data 'grey_stack_glg' filesep];
input_old = [F.Data 'grey_stack' filesep];

output = [F.Data directory filesep 'black_stack' filesep];
n_layers = length(F.sets);

if ~exist(output, 'dir')
    mkdir(output);
end

for it = 1:ntimes
    for layer = 1:n_layers
        try
            copyfile([input_new 'Image_' num2str(layer, '%02i') '.tif'], ...
                [output 'Image_' num2str(layer + (it - 1)*n_layers, '%02i') '.tif']);
        catch
            copyfile([input_old 'Image_' num2str(layer, '%02i') '.tif'], ...
                [output 'Image_' num2str(layer + (it - 1)*n_layers, '%02i') '.tif']);
        end
    end
end
end