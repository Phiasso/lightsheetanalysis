function [peristim_time, peristim_sig] = periStimMean(signal, stim, time_before, time_after, F)
% This function averages the signal around specified events. The signal is
% oversampled on a time basis common to all layers.
%
% INPUTS :
% ------
% signal : signal to average. Ntimes x Npixels : time series are on
% columns.
% stim : stimulation onsets in seconds, signal is averaged around the time points in
% this vector. 
% time_before : defines the low limit of the averaging window around stimulus, in seconds.
% time_after : defines the high limit of the averaging window around stimulus, in seconds.
% F : Focus object corresponding to the experiment, focused on the current
% layer. Contains additional parameters (dt, number of layers...).
%
% OUTPUTS : 
% -------
% peristim_time : time vector corresponding to the average, 0 is the onset
% of the stimuli.
% peristim_sig : averaged signal around stimulus.
%
% 2018-01-22 GLG

% Complete parameters
% -------------------
n_layers = length(F.sets);
dt = F.dt*1e-3;
dt_brain = (dt*n_layers);
Ntimes = numel(F.set.frames);

% Check input size
% ----------------
if size(signal, 1) ~= Ntimes
    error("'signal' size doesn't match Focus data. 'signal' size should be Nframes x Npixels.");
end
if size(stim, 2) == 1 && numel(stim) ~= 1
    stim = stim';
end

% Convert window in frames
% ------------------------
time_before_im = round(time_before/dt);
time_after_im = round(time_after/dt);

% Define time vectors
% -------------------
query_time_full = linspace(0, Ntimes*dt_brain, Ntimes*n_layers);
time_layer_full = F.set.t*(1e-3);

% Loop over stimulus onsets
% -------------------------
peristim_sig = 0;
for stim_onset = stim
    % Find closest available time
    [~, idx_closest_time] = min(abs(query_time_full - stim_onset));
    % Define time points to interpolate the signal
    time_points = (idx_closest_time - time_before_im):(idx_closest_time + time_after_im);
    query_time = query_time_full(time_points);
    % Oversample the signal
    interp_signal = interp1(time_layer_full, signal, query_time);
    % Add the new values
    peristim_sig = peristim_sig + interp_signal;
end

peristim_sig = peristim_sig/numel(stim);    % compute the mean signal around stimulus
peristim_time = -time_before:dt:time_after; % corresponding time vector, 0 is the real onset
end