% 2018-02-28 This code does the same thing as worksheet_mean_response_dff
% but takes the median through stimulations rather than the mean.

clear;
close all
clc;

% Parameters
% -------------------------------------------------------------------------
% Experiment
% ----------
study = 'Thermotaxis';
dat = '2018-01-31';
runs = [6 7];

% Options
% -------
% Averaging window
time_before = 0;            % time before stim (s)
time_after = 10;            % time after stim (s)
layer_motion = 5;           % choose this layer for motion
motion_thresh = 7;        % ignore stimulation if motion occured
dff_thresh = 5;             % consider DF/F > dff_thresh*std(dff) as artifacts and remove them

checkdate = datevec(dat);

% Loop over runs
% -------------------------------------------------------------------------

for run = runs
    
    % Get focus
    if checkdate(2) < 9 && checkdate(1) < 2018
        F = getFocus([dat ' ' study], run);
    else
        F = getFocus([study filesep dat], run);
    end
    
    MeanResponse.dffMedian(F, time_before, time_after, layer_motion, motion_thresh, dff_thresh)
    
end