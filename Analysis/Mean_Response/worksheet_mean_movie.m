% 2018-01-18 New version to create a mean movie around stimulations. Uses
% raw images from the experiment, which are drift-corrected. Stimulations
% with too much motions are discarded.

clear;
close all
clc;

% Parameters
% -------------------------------------------------------------------------
% Experiment
% ----------
study = 'Thermotaxis';
dat = '2017-11-16';
runs = [5 6 7 8 9];

% Options
% -------
% Averaging window
time_before = 1;        % time before stim (s)
time_after = 8;         % time after stim (s)
motion_thresh = 8;      % ignore stimulation if motion occured
binsize = 1;

checkdate = datevec(dat);

% Loop over runs
% -------------------------------------------------------------------------

for run = runs
    
    % Get focus
    if checkdate(2) < 9 && checkdate(1) < 2018
        F = getFocus([dat ' ' study], run);
    else
        F = getFocus([study filesep dat], run);
    end
    
    MeanResponse.movie(F, time_before, time_after, motion_thresh, binsize);
    
end