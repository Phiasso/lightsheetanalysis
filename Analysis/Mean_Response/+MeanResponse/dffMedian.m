function [] = dffMedian(F, time_before, time_after, layer_motion, motion_thresh, dff_thresh)
% Compute the mean response, defined as the median of the DF/F of each pixels
% around stimulations. TIME_BEFORE and TIME_AFTER defines the averaging
% window. Stimulations with too much motion are discarded. DF/F > dff_thresh
% will be set to 0. Responses are saved as images.

useGpu = 'n';
integrate_after = time_after;       % compute mean response until integrate_after seconds

% Define signals and grey stack directories relative to F.Data
sig_dir = 'signal_stacks_glg';
gre_dir = 'grey_stack_glg';

% Define output
output_path = ['Mean_Response' filesep 'dff_median' filesep];
pos_name = 'median_dff_Pos';
neg_name = 'median_dff_Neg';
sig_name = 'median_sig';

% Complete parameters
dt = F.dt*1e-3;
n_layers = length(F.sets);
dt_brain = dt*n_layers;
base_dir_sig = [F.Data sig_dir filesep];
n_times = length(F.set.t);
time_full = linspace(0, n_times*dt_brain, n_times*n_layers);

% Load stimulus onsets
stim = getStimTimes(F.Data);
last_stim_time = stim(end);
last_time = last_stim_time + time_after + 1;
last_frame = round(last_time./dt_brain);    % limit DF/F to this time
time_full = time_full(1:last_frame*n_layers);

% Convert times in frames
stim_im = floor(stim/dt_brain);
time_after_im = floor(time_after/dt_brain);     % to discard after a motion

% Functions to load files
load_dff = @(n) load([base_dir_sig num2str(n) filesep 'dff.mat']);

% Create output structures and directories;
%peristim_dff = struct;  % will contain mean dff around stimulus
%R = struct;             % will contain mean response integrated after stimulus

% Create directories
mkdir([F.Data output_path]);
mkdir([F.Data output_path pos_name]);
mkdir([F.Data output_path neg_name]);
mkdir([F.Data output_path sig_name]);

timer = 0;      % initialise timer

fprintf('%s : Computing mean response map\n', F.name);
ML.CW.line;

% Discard stimulus with motions
% -----------------------------
fprintf('Discarding stimulus with motions ');
F.select(layer_motion);
Drifts = load(F.fname('IP/@Drifts'));       % Load drifts
drift = [Drifts.dx(1:last_frame)' Drifts.dy(1:last_frame)'];
motion_times = extract_motion_times_glg(drift, motion_thresh, time_after_im, 'sym');
stim_wm = stim;
stim_wm(ismember(stim_im, motion_times)) = [];  % stim onsets without motion
n_stim_wm = length(stim_wm);

if numel(stim_wm) < 0.5*numel(stim)
    error('Not enough stimulation remaining.');
end
fprintf('(%i/%i remaining).\n', numel(stim_wm), numel(stim));

% Loop over layers
% ---------------------------------------------------------------------

for layer = 1:n_layers
    
    F.select(layer);
    
    fprintf('Layer #%i...\n', layer);
    
    % Average DF/F around stimulus
    % -----------------------------------------------------------------
    
    % Load DF/F and index
    % -------------------
    tic;
    fprintf('Loading DF/F... ');
    tmp = load_dff(layer);
    index = tmp.dff.index;
    dff = tmp.dff.signal_stack(:, 1:last_frame);
    n_pixels = length(index);
    
    % Free RAM
    clear tmp
    
    timer = timer + toc;
    fprintf('Done (%2.2fs).\n', toc);
    
    % Compute median DF/F around stimulus
    % -----------------------------------
    tic
    fprintf('Interpolating DF/F around stimulus... ');
    
    time = -time_before:dt:time_after; % time vector around stimulations
    n_times_peristim = length(time);
    all_stim_dff = cell(n_stim_wm, 1);     % initialise cell containing peristim DF/F
    time_layer = (F.set.t)*1e-3;        % time vector (seconds)
    time_layer = time_layer(1:last_frame);  % limit to stimulation period
    
    % Get peristim DF/F for each stimulation
    for idx_stim = 1:n_stim_wm
        % Find the closest time point to stimulation onset
        [~, time_stim] = min(abs(time_full - stim_wm(idx_stim)));
        % Select time points to interpolate
        query_time = time_full(time_stim-round(time_before/dt):time_stim+floor(time_after/dt));
        % Interpolate
        switch useGpu
            case 'y'
                dff = gpuArray(dff);
        end
        interp_sig = interp1(time_layer, dff', query_time);
        interp_sig = gather(interp_sig);
        all_stim_dff{idx_stim} = interp_sig';
    end
    
    % Free RAM
    clear dff
    
    timer = timer + toc;
    fprintf('Done (%2.2fs).\nComputing median... ', toc);
    
    % Gather time series of all pixels and compute median
    all_stim_dff = cell2mat(all_stim_dff);
    all_stim_dff = reshape(all_stim_dff', n_times_peristim, n_pixels, n_stim_wm);
    median_dff = median(all_stim_dff, 3);  % compute median value
    median_dff = median_dff';
    
    % Free RAM
    clear all_stim_dff
    
    %         peristim_dff.median_dff = median_dff;
    %         peristim_dff.median_time = time;
    %         save([base_dir_sig num2str(layer) filesep 'median_peristim_dff.mat'], 'peristim_dff');
    
    timer = timer + toc;
    fprintf('Done (%2.2fs).\n', toc);
    
    % Compute mean response after a stimulation
    % -----------------------------------------------------------------
    tic;
    fprintf('Computing mean response after stimulus... ');
    [~, onset] = min(abs(time));      % find closest time to stimuli onset
    offset = onset + fix(integrate_after/(mean(diff(time))));  % time after
    % Mean response ('integral') with first value removed
    dff_median_response = mean(median_dff(:, onset:offset) - median_dff(:, onset), 2);
    %R.dff_median_response = dff_median_response;
    %R.index = index;
    %save([base_dir_sig num2str(layer) filesep 'R_median_dff.mat'], 'R');
    fprintf('Done (%2.2fs).\n', toc);
    timer = timer + toc;
    
    % Save the response as an image
    % -----------------------------------------------------------------
    tic;
    fprintf('Converting response into image... ');
    mean_img = imread([F.Data gre_dir filesep 'Image_' sprintf('%02i', layer) '.tif']);
    img_neg = zeros(size(mean_img));    % canvas for negative response
    img_pos = zeros(size(mean_img));    % canvas for positive response
    
    % Positive response
    % -----------------
    R_pos = dff_median_response;
    R_pos(R_pos<0) = 0;
    R_pos(R_pos > dff_thresh) = 0;      % remove artifacts
    img_pos(index) = R_pos;
    
    % Negative response
    % -----------------
    R_neg = dff_median_response;
    R_neg(R_neg > 0) = 0;
    R_neg = - R_neg;
    R_neg(R_neg > dff_thresh) = 0;      % remove artifacts
    img_neg(index) = R_neg;
    
    % Convert to uint16
    % -----------------
    img_pos = uint16(100*img_pos);
    img_neg = uint16(100.*img_neg);
    
    % Save images
    % -----------
    imwrite(img_pos, [F.Data output_path pos_name filesep 'Image_' sprintf('%02i', layer) '.tif']);
    imwrite(img_neg, [F.Data output_path neg_name filesep 'Image_' sprintf('%02i', layer) '.tif']);
    imwrite(mean_img, [F.Data output_path sig_name filesep 'Image_' sprintf('%02i', layer) '.tif']);
    
    fprintf('Done (%2.2fs).\n', toc);
    timer = timer +  toc;
    
end