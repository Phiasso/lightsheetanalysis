function [] = dffMean(F, time_before, time_after, integrate_after, layer_motion, motion_thresh, dff_thresh)
% Compute the mean response, defined as the mean of the DF/F of each pixels
% around stimulations. TIME_BEFORE and TIME_AFTER defines the averaging
% window. Stimulations with too much motion are discarded. DF/F > dff_thresh
% will be set to 0. Responses are saved as images within the folder focused on.

% Complete parameters
useGpu = 'n';

% Define signals and grey stack directories relative to F.Data
sig_dir = 'signal_stacks';	% matfile with time series per pixels
gre_dir = 'grey_stack';		% mean images for each layer

% Define output
output_path = ['Mean_Response' filesep 'dff' filesep];
pos_name = 'mean_dff_Pos';
neg_name = 'mean_dff_Neg';
sig_name = 'mean_sig';

dt = F.dt*1e-3;
n_layers = length(F.sets);
dt_brain = dt*n_layers;
base_dir_sig = [F.Data sig_dir filesep];

% Load stimulus onsets
stim = getStimTimes(F.Data);

% Convert times in frames
stim_im = floor(stim/dt_brain);
time_after_im = floor(time_after/dt_brain);     % to discard after a motion

% Functions to load files
load_dff = @(n) load([base_dir_sig num2str(n) filesep 'dff.mat']);

% Create output structures and directories;
peristim_dff = struct;  % will contain mean dff around stimulus
R = struct;             % will contain mean response integrated after stimulus

% Create directories
mkdir([F.Data output_path]);
mkdir([F.Data output_path pos_name]);
mkdir([F.Data output_path neg_name]);
mkdir([F.Data output_path sig_name]);

timer = 0;      % initialise timer

fprintf('%s : Computing mean response map\n', F.name);
ML.CW.line;

% Discard stimulus with motions
% -----------------------------
fprintf('Discarding stimulus with motions ');
F.select(layer_motion);
Drifts = load(F.fname('IP/@Drifts'));       % Load drifts
drift = [Drifts.dx' Drifts.dy'];
motion_times = extract_motion_times_glg(drift, motion_thresh, time_after_im, 'sym');
stim_wm = stim;
stim_wm(ismember(stim_im, motion_times)) = [];  % stim onsets without motion

if numel(stim_wm) < 0.5*numel(stim)
    error('Not enough stimulation remaining.');
end
fprintf('(%i/%i remaining).\n', numel(stim_wm), numel(stim));

% Loop over layers
% ---------------------------------------------------------------------

for layer = 1:n_layers
    
    F.select(layer);
    
    fprintf('Layer #%i...\n', layer);
    
    % Average DF/F around stimulus
    % -----------------------------------------------------------------
    
    % Load DF/F and index
    % -------------------
    tic;
    fprintf('Loading DF/F... ');
    tmp = load_dff(layer);
    index = tmp.dff.index;
    dff = tmp.dff.signal_stack;
    timer = timer + toc;
    fprintf('Done (%2.2fs).\n', toc);
    
    % Compute mean DF/F around stimulus
    % ---------------------------------
    tic
    switch useGpu
        case 'y'
            dff = gpuArray(dff);
    end
    fprintf('Computing mean DF/F around stimulus... ');
    [time, mean_dff] = periStimMean(dff', stim_wm, time_before, time_after, F);
    mean_dff = gather(mean_dff');
    peristim_dff.mean_dff = mean_dff;
    peristim_dff.mean_time = time;
    save([base_dir_sig num2str(layer) filesep 'mean_peristim_dff.mat'], 'peristim_dff');
    timer = timer + toc;
    fprintf('Done (%2.2fs).\n', toc);
    
    % Compute mean response after a stimulation
    % -----------------------------------------------------------------
    tic;
    fprintf('Computing mean response after stimulus... ');
    [~, onset] = min(abs(time));      % find closest time to stimuli onset
    offset = onset + fix(integrate_after/(mean(diff(time))));  % time after
    % Mean response ('integral') with first value removed
    dff_mean_response = mean(mean_dff(:, onset:offset) - mean_dff(:, onset), 2);
    R.dff_mean_response = dff_mean_response;
    R.index = index;
    save([base_dir_sig num2str(layer) filesep 'R_dff.mat'], 'R');
    fprintf('Done (%2.2fs).\n', toc);
    timer = timer + toc;
    
    % Save the response as an image
    % -----------------------------------------------------------------
    tic;
    fprintf('Converting response into image... ');
    mean_img = imread([F.Data gre_dir filesep 'Image_' sprintf('%02i', layer) '.tif']);
    img_neg = zeros(size(mean_img));    % canvas for negative response
    img_pos = zeros(size(mean_img));    % canvas for positive response
    
    % Positive response
    % -----------------
    R_pos = R.dff_mean_response;
    R_pos(R_pos<0) = 0;
    R_pos(R_pos > dff_thresh) = 0;      % remove artifacts
    img_pos(R.index) = R_pos;
    
    % Negative response
    % -----------------
    R_neg = R.dff_mean_response;
    R_neg(R_neg > 0) = 0;
    R_neg = - R_neg;
    R_neg(R_neg > dff_thresh) = 0;      % remove artifacts
    img_neg(R.index) = R_neg;
    
    % Convert to uint16
    % -----------------
    img_pos = uint16(100*img_pos);
    img_neg = uint16(100*img_neg);
    
    % Save images
    % -----------
    imwrite(img_pos, [F.Data output_path pos_name filesep 'Image_' sprintf('%02i', layer) '.tif']);
    imwrite(img_neg, [F.Data output_path neg_name filesep 'Image_' sprintf('%02i', layer) '.tif']);
    imwrite(mean_img, [F.Data output_path sig_name filesep 'Image_' sprintf('%02i', layer) '.tif']);
    
    fprintf('Done (%2.2fs).\n', toc);
    timer = timer +  toc;
    
end
end