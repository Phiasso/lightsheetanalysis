% 2018-05-31 Deconvolve neurons calcium signal to get spikes (example of utilisation).

close all
clear
clc

study = 'Thermotaxis';
experiment_list = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/CY44_list.txt';

% GCaMP6f time constants
% tauRise = 0.15;
% tauDecay = 1.6;

% CY44 time constants
tauRise = 0.6;
tauDecay = 4;

list = readtable(experiment_list);
n_exp = size(list, 1);

fprintf('Infering spikes...\n');
fprintf('[');
tic

for idx_date = 1:n_exp
    
    dat = char(list.Date(idx_date));
    runs = eval([ '[' list.RunNumber{idx_date} ']']);
    
    for idx_run = 1:numel(runs)
        
        fprintf('#');
        
        run = runs(idx_run);
        
        F = getFocus([study filesep dat], run);
        
        n_layers = length(F.sets);
        dt = F.dt*1e-3;
        
        for layer = 1:n_layers
            
            F.select(layer);
            
            % Load signal
            signal = load(F.fname('@DFF'), 'neurons');
            signal = double(signal.neurons');
            
            % Inference algorithm parameters
            Oalg = struct;                      % Struct of experimental conditions & decoding options.
            Oalg.Time = size(signal, 1);        % Number of time frames.
            Oalg.dt = n_layers*dt;              % interval duration. (s)
            Oalg.nNeurons = size(signal, 2);    % Number of neurons.
            Oalg.adaptive = 0;                  % Not adaptive. Will use provided values for parameters, and estimate the unknown ones.
            
            Palg = struct;
            Palg.tauRise = tauRise;             % Fluorescence raise time (s)
            Palg.tauDecay = tauDecay;           % Fluorescence decay time (s)
            
            [spikes, ~, ~, Pphys] = pBSD(signal, Oalg, Palg);   % Blind sparse deconvolution from Tubiana
            
            % Binarize spikes
%             spikes_bin = NaN(Oalg.Time, Oalg.nNeurons);
%             th = Pphys.threshold;
%             parfor n = 1:Oalg.nNeurons
%                 spikes_bin(:, n) = BSD_binarize(spikes(:, n), th(n), 1, dt*n_layers); % Binarize spikes
%             end

            spikes_bin = spikes > Pphys.threshold;

            % Get back to usual order
            spikes = spikes';
            spikes_bin = spikes_bin';
            
            % Save
            save(F.fname('@Spikes'), 'spikes', 'spikes_bin');
            
        end
    end
end
fprintf(']\n');
fprintf('All done (%2.2fs).\n', toc);