% 2018-06-14 - HBO.display Plot HBO activity from the given experiment.

clear
clc

root = '/home/ljp/Science/Projects/Neurofish/Data/';

% Experiment
% ----------
study = 'Thermotaxis';
date = '2018-01-16';
run = 4;
% To define stimulation file
Tset = 15;

% Define filenames
% ----------------
calib_filename = 'calib';

% Options
% -------
discard_motions = 'n';
motion_thresh = 4.5;
time_after_motion = 3;

t_before = 0;   % For mean response (seconds)
t_after = 9;

% Define paths
% ------------
hbo_dir = [root study filesep 'HBO' filesep];

% Get Focus and parameters
F = getFocus([study filesep date], run);
n_layers = length(F.sets);
dt_brain = n_layers.*F.dt.*1e-3;

% Load data
% ---------
% Stimulation
onsets = getStimTimes(F.Data);
stim = load([root study filesep date filesep 'Calibration' filesep calib_filename '_Tset=' num2str(Tset)], 'trace');
stim = stim.trace;
stim.temperature = stim.temperature(1:round(onsets(end)./mean(diff(stim.time))) + 80);
stim.time = stim.time(1:round(onsets(end)./mean(diff(stim.time))) + 80);


% HbO DF/F
hbo_filename = [hbo_dir date '_Run' sprintf('%02i', run) filesep 'Matfiles' filesep 'hbo_dff.mat'];
hbo_dff = load(hbo_filename, 'hbo_dff');
hbo_dff = hbo_dff.hbo_dff;

% Gather left/right
HbO_right = mean(vertcat(hbo_dff{:, [2 3 4]}));
HbO_left = mean(vertcat(hbo_dff{:, [1 5 6]}));

% Oversample stimulation trace to match DF/F sampling
time = linspace(0, numel(HbO_right).*dt_brain, numel(HbO_right));
T = interp1(stim.time, stim.temperature, time);
% Replace outside values by mean of the end
T_baseline = T;
T_baseline(isnan(T)) = [];
% Fill values outside the stimulation period with a
% baseline
T(isnan(T)) = mean(T_baseline(end-10:end));

switch discard_motions
    case 'y'
        motion_times = getMotionTimes(F, 5, motion_thresh, time_after_motion, 'uni');
        HbO_left(motion_times) = 0;
        HbO_right(motion_times) = 0;
end

% Compute derivative
% ------------------
dT = [0, diff(T)./diff(time)];
rise_region = find(dT > 0);
decay_region = find(dT < 0);

delta = zeros(size(dT));
delta(rise_region) = 1;
delta(decay_region) = -1;

% Plot
% ----
% Get colors
f = figure;
ax = gca;
c1 = ax.ColorOrder(1, :);
c2 = ax.ColorOrder(2, :);
close(f);

fig = figure;
left_color = c1;
right_color = [0 0 0];
set(fig,'defaultAxesColorOrder',[left_color; right_color]);

hold on;

% yyaxis right
% p1 = plot(time, T, 'k', 'LineWidth', 1.25);
% p1.Color(4) = 0.25;
% yyaxis left
plot(time, HbO_right, '-', 'Color', c1, 'LineWidth', 1.5);
% yyaxis left
plot(time, HbO_left, '-', 'Color', c2, 'LineWidth', 1.5);

infe = min([HbO_left HbO_right]);
supe = max([HbO_left HbO_right]);

for n = 1:length(dT)
    
    if delta(n) == 1 %&& delta(n - 1) ~= -1
        
        coordX = [time(n) time(n) time(n + 1) time(n + 1)];
        coordY = [infe supe supe infe];
        p = patch(coordX, coordY, [1 0 0]);
        
    elseif delta(n) == -1 %&& delta(n - 1) ~= 1
        
        coordX = [time(n) time(n) time(n + 1) time(n + 1)];
        coordY = [infe supe supe infe];
        p = patch(coordX, coordY, [0 0 1]);
    end
    
    if exist('p', 'var')
        set(p,'FaceAlpha',0.2);
        set(p,'EdgeColor','none')
    end
end