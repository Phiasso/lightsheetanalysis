function [] = merge(study, dat, run)
% Fetch all HBO neurons indices stored in study/HBO/dat/run. Removes
% duplicates and saves them as a single matfile.
% Update 2018-03-14 : Converted to a function as HBO.merge.

% Parameters
% -------------------------------------------------------------------------
% Experiment
% ----------
root = '/home/ljp/Science/Projects/Neurofish/Data/';
% study = 'Thermotaxis';
% dat = '2017-11-15';
% run = 8;

dff_to_use = 'cor';     % DF/F to use, 'cor' (Candelier) or 'corcor' (8th quantile)

n_groups = 10;

% Define directories
% ------------------
checkdate = datevec(dat);
hbo_dir = [root study filesep 'HBO' filesep];
hbo_exp = [hbo_dir dat '_Run' sprintf('%02i', run) filesep];
hbo_idx = [hbo_exp 'Index' filesep];

% Output
% ------
output_matfiles = [hbo_exp filesep 'Matfiles' filesep 'hbo_dff.mat'];

% Gather HBO index and DF/F
% -------------------------------------------------------------------------
% Get Focus
% ---------
if checkdate(2) < 9 && checkdate(1) < 2018
    F = getFocus([study '_Old' filesep dat ' ' study], run);
else
    F = getFocus([study filesep dat], run);
end

fprintf('%s - HBO merger\n', F.name);
ML.CW.line;

% Complete parameters
% -------------------
n_layers = length(F.sets);

% Get HBO index and corresponding DF/F
% ------------------------------------
list_index = dir([hbo_idx '*.mat']);       % list matfiles in directory
n_files = length(list_index);

if n_files == 0
    error('No index files found.');
end

if ~exist([hbo_exp filesep 'Matfiles' filesep], 'dir')
    mkdir([hbo_exp filesep 'Matfiles' filesep]);
end

make_index_filename = @(n) [list_index(n).folder filesep list_index(n).name];

hbo_index = cell(n_layers, n_groups);
hbo_dff = cell(n_layers, n_groups);

fprintf('Loading HBO index and DF/F... ');
tic;
for idx_file = 1:n_files
    
    filename = make_index_filename(idx_file);
    indices = load(filename);
    
    % HBO index
    old = hbo_index{indices.layer, indices.group_number};       % already stored
    new = indices.index_neurons_group;                          % new indices
    new(ismember(new, old)) = [];                               % remove duplicates
    hbo_index{indices.layer, indices.group_number} = cat(2, old, new);
    
    % HBO DF/F
    F.select(indices.layer);
    switch dff_to_use
        case 'cor'
            DFF = F.matfile('@DFF');
            DFF = DFF.load('neurons');
            DFF = DFF.neurons;
        case 'corcor'
            DFF = F.matfile('@DFFprime');
            DFF = DFF.load('DFFcorcor');     % DFFcorcor
            DFF = DFF.DFFcorcor;
        otherwise
            fprintf('DFF to use not recognised, choose ''cor'' or ''corcor''\n');
    end
    
    hbo_dff{indices.layer, indices.group_number} = cat(1, hbo_dff{indices.layer, indices.group_number}, DFF(indices.index_neurons_group, :));
    
end
save(output_matfiles, 'hbo_index', 'hbo_dff', 'dff_to_use');

fprintf('Done and saved (%2.2fs).\n', toc);