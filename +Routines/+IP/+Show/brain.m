function brain(varargin)
%Routines.IP.Show.brain Display the brain contour
%   ROUTINES.IP.SHOW.BRAIN(F) show the brain contour for the current set of 
%   the F Focus object.
%
%   ROUTINES.IP.SHOW.BRAIN(..., 'set', SET) uses a specific SET of the F
%   Focus object.
%
%   ROUTINES.IP.SHOW.BRAIN(..., 'img', IMG) display Image object IMG 
%   instead of the mean image associated to the Focus object.
%
%   ROUTINES.IP.SHOW.BRAIN(..., 'pos', POS) uses the position array
%   POS [x,y] for displaying the contour.
%
%   See also: Routines.IP, Routines.IP.brain.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addParamValue('F', NaN, @(x) isa(x,'Focus'));
in.addParamValue('set', NaN, @isnumeric);
in.addParamValue('img', NaN, @(x) isa(x, 'Image'));
in.addParamValue('pos', [], @isnumeric);
in = +in;

% === Parameters ==========================================================

btag = 'IP/@Brain';

% =========================================================================

% --- Get Focus object
if ~isa(in.F, 'Focus')
    tmp = ML.WS.get_by_class('Focus');
    if ~isempty(tmp)
        in.F = evalin('base', tmp{1});
    else
        in.F = getFocus;
    end
end

% --- Focus set
if ~isnan(in.set)
    in.F.select(in.set);
end

% --- Load image
if isnumeric(in.img)
    in.img = in.F.iload(1);
    in.img.rm_infos();
end

% --- Load contour
if isempty(in.pos)
    Bmat = in.F.matfile(btag);
    tmp = Bmat.load('contours');
    in.pos = tmp.contours;
end

% --- Display
Fig = ML.Figures;
Fig.clear('IP');

in.img.show();
plot(in.pos(:,1), in.pos(:,2), 'm+-');

% Adjustments
in.F.title('Brain contour');