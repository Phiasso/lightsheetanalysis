function [] = neurons(F, plist)
%Routines.IP.Show.neurons Show neuronal segmentation
%   Completely rewritten on 2018-04-17 by GLG. Now just shows neuronal
%   segmentation.

% === Inputs ==============================================================

% === Parameters ==========================================================

mtag = 'IP/@Mean';

% =========================================================================

% --- Preparation
Fig = ML.Figures;
Fig.select('IP');

% --- Display
Fig.clear('IP');

% Mean image
Mean = Image(F.fname(mtag, 'png'));

Img = Mean.pix;
Resc = (Img-min(Img(:)))/(max(Img(:))-min(Img(:)));

Grid = ones(size(Img))*0.8;

for i = 1:numel(plist)
    Grid(plist{i}) = 1;
end

CD = cat(3, Resc, Resc.*Grid, Resc);
CD = Image(CD);
CD.show;

caxis(F.IP.range)

% --- Adjustment
F.title('Neurons');