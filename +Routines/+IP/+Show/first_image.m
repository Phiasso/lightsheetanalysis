function first_image(F, varargin)
%Routines.IP.Show.first_image Display the first image of a set
%   ROUTINES.IP.SHOW.FIRST_IMAGE(F) show the first image of the current set
%   of the Focus object F.
%
%   ROUTINES.IP.SHOW.FIRST_IMAGE(F, 'set', SET) show the first image of the
%   set SET of the Focus object F.
%
%   See also: Routines.IP.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addParamValue('set', NaN, @isnumeric);
in = +in;

% =========================================================================

% --- Focus set
if ~isnan(in.set)
    F.select(in.set);
end

% --- Display

Fig = ML.Figures;
Fig.clear('IP');

Img = F.iload(1);
Img.show();

% Adjustments
F.title('First image');