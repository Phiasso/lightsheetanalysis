function neurohexs(F, varargin)
%Routines.IP.Show.neurohexs Display neuropils' contours
%   ROUTINES.IP.SHOW.NEUROHEXS(F) show the neuropils' contours for the
%   current set in Focus object F.
%
%   ROUTINES.IP.SHOW.NEUROHEXS(..., 'set', SET) uses a specific SET of the 
%   Focus object F.
%
%   See also: Routines.IP, Routines.IP.neurohexs.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addParamValue('set', NaN, @isnumeric);
in = +in;

% === Parameters ==========================================================

mtag = 'IP/@Mean';
NPtag = 'IP/@Neuropils';
NHtag = 'IP/@Neurohexs';


% =========================================================================

% --- Focus set
if ~isnan(in.set)
    F.select(in.set);
end

% --- Get neuropils and neurohex

NPmat = F.matfile(NPtag);
NHmat = F.matfile(NHtag);

neuropils = NPmat.load();
neurohexs = NHmat.load();

% --- Display

% Preparation
Fig = ML.Figures;
Fig.clear('IP');

% Mean image
Mean = Image(F.fname(mtag, 'png'));
Mean.show;

% Boundaries
for i = 1:neuropils.N
    
    plot(neuropils.contours{i}(:,1), neuropils.contours{i}(:,2), 'c-');
    scatter(neurohexs.pos{i}(:,1), neurohexs.pos{i}(:,2), 20, 'm+');
    
    for j = 1:neurohexs.N{i}
        plot(neurohexs.contours{i}{j}(:,1), neurohexs.contours{i}{j}(:,2), 'b-');
    end
end

% Adjustments
F.title('Neurohexs');