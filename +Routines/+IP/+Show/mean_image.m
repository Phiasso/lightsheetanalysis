function mean_image(F, varargin)
%Routines.IP.Show.mean_image Display the mean image
%   ROUTINES.IP.SHOW.MEAN_IMAGE(F) show the mean image of the current set
%   of the Focus object F.
%
%   ROUTINES.IP.SHOW.HEAD(..., 'set', SET) uses a specific SET of the F
%   Focus object.
%
%   See also: Routines.IP, Routines.IP.mean_image

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addParamValue('set', NaN, @isnumeric);
in.addParamValue('img', NaN, @(x) isa(x, 'Image'));
in = +in;

% === Parameters ==========================================================

mtag = 'IP/@Mean';

% =========================================================================

% --- Focus set
if ~isnan(in.set)
    F.select(in.set);
end

% --- Load image
if isnumeric(in.img)
    in.img = Image(F.fname(mtag, 'png'));
end

% --- Display
Fig = ML.Figures;
Fig.clear('IP');

in.img.show();
caxis(F.IP.range)

% Adjustments
F.title('Averaged image');