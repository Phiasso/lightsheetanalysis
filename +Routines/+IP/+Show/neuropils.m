function neuropils(F, varargin)
%Routines.IP.Show.neuropils Display neuropils' contours
%   ROUTINES.IP.SHOW.NEUROPILS(F) show the neuropils' contours for the
%   current set in Focus object F.
%
%   ROUTINES.IP.SHOW.NEUROPILS(..., 'set', SET) uses a specific SET of the 
%   Focus object F.
%
%   ROUTINES.IP.SHOW.NEUROPILS(..., 'contours', C) uses the contours 
%   contained in the cell array of positions C {[x y]}.
%
%   See also: Routines.IP, Routines.IP.neuropils.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addParamValue('set', NaN, @isnumeric);
in.addParamValue('contours', NaN, @iscell);
in = +in;

% === Parameters ==========================================================

NPtag = 'IP/@Neuropils';
mtag = 'IP/@Mean';

% =========================================================================

% --- Focus set
if ~isnan(in.set)
    F.select(in.set);
end

% --- Get neuropils' contours
if ~iscell(in.contours) && isnan(in.contours)

    NPmat = F.matfile(NPtag);
    neuropils = NPmat.load();
    in.contours = neuropils.contours;
    
end

% --- Display

% Preparation
Fig = ML.Figures;
Fig.select('IP');
cla

% Mean image
Mean = Image(F.fname(mtag, 'png'));
Mean.show;

% Boundaries
for i = 1:numel(in.contours)
    plot(in.contours{i}(:,1), in.contours{i}(:,2), 'c-');
end

% Adjustments
F.title('Neuropils contours');