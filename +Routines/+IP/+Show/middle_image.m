function middle_image(F, varargin)
%Routines.IP.Show.middle_image Display the middle image of a set
%   ROUTINES.IP.SHOW.MIDDLE_IMAGE(F) show the middle image of the current 
%   set of the Focus object F.
%
%   ROUTINES.IP.SHOW.MIDDLE_IMAGE(F, 'set', SET) show the middle image of 
%   the set SET of the Focus object F.
%
%   See also: Routines.IP.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addParamValue('set', NaN, @isnumeric);
in = +in;

% =========================================================================

% --- Focus set
if ~isnan(in.set)
    F.select(in.set);
end

% --- Display

Fig = ML.Figures;
Fig.clear('IP');

n = round(numel(F.set.frames)/2);
Img = F.iload(n);
Img.show();

% Adjustments
F.title(['Middle image (' num2str(n) ')']);