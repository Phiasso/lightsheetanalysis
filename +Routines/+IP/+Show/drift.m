function drift(F)
%ROUTINES.IP.SHOW.DRIFT Display the drift through time
%*  ROUTINES.IP.SHOW.DRIFT() show the drift of the current layer, through
%   time.
%
%*  See also: Routines.IP.drift.

% === Parameters ==========================================================

dtag = 'IP/@Drift';
ttag = 'IP/@Times';

% =========================================================================

% --- Loads
D = F.load(dtag);
T = F.load(ttag);

% --- Display

clf
set(gcf, 'WindowStyle', 'docked');

plot3(D.x, D.y, T.t, 'k.-');

% Adjustments
F.title('Drift');
xlabel('x');
ylabel('y');
axis square
daspect([1 1 1e6]);
view(0, 90);
box on