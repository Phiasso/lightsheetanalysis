%Routines.IP.background Get the background level
%   ROUTINES.IP.BACKGROUND Get the background level of the current Focus
%   set and save it in the corresponding Files directory with the tag
%   'IP/@Background'.
%
%   Note: If no Focus object is defined in the workspace, a CLI is
%   triggered to let you choose.
%
%   Note: If a Steps object is defined in the workspace, it is
%   automatically updated.
%
%   See also: Routines.IP.

% === Parameters ==========================================================

btag = 'IP/@Background';

% =========================================================================

% --- Get Focus object
tmp = ML.WS.get_by_class('Focus');
if ~isempty(tmp)
    F = evalin('base', tmp{1});
else
    F = getFocus;
end

% --- Get Steps object
tmp = ML.WS.get_by_class('ML.Steps');
if ~isempty(tmp)
    steps_exist = true;
    S = evalin('base', tmp{1});
else
    steps_exist = false;
end

% --- Check file existence
M = F.matfile(btag);

if ~M.exist
    
    % Command window display
    T = ML.Time.Display();
    T.start('Computing Background');
    
    % --- Process
    Img = F.iload(1);
    
    % --- Get background noise
    [mean_first, std_first] = Img.background();
    
    % --- Output
    T.start('Saving Background');
    
    M.save(mean_first, 'Average noise level');
    M.save(std_first, 'Standard deviation of the noise level');
    
    fprintf('Background found : %2.2f\n', mean_first);
    
    T.stop
end

% --- Update Steps object
if steps_exist
    tmp = M.load;
    S.set_status(S.elms{F.set.id}, 'Background', ...
        [num2str(tmp.mean_first, '%.2f') ' ' char(177) num2str(tmp.std_first, '%.2f')]);
end