
%Routines.IP.neurons Segmentation of the mean image
%   ROUTINES.IP.NEURONS() Perform segmentation of the mean image for the
%   set and save it in the Files directory with the tag 'IP/@Neurons'.
%
%   Note: If no Focus object is defined in the workspace, a CLI is
%   triggered to let you choose.
%
%   Note: If a Steps object is defined in the workspace, it is
%   automatically updated.
%
%   See also: Routines.IP, Routines.IP.Show.neurons.

% === Parameters ==========================================================

btag = 'IP/@Brain';
mtag = 'IP/@Mean';
nptag = 'IP/@Neuropils';
ntag = 'IP/@Neurons';

% % % smwindow = 5;
% % % pertile = 0.2;
% % % Alim = [50 500];
% % %
% % % margin = 50;
% % %
% % % x_step = 25;
% % % y_step = 25;
% % %
% % % show_segmented = true;

switch F.IP.camera
    
    case 'Andor_iXon'

        area_lth = 2.4;         % Neuron area lower threshold (µm²)
        area_uth = 150;         % Neuron area upper threshold (µm²)
        anc_size = 3;           % Number of neighbooring pixels to correlate
        anc_th = 0.05;
        
        case {'PCO.Edge', 'PCO.edge'}
    
        area_lth = 8;           % Neuron area lower threshold (µm²)    
        area_uth = 120;         % Neuron area upper threshold (µm²)
        anc_size = 3;           % Number of neighbooring pixels to correlate
        anc_th = 0.05;
end

% -------------------------------------------------------------------------

spix = (F.dx + F.dy)/2;

% =========================================================================

% --- Get Focus object
tmp = ML.WS.get_by_class('Focus');
if ~isempty(tmp)
    F = evalin('base', tmp{1});
else
    F = getFocus;
end

% --- Get Steps object
tmp = ML.WS.get_by_class('ML.Steps');
if ~isempty(tmp)
    steps_exist = true;
    S = evalin('base', tmp{1});
else
    steps_exist = false;
end

% --- Matfiles preparation

Pmat = F.matfile(ptag);
Bmat = F.matfile(btag);
NPmat = F.matfile(nptag);
Nmat = F.matfile(ntag);

% --- Processing

if ~Nmat.exist
    
    % Preparation
    clc
    cli_ht = [F.name ' | Neuronal segmentation'];
    fprintf('\n%s\n', ML.CW.line(cli_ht));
    cld = ML.Time.Display;
    Fig = ML.Figures;
    
    % --- Load masks
    cld.step('Loading masks');
    brain = Bmat.load();
    neuropils = NPmat.load();
    
    % --- Load mean image
    cld.step('Loading mean image');
    Mean = Image(F.fname(mtag, 'png'));
    
    % --- Image processing [segmentation procedure updated 2018-03-27]
    
    cld.step('Image procesing');
    Img = Mean.copy();
    
    A = ordfilt2(Img.pix, 5, ones(round(8/spix)));
    B = ordfilt2(Img.pix, 95, ones(round(8/spix)));
    
    if strcmp(F.IP.line, 'Nuc')
        Pre = (B-Img.pix)./(B-A);
    else
        Pre = (Img.pix-A)./(B-A);
    end
    
    % --- Apply masks
    
    cld.step('Apply masks');
    
    % Brain mask
    tmp = zeros(F.IP.height, F.IP.width);
    tmp(brain.ind) = 1;
    Mask = Image(tmp);
    Mask.region(brain.bbox);
    Mask = logical(Mask.pix);
    Pre(~Mask) = Inf;
    
    % Neuropils masks
    for i = 1:neuropils.N
        Pre(neuropils.ind{i}) = Inf;
    end
    
    % --- Watershed
    
    cld.step('Watershed');
    
    % Watershed
    Pre(isnan(Pre)) = Inf;
    L = watershed(Pre);
    R = regionprops(L, 'Centroid', 'Area', 'Eccentricity', 'PixelIdxList', 'PixelList');
    
    cld.stop;
    
    % --- Neuron filtering loop
    
    % Preparation
    Plist = {R(:).PixelIdxList};
    Pos = reshape([R(:).Centroid], [2 numel(R)])';
    Posi = sub2ind(size(Img.pix), Pos(:,2), Pos(:,1));
    Area = [R(:).Area];
    mode = 'auto';
    
    while true
        
        switch mode
            
            case 'auto'
                
                pos = Pos;
                posi = Posi;
                area = Area;
                
                % --- Filter mask
                I = Mask(sub2ind(size(Img.pix), round(pos(:,2)), round(pos(:,1))));
                area = area(I);
                pos = pos(I,:);
                n_init = numel(area);

                % --- Filter size
                I = area>=area_lth & area<=area_uth;
                pos = pos(I,:);
                
                % --- Filter with correlations
                Raw = zeros(size(Img.pix));
                Raw(sub2ind(size(Img.pix), round(pos(:,2)), round(pos(:,1)))) = 1;
                
                if strcmp(F.IP.line, 'Nuc')
                    Res = -bwdist(Raw);
                else
                    Res = bwdist(Raw);
                end

                coeff = NaN(size(pos,1), 1);
                
                for i = 1:size(pos,1)
                    
                    x = round(pos(i,1));
                    y = round(pos(i,2));
                    
                    Sub = Img.pix(max(y-anc_size,1):min(y+anc_size, size(Img.pix,1)), max(x-anc_size,1):min(x+anc_size, size(Img.pix,2)));
                    Sub2 = Res(max(y-anc_size,1):min(y+anc_size, size(Img.pix,1)), max(x-anc_size,1):min(x+anc_size, size(Img.pix,2)));
                    
                    coeff(i) = corr2(Sub, Sub2);
                    
                end
                I = coeff>=anc_th;
                coeff = coeff(I);
                pos = pos(I,:);
                
                % --- Second watershed
                Raw = zeros(size(Img.pix));
                Raw(sub2ind(size(Img.pix), round(pos(:,2)), round(pos(:,1)))) = 1;
                Wat = bwdist(Raw);
                L = watershed(Wat);
    
                R = regionprops(L, {'Centroid', 'Area', 'PixelIdxList', 'Eccentricity', 'PixelList'});
    
                pos = reshape([R(:).Centroid], [2 numel(R)])';
                area = [R(:).Area];
                plist = {R(:).PixelIdxList};
                slist = {R(:).PixelList};
                ecc = [R(:).Eccentricity];
                
                % --- Filter mask
                I = Mask(sub2ind(size(Img.pix), round(pos(:,2)), round(pos(:,1))));
                area = area(I);
                pos = pos(I,:);
                plist = plist(I);
                slist = slist(I);
                ecc = ecc(I);
                
                % --- Filter area
                I = area>=area_lth & area<=area_uth;
                area = area(I);
                pos = pos(I,:);
                plist = plist(I);
                slist = slist(I);
                ecc = ecc(I);
                
        end
        
        % --- Display
        Routines.IP.Show.neurons(F, plist);
        
        % --- CLI
        clc
        fprintf('\n%s\n', ML.CW.line(cli_ht));
        
        fprintf('%i neurons kept over %i (%0.2f%%)\n', numel(I), n_init, numel(I)/n_init*100);
        
        fprintf('\nPlease choose an action:\n');
        
        fprintf('\n\t --- Shape filters\n');
        fprintf('\t[A] Display area pdf\n');
        fprintf('\t[E] Display eccentricity pdf\n');
        
        fprintf('\n\t[l] Set area lower threshold (current %i µm²)\n', area_lth);
        fprintf('\t[u] Set area upper threshold (current %i µm²)\n', area_uth);
        
        fprintf('\n\t --- Average Neighborhood correlation\n');
        fprintf('\t[w] Set ANC window size (current %i µm)\n', anc_size);
        fprintf('\t[t] Set ANC threshold (current %0.2f)\n', anc_th);
        
        fprintf('\n\t --- Line\n');
        fprintf('\t[L] Change line (current %s)', F.IP.line);
        
        fprintf('\n\t --- Continue\n');
        fprintf('\n\t[s] Save and continue\n\n');
        
        switch input('?> ', 's')
            
            case 'A'
                
                % --- Display area pdf
                p = ML.Stat.pdf(0:5:500, area(:));
                
                Fig.new('Area PDF');
                
                hold on
                box on
                plot(p.bin, p.pdf, 'b+-');
                ML.Visu.line(area_lth, 'x', 'linestyle', '--', 'color', 'k');
                ML.Visu.line(area_uth, 'x', 'linestyle', '--', 'color', 'k');
                xlabel('Area (pix)');
                ylabel('PDF');
                title('PDF of the neurons'' area');
                
            case 'E'
                
                % --- Display eccenticity pdf
                p = ML.Stat.pdf(linspace(0,1,round(numel(ecc)/50)), ecc(:));
                
                Fig.new('Eccenticity PDF');
                hold on
                box on
                plot(p.bin, p.pdf, 'b+-');
%                 ML.Visu.line(ecc_th, 'x', 'linestyle', '--', 'color', 'k');
                
                xlabel('Eccentricity');
                ylabel('PDF');
                title('PDF of the neurons'' eccentricities');
                
            case 'l'
                fprintf('\nNew area lower threshold: [1 ; %i]\n', area_uth);
                area_lth = input('?> ');
                mode = 'auto';
                
            case 'u'
                fprintf('\nNew area upper threshold: [%i ; Inf[\n', area_lth);
                area_uth = input('?> ');
                mode = 'auto';
                
            case 'w'
                fprintf('\nNew size: [1 ; Inf[\n');
                anc_size = input('?> ');
                mode = 'auto';
                
            case 't'
                fprintf('\nNew threshold value: [0 ; Inf[\n');
                anc_th = input('?> ');
                mode = 'auto';
                
            case 'L'
                fprintf('\nNew line: ''Nuc'', ''Cyt''');
                F.IP.line = input('?> ', 's');      % added 2018-03-28 for new segmentation
                mode = 'auto';
                
            case 's'
                
                % --- Save parameters
                cld.step('Saving parameters');
                Pmat.save('neurons_lower_area', area_lth, 'Area lower threshold');
                Pmat.save('neurons_upper_area', area_uth, 'Area upper threshold');
                
                % Save in Neurons
                cld.step('Saving neurons');
                Nmat.save('N', numel(area), 'Number of neurons');
                Nmat.save('pos', pos, 'Positions of the neurons'' centroids [x, y]');
                Nmat.save('ind', plist, 'Linear indices of the pixels belonging to each neuron in the mean image {[i]}.');
                Nmat.save('sub', slist, 'Subscript indices of the pixels belonging to each neuron in the mean image {[i j]}.');
                %Nmat.save('contours', Pcont(idx), 'Contours of the neurons {[x y]}.');
                
                cld.stop;
                
                break;
                
        end
    end
end

% --- Update Steps object
if steps_exist
    S.set_status(S.elms{F.set.id}, 'Neurons', ...
        ['<a href="matlab:Routines.IP.Show.neurons(F, ''set'', ' num2str(F.set.id) ');" style="text-decoration: none;">' S.tick '</a>']);
end