%Routines.IP.signals Signal extraction
%   ROUTINES.IP.SIGNALS() extract signals from the current set of the Focus
%   object and save it in the Files directory with the tag '@DFF'.
%
%   Note: If no Focus object is defined in the workspace, a CLI is
%   triggered to let you choose.
%
%   Note: If a Steps object is defined in the workspace, it is
%   automatically updated.
%
%   See also: Routines.IP.

% === Parameters ==========================================================

ptag = 'IP/@Parameters';
ktag = 'IP/@Background';
btag = 'IP/@Brain';
mtag = 'IP/@Mean';
dtag = 'IP/@Drifts';
ntag = 'IP/@Neurons';
nhtag = 'IP/@Neurohexs';
ttag = '@Times';
Dtag = '@DFF';

tw = 80;                % Baseline time window (s)

% =========================================================================

% --- Get Focus object
tmp = ML.WS.get_by_class('Focus');
if ~isempty(tmp)
    F = evalin('base', tmp{1});
else
    F = getFocus;
end

% --- Get Steps object
tmp = ML.WS.get_by_class('ML.Steps');
if ~isempty(tmp)
    steps_exist = true;
    S = evalin('base', tmp{1});
else
    steps_exist = false;
end

% --- Command line display
cld = ML.Time.Display;

% --- Matfiles objects
Pmat = F.matfile(ptag);
Kmat = F.matfile(ktag);
Bmat = F.matfile(btag);
dmat = F.matfile(dtag);
Nmat = F.matfile(ntag);
NHmat = F.matfile(nhtag);
Tmat = F.matfile(ttag);
Dmat = F.matfile(Dtag);

% --- Get fluorescence signals
if ~Nmat.exist('fluo')
    
    clc
    fprintf('\n%s%s\n', [F.Name - ML.CW.line('Signals')]);
    fprintf('Layer : %i\n', F.set.id);
    cld.start('Loading reference image');
    Ref = Image(F.fname(mtag, 'png'));
    
    cld.step('Getting timestamp reference');
    Img = F.iload(1);
    t0 = Img.timestamp;
    
    cld.step('Loading bounding box');
    Bmat = F.matfile(btag);
    brain = Bmat.load('bbox', 'contours');
    
    cld.step('Loading Neurons');
    Nmat = F.matfile(ntag);
    neurons = Nmat.load('N', 'ind');
    
    cld.step('Loading Neurohexs');
    NHmat = F.matfile(nhtag);
    neurohexs = NHmat.load('N', 'ind');
    
    % --- The fluorescence loop
    
    % Mask
    cld.step('Get background mask');
    mask = zeros(F.IP.height, F.IP.width);
    [X, Y] = meshgrid(1:F.IP.width, 1:F.IP.height);
    mask(inpolygon(X,Y,brain.contours(:,1), brain.contours(:,2))) = 1;
    D = bwdist(mask);
    mask = find(D>=max(D(:))/2);
    
    % Preparation
    cld.step('Fluorescence loop preparation');
    
    cld.period = numel(F.set.frames)/100;
    tstamp = NaN(1, numel(F.set.frames));
    mbkg = NaN(1, numel(F.set.frames));
    dx = NaN(1, numel(F.set.frames));
    dy = NaN(1, numel(F.set.frames));
    
    mfn = NaN(neurons.N, numel(F.set.frames)); % Mean fluorescence of neurons
    sfn = NaN(neurons.N, numel(F.set.frames)); % Std fluorescence of neurons
    
    mfh = cell(numel(neurohexs.N),1);
    sfh = cell(numel(neurohexs.N),1);
    for i = 1:numel(neurohexs.N)
        mfh{i} = NaN(neurohexs.N{i}, numel(F.set.frames)); % Mean fluorescence of neurohexs
        sfh{i} = NaN(neurohexs.N{i}, numel(F.set.frames)); % Std fluorescence of neurohexs
    end
    
    cld.stop
    
    for ti = 1:numel(F.set.frames)
        
        % --- Load image
        
        Img = F.iload(ti);
        
        % --- Get relative timestamp (ms)
        
        tmp = Img.timestamp;
        tstamp(ti) = (tmp.t-t0.t)/1000;
        Img.rm_infos('rep', NaN);
        
        % --- Get background
        
        % Direct mean is 10x faster than Img.background
        mbkg(ti) = nanmean(Img.pix(mask));
        
        % ---- Correct for drift
        
        Img.region(brain.bbox);
        [dx(ti), dy(ti)] = Ref.fcorr(Img);
        Img.translate(-dy(ti), -dx(ti));
        
        % --- Get fluorescence
        
        % Using direct definitions in cellfun is 2x faster than @mean and @std
        mfn(:,ti) = cellfun(@(x) sum(Img.pix(x))/numel(x), neurons.ind)';
        sfn(:,ti) = cellfun(@(x) sqrt(sum((Img.pix(x)-mean(Img.pix(x))).^2)/numel(x)), neurons.ind)';
        
        mfh = cell(numel(neurohexs.N),1);
        sfh = cell(numel(neurohexs.N),1);
        for i = 1:numel(neurohexs.N)
            mfh{i}(:,ti) = cellfun(@(x) sum(Img.pix(x))/numel(x), neurohexs.ind{i})';
            sfh{i}(:,ti) = cellfun(@(x) sqrt(sum((Img.pix(x)-mean(Img.pix(x))).^2)/numel(x)), neurohexs.ind{i})';
        end
        
        cld.waitline('ti');
        
    end
    
    % --- Saving
    
    % Times
    cld.step('Saving timestamps');
    Tmat.save('t', tstamp, 'Frame timestamps, relative to the first frame of the run (ms)');
    
    % Backgrounds
    cld.step('Saving background average values');
    Kmat.save('mean', mbkg, 'Average background noise at each time step [m]');
    
    % Drifts
    cld.step('Saving drifts');
    dmat.save(dx, 'Drift in the x-direction, at each time step [pix]');
    dmat.save(dy, 'Drift in the y-direction, at each time step [pix]');
    
    % Fluorescence
    cld.step('Saving fluorescence signals');
    Nmat.save('fluo', mfn, 'Mean fluorescence for each neuron at each frame [i \ ti]');
    Nmat.save('sfluo', sfn, 'Standard deviation of the fluorescence in each neuron at each frame [i \ ti]');
    NHmat.save('fluo', mfh, 'Mean fluorescence for each neuron at each frame {[i \ ti]}');
    NHmat.save('sfluo', sfh, 'Standard deviation of the fluorescence in each neuron at each frame {[i \ ti]}');
    
    cld.stop
end

% --- Get baselines
if true %~Nmat.exist('baseline')
    
    % Load times
    cld.step('Loading times');
    Times = Tmat.load();
    Times.t = Times.t/10^6;
    
    % Load Background
    %cld.step('Loading Background');
    %background = Kmat.load('mean');
    
    % Load fluorescence signals
    cld.step('Loading fluorescence signals');
    neurons = Nmat.load('N', 'fluo');
    neurohexs = NHmat.load('N', 'fluo');
    
    % Get filter size
    cld.step('Get filter size');
    dt = mean(diff(F.uconv('t', 's')));
    fs = round(tw/dt/2);
    
    % Get neurons baselines
    cld.step('Preparation for neurons'' baseline computation');
%     bn = NaN(neurons.N, numel(F.set.frames));
    cld.period = neurons.N/100;
    
    cld.stop('Getting neurons'' baselines');
    cld.period = numel(F.set.frames)/100;
    
    % Use faster function to get the baseline (2018-07-04 GLG)
    bn = computeBaseline(neurons.fluo, 8, round(tw/dt), false);
    
%     for ti = 1:numel(F.set.frames)
%         
%         % Time indexes
%         I = max(ti-fs, 1):min(ti+fs, numel(F.set.frames));
%         
%         % Mean and std
%         m = sum(neurons.fluo(:,I),2)/numel(I);
%         tmp = bsxfun(@minus, neurons.fluo(:,I), m);
%         mask = bsxfun(@minus, abs(tmp), sqrt(sum(tmp.^2,2)/numel(I)))<=1;
%         bn(:,ti) = sum(neurons.fluo(:,I).*mask,2)./sum(mask,2);
%         
%         cld.waitline('ti');
%         
%     end

    % Get neurohexs baselines
    fprintf('Getting neurohexs'' baselines\n');
    
    bh = cell(numel(neurohexs.N),1);
    for ti = 1:numel(F.set.frames)
        
        % Time indexes
        I = max(ti-fs, 1):min(ti+fs, numel(F.set.frames));
        
        for i = 1:numel(neurohexs.N)
            
            % Mean and std
            m = sum(neurohexs.fluo{i}(:,I),2)/numel(I);
            tmp = bsxfun(@minus, neurohexs.fluo{i}(:,I), m);
            mask = bsxfun(@minus, abs(tmp), sqrt(sum(tmp.^2,2)/numel(I)))<=1;
            bh{i}(:,ti) = sum(neurohexs.fluo{i}(:,I).*mask,2)./sum(mask,2);
            
        end
        
        cld.waitline('ti');
    end
    
    % --- Save
    
    cld.start('Saving parameters'),
    Pmat.save('baseline_window', tw, 'Baseline averaging time window (sec)');
    
    cld.step('Saving neurons baselines');
    Nmat.save('baseline', bn, 'Baseline for each neuron at each frame [i\ti]');
    
    cld.step('Saving neurohexs baselines');
    NHmat.save('baseline', bh, 'Baseline for each neurohex at each frame [i\ti]');
    
    cld.stop
    
end

% --- Get DFF
if ~Dmat.exist
    
    % Loadings
    cld.start('Loading fluorescence signals and baseline');
    neurons = Nmat.load('fluo', 'baseline');
    neurohexs = NHmat.load('fluo', 'baseline');
    
    % MODIFICATION 2017-12-18 GLG : Now the removed background is the
    % background properly computed with Image.background rather than the
    % mean (see line 128). Drawback is the background is from the first
    % frame only.
    
    cld.step('Loading backgrounds');
    %background = Kmat.load('mean');            % not accurate
    background = Kmat.load('mean_first');       % from the first frame only
    background = background.mean_first;         % scalar
    
    % Compute DFF
    cld.step('Computing DFF');
    % With over-estimated background (repmat is not useful from R2017a)
    %dffn = (neurons.fluo - neurons.baseline)./(neurons.baseline - repmat(background.mean,[size(neurons.fluo,1) 1]));   
    % With background from first frame (Image.background)
    
    dffn = (neurons.fluo - neurons.baseline)./(neurons.baseline - background);
    dffh = cell(numel(neurohexs.fluo),1);
    
    for i = 1:numel(dffh)
        dffh{i} = (neurohexs.fluo{i} - neurohexs.baseline{i})./(neurohexs.baseline{i} - repmat(background.mean,[size(neurohexs.fluo{i},1) 1]));
    end
    
    % --- Save
    cld.step('Saving neurons DFF');
    Dmat.save('neurons', dffn, 'DFF = (F(i,t)-baseline(i,t))/(baseline(i,t)-background(t))');
    
    cld.step('Saving neurohexs DFF');
    Dmat.save('neurohexs', dffh, 'DFF = (F(i,t)-baseline(i,t))/(baseline(i,t)-background(t))');
    
end

cld.stop

% --- Update Steps object
if steps_exist
    S.set_status(S.elms{F.set.id}, 'Signals', S.tick);
end