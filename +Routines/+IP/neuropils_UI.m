function neuropils()
%Routines.IP.neuropils Segmentation of the mean image
%   ROUTINES.IP.NEUROPILS() Finds the neuropils in the mean image
%   for the current set of the Focus object F present in the base
%   workspace. Neuropils contours and masks are saved in the matfile tagged
%   'IP/@Neuropils' in the current set's Files directory.
%
%   Note: If no Focus object is defined in the base workspace, a CLI is
%   triggered to let you choose.
%
%   Note: If a Steps object is defined in the base workspace, it is
%   automatically updated.
%
%   See also: Routines.IP, Routines.IP.Show.neuropils.

% === Parameters ==========================================================

ptag = 'IP/@Parameters';
btag = 'IP/@Brain';
mtag = 'IP/@Mean';
nptag = 'IP/@Neuropils';

% Tolerance for polygon reduction
tol = 2;

% Default values
th = 0.0143;    % Binarization threshold
wd = 10;        % Density window size
dth = 0.05;     % Density threshold
a = 3000;       % Minimum area size

% =========================================================================

% --- Global variables
global B V

% --- Get Focus object
tmp = ML.WS.get_by_class('Focus');
if ~isempty(tmp)
    F = evalin('base', tmp{1});
else
    F = getFocus;
end

% --- Get Steps object
tmp = ML.WS.get_by_class('ML.Steps');
if ~isempty(tmp)
    steps_exist = true;
    S = evalin('base', tmp{1});
else
    steps_exist = false;
end

% --- Prepare matfiles

Pmat = F.matfile(ptag);
Bmat = F.matfile(btag);
NPmat = F.matfile(nptag);

if ~NPmat.exist
    
    % --- Preparation
    Fig = ML.Figures;
    fig = Fig.clear('IP');
    
    % Time display
    clc
    cld = ML.Time.Display;

    % --- Loads
    cld.start('Loading mean image and brain infos');
    Mean = Image(F.fname(mtag, 'png'));
    brain = Bmat.load();
    
    % Create brain mask
    Mask = zeros(F.IP.height, F.IP.width);
    Mask(brain.ind) = 1;
    Mask = Image(Mask);
    Mask.region(brain.bbox);
    
    cld.stop
        
    % Definition and computation
    get_neuropils('sigma', 1, 'size', 21, 'threshold', 8.5, 'area', 1000);
    
    % Protect figure toolbar
    set(gcf,'toolbar','figure');
    
    % Slider sigma
    h = uicontrol('style','text', 'String', 'Gaussian sigma', ...
        'Units', 'pixels', 'position', [10 260 120 20]);
    ML.GUI.spinner('value', 1, 'inc', 0.1, ...
        'position', [10,240,120,20], ...
        'callback', @(h, e) get_neuropils('sigma', h.Value));
    
    % Slider size
    h = uicontrol('style','text', 'String', 'STD window size', ...
        'Units', 'pixels', 'position', [10 200 120 20]);
    ML.GUI.spinner('value', 21, 'inc', 2, ...
        'position', [10,180,120,20], ...
        'callback', @(h, e) get_neuropils('size', h.Value));
    
    % Slider threshold
    h = uicontrol('style','text', 'String', 'STD threshold', ...
        'Units', 'pixels', 'position', [10 140 120 20]);
    ML.GUI.spinner('value', 8.5, 'inc', 0.1, ...
        'position', [10,120,120,20], ...
        'callback', @(h, e) get_neuropils('threshold', h.Value));
    
    % Area threshold
    h = uicontrol('style','text', 'String', 'Area threshold', ...
        'Units', 'pixels', 'position', [10 80 120 20]);
    ML.GUI.spinner('value', 1000, 'inc', 50, ...
        'position', [10,60,120,20], ...
        'callback', @(h, e) get_neuropils('area', h.Value));
    
    % CLI
    h = uicontrol('style','pushbutton', 'String', 'Continue', ...
        'Units', 'pixels', 'position', [10 20 120 20], ...
        'callback', @(h, e) cli_neuropils);
    
    uiwait
    
    delete(fig);
    
else
    if steps_exist
        S.set_status(S.elms{F.set.id}, 'Neuropils', ...
            ['<a href="matlab:Routines.IP.Show.neuropils(F,''set'',' num2str(F.set.id) ');" style="text-decoration: none;">' S.tick '</a>']);
    end
end

    % =====================================================================
    function get_neuropils(varargin)
        
        % --- Inputs ------------------------------------------------------
        
        in = ML.Input(varargin{:});
        in.addParamValue('sigma', NaN, @isnumeric);
        in.addParamValue('size', NaN, @isnumeric);
        in.addParamValue('threshold', NaN, @isnumeric);
        in.addParamValue('area', NaN, @isnumeric);
        in = +in;
        
        % --- Variables ---------------------------------------------------
        
        persistent vars
        
        % -----------------------------------------------------------------
        
        % Default values
        if isempty(vars)
            vars = struct('sigma', NaN, 'size', NaN, ...
                'threshold', NaN, 'area', NaN);
        end
        
        % Current values
        if ~isnan(in.sigma), vars.sigma = in.sigma; end
        if ~isnan(in.size), vars.size = in.size; end
        if ~isnan(in.area), vars.area = in.area; end
        if ~isnan(in.threshold), vars.threshold = in.threshold; end
        
        % --- Computation
        Img = Mean.copy;
        
        % Asymetric smooth
        Test = Img.copy;
        Test.filter('Gaussian', 'box', [30 1], 'sigma', vars.sigma);
        Img.pix = Img.pix./Test.pix;
        
        % Standard deviation filtering
        pix = stdfilt(Img.pix, ones(vars.size));
        bin = pix<=vars.threshold/1000;
        
        % Remove small elements and holes
        bin = bwareaopen(bin, vars.area);
        bin = imfill(bin, 'holes');
        
        % Get boundaries
        B = cellfun(@(x) reduce(x), bwboundaries(bin), 'UniformOutput', false);
        B = cellfun(@(x) reduce(x), bwboundaries(bin), 'UniformOutput', false);
        
        % Store parameters
        V = vars;
        
        % --- Display
        
        % Mean image
        Routines.IP.Show.neuropils(F, 'contours', B);
        colorbar off
        set(gca, 'Position', [0.25 0.11 0.73 0.815]);
        
        % =============================================================
        function out = reduce(in)
            [x, y] = reducem(in(:,1), in(:,2), tol);
            out = [y x];
        end
    end

    % =====================================================================
    function cli_neuropils(varargin)
        
        clc
        ML.CW.line([F.name ' | Neuropils']);
        
        fprintf('\nPlease choose an action:\n\n');
        
        fprintf('\t[g] Back to the graphical interface\n');
        fprintf('\t[d] Delete manually a contour\n');
        
        fprintf('\n\t[s] Save and continue\n\n');
        
        switch input('?> ', 's')
            
            case 'g'
                Routines.IP.neuropils;
                
            case 'd'
                
                % Graphical selection
                [x, y] = ML.ginput(1);
                
                % Get distances
                d = NaN(numel(B),1);
                for i = 1:numel(B)
                    d(i) = min((B{i}(:,1)-x).^2 + (B{i}(:,2)-y).^2);
                end
                
                % Remove selected boundary
                [~, mi] = min(d);
                B(mi) = [];
                
                % Loop
                Routines.IP.Show.neuropils(F, 'contours', B);
                colorbar off
                set(gca, 'Position', [0.25 0.11 0.73 0.815]);
                cli_neuropils;
                
            case 's'
                                
                % --- Compute indices
                cld.start('Computing indices');
                pos = NaN(numel(B),2);
                ind = cell(numel(B),1);
                sub = cell(numel(B),1);
                for i = 1:numel(B)
                    tmp = poly2mask(B{i}(:,1), B{i}(:,2), Mean.height, Mean.width);
                    ind{i} = find(tmp);
                    [I, J] = find(tmp);
                    sub{i} = [I J];
                    pos(i,:) = [mean(J) mean(I)];
                end
                
                % --- Save parameters
                cld.step('Saving parameters');
                Pmat.save('neuropils_sigma', V.sigma, 'standard deviation of the Gaussian filter');
                Pmat.save('neuropils_size', V.size, 'Size of the stdfilt');
                Pmat.save('neuropils_threshold', V.threshold, 'Threshold on the standard filter values');
                Pmat.save('neuropils_area', V.area, 'Minimum area size');
                Pmat.save('neuropils_tol', tol, 'Angular tolerance for contour reduction');
                
                % --- Save
                cld.step('Saving neuropils');
                NPmat.save('N', numel(B), 'Number of neuropils');
                NPmat.save(pos, 'Centers of mass of the neuropils [x y]');
                NPmat.save(ind, 'Linear indices of the neuropils masks {[i]}.');
                NPmat.save(sub, 'Subscript indices of the neuropils masks {[i j]}.');
                NPmat.save('contours', B, 'Contour polygons {[x y]}');
               
                cld.stop;
                
                % --- Update Steps object
                if steps_exist
                    S.set_status(S.elms{F.set.id}, 'Neuropils', ...
                        ['<a href="matlab:Routines.IP.Show.neuropils(F, ''set'',' num2str(F.set.id) ');" style="text-decoration: none;">' S.tick '</a>']);
                end
                
                % --- clear globals
                clear global B V
                
                uiresume
        end
        
    end
end

