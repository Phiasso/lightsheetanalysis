function out = Config(varargin)
%Routines.Config Configuration routine.
%   ROUTINES.CONFIG(DATE, RUN) runs the configuration utility for RUN at
%   DATE.
%
%   ROUTINES.CONFIG(DATE) asks for the RUN prior to configuration.
%
%   ROUTINES.CONFIG() asks for the DATE and RUN prior to configuration.
%
%
%   Updated 2018-03-28 : Now you can add the strain used, nuclear or not.
%   It plays a role in the segmentation.
%   Updated 2018-07-02 : Now uses the Parameter object to read the
%   parameter file.
%   Updated 2018-07-02 : New interleaved mode for defining sets, now get
%   real values of Z altitude in the slaveHM & triangle mode.
%
%   See also .

% === Input variables =====================================================

in = ML.Input;
in.addOptional('date', '', @ischar);
in.addOptional('run', [], @isnumeric);
in = +in;

% === Parameters ==========================================================

tag = 'Config';
ext = 'tif';

% === Default values ======================================================

dt = 20;            % 1/f Sampling (ms)
exposure = 10;      % Exposure time (ms)
delaybefore = NaN;
delayafter = NaN;
dx = 0.8;           % pixel size (um). 0.8 is with lens 150mm and bin x2
dy = 0.8;           % pixel size (um)
camera = 'PCO.Edge';
Line = 'Cyt';

units = struct('dx', 'um', ...
    'dy', 'um', ...
    'dt', 'ms', ...
    'z', 'um', ...
    't', 'ms', ...
    'exposure', 'ms', ...
    'delaybefore', 'ms', ...
    'delayafter', 'ms');

% =========================================================================

% --- Preparation
line = @(x) fprintf('%s\n', ML.CW.line(x));
quit = false;

% --- The main loop
while ~quit
    
    % --- Get Focus object
    if ~isempty(in.date)
        F = getFocus(in.date, in.run);
    else
        tmp = ML.WS.get_by_class('Focus');
        if ~isempty(tmp)
            F = evalin('base', tmp{1});
        else
            F = getFocus;
        end
    end
    
    % --- Display
    clc
    line('Configuration');
    
    % --- Actions
    while true
        
        Conf = F.matfile(tag);
        
        % --- Display
        clc
        line(['Config ' F.date ' (' num2str(F.run, '%02i') ')']);
        fprintf('The ''%s.mat'' file ', Conf.name);
        if Conf.exist
            ML.cprintf([0 0.8 0], 'exists');
        else
            ML.cprintf('red', 'does not exist');
        end
        fprintf('.\n');
        
        % --- Prepare images list
        images = dir([F.Images '*.' ext]);
        
        % --- Prepare config structure
        if ~exist('config', 'var')
            if Conf.exist
                
                % Load config
                config = Conf.load;
                
            else
                
                % Initialize with default parameters
                config = struct('dx', dx, ...
                    'dy', dy, ...
                    'dt', dt, ...
                    'exposure', exposure, ...
                    'delaybefore', delaybefore, ...
                    'delayafter', delayafter, ...
                    'units', units);
                
                % Get Image Processing parameters
                tmp = regexp(images(1).name, '^(.*_)([0-9]*)\.(.*)', 'tokens');
                config.IP.prefix = tmp{1}{1};
                config.IP.format = ['%0' num2str(numel(tmp{1}{2})) 'i'];
                config.IP.extension = tmp{1}{3};
                
                tmp = imfinfo([F.Images images(1).name]);
                
                config.IP.date = tmp.FileModDate;
                config.IP.width = tmp.Width;
                config.IP.height = tmp.Height;
                config.IP.bitdepth = tmp.BitDepth;
                config.IP.class = ['uint' num2str(tmp.BitDepth)];
                
                if contains(tmp.Software, 'PCO')
                    tmp.Software = 'PCO.edge';
                end
                
                switch tmp.Software
 
                    case 'National Instruments IMAQ   '
                        config.dx = 0.66;           
                        config.dy = 0.66;           
                        config.IP.camera = 'Andor_iXon';
                    
                    case 'PCO.edge'
                        config.dx = 0.8;
                        config.dy = 0.8;
                        config.IP.camera = 'PCO.edge';
                        
                    otherwise
                        config.IP.camera = camera;  % default
                end
                
                tmp = Image([F.Images images(round(numel(images)/2)).name]);
                config.IP.range = tmp.autorange;
                config.IP.line = Line;
                
                % Define sets
                config.sets = struct('id', {}, 'type', {}, 'frames', {}, 't', {}, 'z', {});
                
                % Autoload from Parameters.txt file
                fparam1 = [F.Data 'Parameters.txt'];
                fparam2 = [F.Data 'Parameters' num2str(F.run) '.txt'];
                
                if exist(fparam1, 'file') || exist(fparam2, 'file')
                    
                    if exist(fparam1, 'file')
                        fparam = fparam1;
                    elseif exist(fparam2, 'file')
                        fparam = fparam2;
                    end
                    
                    P = NT.Parameters;
                    P.load(fparam);
                    
                    % Check consistency 
                    
                    if numel(images) < P.NFrames
                        ML.cprintf([1 0.5 0], ['\nWARNING: Inconsistent number of images: ' ... 
                            num2str(P.NFrames) ' expected, ' num2str(numel(images)) ' found.\n']); 
                    end 
                    
                    % --- Exposure
                    config.exposure = P.Exposure;
                    
                    % --- Time between two frames
                    config.dt = P.Exposure + P.Delay;
                    
                    % --- Delay before
                    config.delaybefore = P.DelayBefore;
                   
                    % --- Delay after
                    config.delayafter = P.DelayAfter;

                end
                
            end
        end
        
        % Get existing frames
        frames = cellfun(@(x) str2double(x{1}), cellfun(@(x) regexp(x, ['^' config.IP.prefix '([0-9]*)'], 'tokens'), {images(:).name}));
        
        % --- Select action
        
        fprintf('\nPlease choose an action:\n');
        
        fprintf('\n--- General\n');
        fprintf('\t[x] - Change dx = %f %s\n', config.dx, config.units.dx);
        fprintf('\t[y] - Change dy = %f %s\n', config.dy, config.units.dy);
        fprintf('\t[t] - Change dt = %f %s\n', config.dt, config.units.dt);
        fprintf('\t[e] - Change exposure = %f %s\n', config.exposure, config.units.exposure);
        fprintf('\t[b] - Change delay before = %f %s\n', config.delaybefore, config.units.delaybefore);
        fprintf('\t[a] - Change delay after = %f %s\n', config.delayafter, config.units.delayafter);
        fprintf('\t[p] - Change prefix = %s\n', config.IP.prefix);
        fprintf('\t[f] - Change format = %s\n', config.IP.format);
        fprintf('\t[.] - Change extension = %s\n', config.IP.extension);
        fprintf('\t[r] - Change range = %s\n', mat2str(config.IP.range));
        fprintf('\t[L] - Change line = %s\n', config.IP.line);
        
        fprintf('\n--- Sets\n');
        
        % Existing sets
        I = frames;
        if numel(config.sets)
            for i = 1:numel(config.sets)
                fprintf('\t[%i] - %s (%i frames)\n', i, config.sets(i).type, numel(config.sets(i).frames));
                I = setdiff(I, config.sets(i).frames);
            end
            fprintf('\n');
        end
        
        % Remaining frames
        fprintf('\tRemaining frames: %i (on %i)\n', numel(I), numel(frames));
        
        if numel(I)
            fprintf('\t[n] - New set\n');
            fprintf('\t[I] - Automatic 3D, interleaved-triangle-slaveHM mode\n');
            fprintf('\t[l] - Automatic 3D, linear mode\n');
            fprintf('\t[i] - Automatic 3D, interleaved mode\n');
        end
        if numel(config.sets)
            fprintf('\t[d] - Delete set\n');
        end
        
        fprintf('\n--- Save & quit\n');
        fprintf('\n\t[s] - Save the parameters in the ''%s.mat'' file and quit.\n', tag);
        fprintf('\t[q] - Quit\n');
        a = input('?> ', 's');
        
        switch a
            
                % --- Change dx
            case 'x'
                line('');
                fprintf('Please enter the new value for dx (%s):\n', config.units.dx);
                config.dx = input('?> ');
                
                % --- Change dy
            case 'y'
                line('');
                fprintf('Please enter the new value for dy (%s):\n', config.units.dy);
                config.dy = input('?> ');
                
                % --- Change dt
            case 't'
                line('');
                fprintf('Please enter the new value for dt (%s):\n', config.units.dt);
                config.dt = input('?> ');
                
                % --- Change exposure
            case 'e'
                line('');
                fprintf('Please enter the new value for the exposure time (%s):\n', config.units.exposure);
                config.exposure = input('?> ');
                
                % --- Change delay before
            case 'b'
                line('');
                fprintf('Please enter the new value for the delay before time (%s):\n', config.units.delaybefore)
                config.delaybefore = input('?> ');
                
                % --- Change delay after
            case 'a'
                line('');
                fprintf('Please enter the new value for the delay after time (%s):\n', config.units.delayafter)
                config.delayafter = input('?> ');
                
                % --- Change prefix
            case 'p'
                line('');
                fprintf('Please enter the prefix:\n');
                config.IP.prefix = input('?> ', 's');
                
                % --- Change format
            case 'f'
                line('');
                fprintf('Please enter the new format:\n');
                config.IP.format = input('?> ', 's');
                
                % --- Change extension
            case '.'
                line('');
                fprintf('Please enter the new extension:\n');
                config.IP.extension = input('?> ', 's');
                
                % --- Change range
            case 'r'
                line('');
                fprintf('Please enter the new range:\n');
                config.IP.range = input('?> ');
                
                % --- Change line
            case 'L'
                line('');
                fprintf('Please enter new line (''Nuc'' or else)');
                config.IP.line = input('?> ', 's');
                
                % --- New set
            case 'n'
                
                line('New set');
                id = numel(config.sets)+1;
                config.sets(id).id = id;
                
                % Type
                config.sets(id).type = choose_type();
                
                % Index
                fprintf('Please enter the corresponding frame numbers (from %i to %i) [Enter for all]:\n', min(frames), max(frames));
                tmp = input('?> ');
                if isempty(tmp)
                    config.sets(id).frames = frames;
                else
                    config.sets(id).frames = tmp;
                end
                
                % Times
                switch config.IP.camera
                    case 'Andor_iXon'
                        config.sets(id).t = load([F.Images 'Times.txt']);
                    otherwise
                        config.sets(id).t = config.sets(id).frames*config.dt;
                end
                
                % Altitudes
                switch config.sets(id).type
                    
                    case 'Layer'
                        
                        fprintf('Please enter the layer altitude (%s) [Press enter for zero]:\n', config.units.z);
                        tmp = input('?> ');
                        if isempty(tmp)
                            config.sets(id).z = 0;
                        else
                            config.sets(id).z = tmp;
                        end
                        
                    case 'Scan'
                        fprintf('Please enter the corresponding altitudes (%s):\n', config.units.z);
                        config.sets(id).z = input('?> ');
                end
                
                % --- Delete set
            case 'd'
                line('');
                fprintf('Please enter the index of the set to delete:\n');
                config.sets(input('?> ')) = [];
                
                % --- Automatic layers, linear
            case 'l'
                
                % Number of layers
                fprintf('Please enter the number of layers:\n');
                nL = input(['[Auto : ' num2str(P.NLayers) '] ?> ']);
                
                if isempty(nL)
                    nL = P.NLayers;
                end
                
                % Altitudes
                fprintf('Please enter the altitude increment (%s):\n', config.units.z);
                dz = input(['[Auto : ' num2str(P.Increment) '] ?> ']);
                
                if isempty(dz)
                    dz = P.Increment;
                end
                
                for i = 1:nL
                    
                    id = numel(config.sets)+1;
                    config.sets(id).id = id;
                    
                    % Type
                    config.sets(id).type = 'Layer';
                    
                    % Index
                    config.sets(id).frames = frames(i:nL:end);
                    
                    % Times
                    config.sets(id).t = config.sets(id).frames*config.dt;
                    
                    % Altitudes
                    config.sets(id).z = dz*(i-1);
                    
                end
                
                % --- Automatic layers, interleaved in triangle + slave HM
            case 'I'
                
                % Number of layers
                fprintf('Please enter the number of layers:\n');
                nL = input(['[Auto : ' num2str(P.NLayers) '] ?> ']);
                
                if isempty(nL)
                    nL = P.NLayers;
                end
                
                % Altitudes
                Z = getTriangleAltitude(P);
                
                for i = 1:nL
                    
                    id = numel(config.sets)+1;
                    config.sets(id).id = id;
                    
                    % Type
                    config.sets(id).type = 'Layer';
                    
                    % Index
                    config.sets(id).frames = frames(i:nL:end);
                    
                    % Times
                    config.sets(id).t = config.sets(id).frames*config.dt;
                    
                    % Altitudes
                    config.sets(id).z = Z(i);
                    
                end
                
                % --- Automatic layers, interleaved
            case 'i'
                
                % Number of layers
                fprintf('Please enter the number of layers:\n');
                nL = input(['[Auto : ' num2str(P.NLayers) '] ?> ']);
                
                if isempty(nL)
                    nL = P.NLayers;
                end
                
                % Altitudes
                fprintf('Please enter the altitude increment (%s):\n', config.units.z);
                dz = input(['[Auto : ' num2str(P.Increment) '] ?> ']);
                
                if isempty(dz)
                    dz = P.Increment;
                end
                
                tmp = sortrows([[1:2:nL fliplr(2:2:nL)]' (1:nL)']);
                I = tmp(:,2);
                
                for i = 1:nL
                    
                    id = numel(config.sets)+1;
                    config.sets(id).id = id;
                    
                    % Type
                    config.sets(id).type = 'Layer';
                    
                    % Index
                    config.sets(id).frames = frames(I(i):nL:end);
                    
                    % Times
                    config.sets(id).t = config.sets(id).frames*config.dt;
                    
                    % Altitudes
                    config.sets(id).z = dz*(i-1);
                    
                end
                
                % --- Save parameters
            case 's'
                Conf.save('dx', config.dx, ['Pixel x-size (' config.units.dx ')']);
                Conf.save('dy' ,config.dy, ['Pixel y-size (' config.units.dy ')']);
                Conf.save('dt', config.dt, ['Inverse of the acquisition frequency (' config.units.dt ')']);
                Conf.save('exposure', config.exposure, ['Exposure time (' config.units.exposure ')']);
                Conf.save('delaybefore', config.delaybefore, ['Delay before (' config.units.delaybefore ')']);
                Conf.save('delayafter', config.delayafter, ['Delay after (' config.units.delayafter ')']);
                Conf.save('sets', config.sets, 'Sets (layers and/or scans)');
                Conf.save('IP', config.IP, 'Image processing parameters');
                Conf.save('units', config.units, 'Summary of the units used in this configuration file');
                
                quit = true;
                break;
                
            case 'q'
                quit = true;
                break;
                
            otherwise
                
                % Check that the input is a number
                if ~numel(a) || ~all(isstrprop(a, 'digit')), continue; end
                
                line(['Modifying set ' a]);
                n = str2double(a);
                
                while true
                    
                    fprintf('Please choose an action:\n');
                    
                    fprintf('\t[t] - Change type (''%s'')\n', config.sets(n).type);
                    fprintf('\t[i] - Change frames (%i elements)\n', numel(config.sets(n).frames));
                    fprintf('\t[z] - Change altitudes (%i elements)\n', numel(config.sets(n).z));
                    
                    fprintf('\t[Enter] - Return\n');
                    a = input('?> ', 's');
                    
                    switch a
                        
                        % Type
                        case 't'
                            line('');
                            config.sets(n).type = choose_type();
                            
                            % Frames
                        case 'i'
                            line('');
                            fprintf('Please enter the frame numbers:\n');
                            config.sets(n).frames = input('?> ');
                            
                            % Altitudes
                        case 'z'
                            line('')
                            fprintf('Please enter the altitudes (µm):\n');
                            config.sets(n).z = input('?> ');
                            
                        otherwise
                            break
                            
                    end
                    
                end
                
        end
    end
end

fprintf('%s\n', ML.CW.line('End of configuration'));

% --- Output
out = 'Done';

% -------------------------------------------------------------------------
function out = choose_type()

while true
    fprintf('Please choose a set type:\n');
    fprintf('\t[l] Layer\n');
    fprintf('\t[s] Scan\n');
    switch lower(input('?> ', 's'))
        case 'l', out = 'Layer';
        case 's', out = 'Scan';
        otherwise, continue
    end
    break;
end
